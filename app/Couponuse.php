<?php

namespace catalyst;

use Illuminate\Database\Eloquent\Model;

class Couponuse extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'coupon_use';
    protected $fillable = [
        'video_id','user_id', 'coupon_id', 'ip_adddress',
    ];
}
