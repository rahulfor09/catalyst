<?php

namespace catalyst;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','coach_sponsor','company', 'username','email', 'password','phone_no','user_type','coach_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
        //return $this->belongsTo('catalyst\Profile');
        return $this->hasOne('catalyst\Profile', 'user_id');
    }
}
