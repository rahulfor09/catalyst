<?php

namespace catalyst;

use Illuminate\Database\Eloquent\Model;

class CoachMapCoachee extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'coach_map_coachee';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'coachee_id', 'coach_id', 'status'
    ];
}
