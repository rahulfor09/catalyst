<?php

namespace catalyst;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code','description', 'no_of_use', 'from_date','to_date','status','created_at',
    ];

    public function Owner()
    {
        return $this->hasOne('catalyst\User','id','user_id');
    }
}
