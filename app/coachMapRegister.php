<?php

namespace catalyst;

use Illuminate\Database\Eloquent\Model;

class CoachMapRegister extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'coachee_information';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'coachee_id', 'manager_name', 'home_address','work_address','work_email','contact_preference','jobTitle','work-experience','Time-experience','dob','maritialstatus','hobbies'
    ];
}
