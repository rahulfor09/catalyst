<?php

namespace catalyst;

use Illuminate\Database\Eloquent\Model;

class RecentReviewView extends Model
{
    protected $fillable = [
        'video_id', 'user_id', 'last_comment',
    ];

    public function Video()
    {
        return $this->hasOne('catalyst\Video','id','video_id');//->first();
    }
}
