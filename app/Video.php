<?php

namespace catalyst;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'video_file','user_id','background','ask_question','video_image',
    ];

    public function Owner()
    {
        return $this->hasOne('catalyst\User','id','user_id');
    }
}
