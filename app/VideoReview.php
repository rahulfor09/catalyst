<?php

namespace catalyst;

use Illuminate\Database\Eloquent\Model;

class VideoReview extends Model
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'video_reviewed';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'video_id', 'mentor_id', 'status',
    ];


    public function User()
    {
        return $this->belongsTo('catalyst\User','mentor_id');
    }

    public function Owner()
    {
        return $this->hasOne('catalyst\User','id','user_id');
    }

    public function Video()
    {
        return $this->hasOne('catalyst\Video','id','video_id');
    }
}
