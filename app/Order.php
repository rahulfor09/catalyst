<?php

namespace catalyst;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'video_id', 'total_price', 'total_characters','total_mentor','status',
    ];
}
