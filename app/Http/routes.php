<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use catalyst\Http\VideoStream;


Route::resource('/', 'HomeController');

Route::get('siteimage/{fldname}/{filename}', function($fldname="",$filename=""){
    $path = storage_path() . '/app/public/'.$fldname.'/' . $filename;
     
    if(!File::exists($path)) { return response()->json(['message' => 'Image not found.'], 404); }
    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});


Route::get('site/{fldname}/{filename}', function($fldname="",$filename=""){
    $path = storage_path() . '/app/public/'.$fldname.'/' . $filename;
     
    if(!File::exists($path)) { return response()->json(['message' => 'video not found.'], 404); }
    $file = File::get($path);
    $type = File::mimeType($path);
    //echo $type; die;

   //echo "<pre>"; print_r($file); die;
    if($fldname=="videos"){

        $obj = new VideoStream($path,$type);
        $obj->start();
        /*echo $path;
        echo "<pre>"; print_r($obj); die;

        header('Content-Description: File Transfer');
        header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
        #header("Cache-Control: no-store, public, must-revalidate, max-age=86400");
        header("Pragma: public");
        header('Accept-Ranges: bytes');
        header('Cache-Control: max-age=604800');
        header("Content-Type: ".$type);
        //header('Content-Type: application/octet-stream');
        header("Content-Disposition: inline; filename=" .$filename);
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: ".filesize($path));
        readfile($path);die();*/
    }else{
        $response = Response::make($file, 200);
        $response->header("Expires", "Thu, 19 Nov 1981 08:52:00 GMT");
        $response->header("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
        $response->header("Pragma", "no-cache");
        $response->header("Content-Disposition", "inline; filename=".$filename);
        $response->header("Content-Length", filesize($path));
        $response->header("Content-Type", $type);
        return $response;
    }
});


Route::get('login', 'UserController@user_login');
Route::post('login',array('as'=>'user.login','uses'=>'UserController@login'));

Route::get('logout', array('uses' => 'UserController@logout'));
Route::get('register', function(){return View::make('users.user_register');});
Route::get('register', 'UserController@registerForm');
Route::post('register', array('as'=>'user.register','uses'=>'UserController@register'));
Route::get('clientinfo', function(){return View::make('users.cleint_information_form');});
Route::post('clientInfo', 'UserController@userNextForm');
Route::get('forgotpassword', function(){return View::make('users.user_forgotpassword');});
Route::post('forgotpassword', 'UserController@forgotPassword');


Route::get('contact', 'PageController@contact');
Route::get('about', 'PageController@about');

Route::post('newsletter', 'PageController@newsletter');
Route::post('contactus', 'PageController@contactUs');

Route::post('profilephoto',  array('as'=>'profilephoto','uses'=>'UserController@profilePhoto'))->middleware('auth');
Route::get('profile/{username}',  array('as'=>'profiledetail','uses'=>'UserController@profileDetail'));

//coupon
Route::post('couponcheck',  array('as'=>'couponcheck','uses'=>'CouponController@checkCoupon'));
//Login user information
Route::group(['middleware' => ['auth','normalusersauth']], function () {

    Route::get('dashboard', array('as'=>'user.dashboard','uses'=>'UserController@dashboard'));
    Route::get('profile', array('as'=>'user.profile','uses'=>'UserController@userProfile'));
    Route::post('profile', array('as'=>'user.profile','uses'=>'UserController@updateUserProfile'));
    Route::resource('video', 'VideoController');
    Route::get('video/{videoid}/process', array('as'=>'video.process','uses'=>'VideoController@process'));
    Route::post('video/{videoid}/processing', array('as'=>'video.processing','uses'=>'VideoController@processing'));
    Route::post('export', array('as'=>'video.export','uses'=>'VideoController@export'));
    Route::get('video/{videoid}/comment/{mentorid?}', array('as'=>'video.comment','uses'=>'VideoController@videoComments'));

    Route::delete('video/{videoid}/destroy', array('as'=>'video.destroy','uses'=>'VideoController@destroy'));
    Route::get('videos/reviewed', array('as'=>'video.reviewed','uses'=>'VideoController@reviewed'));
    
    Route::get('password/{id}', array('as'=>'user.password','uses'=>'UserController@updatePassword'));
    Route::post('updatepassword/{id}', array('as'=>'user.updatepassword','uses'=>'UserController@updatePasswordSave'));
    Route::get('archieve', array('as'=>'video.archieve','uses'=>'VideoController@showarchive'));

});


//Mentor router with login
Route::group(['prefix' => 'coach','middleware' => ['auth','mentorsauth']], function () {
    
    Route::get('dashboard', array('as'=>'mentor.dashboard','uses'=>'MentorController@dashboard'));
    Route::get('profile', array('as'=>'mentor.profile','uses'=>'MentorController@profile'));
    Route::post('profile',  array('as'=>'mentor.profile','uses'=>'MentorController@updateProfile'));

    Route::get('video/{videoid}/detail', array('as'=>'video.detail','uses'=>'VideoController@detail'));
    
    Route::get('video/{videoid}/comments', array('as'=>'video.comments','uses'=>'VideoController@comments'));
    Route::post('video/{videoid}/comments', array('as'=>'video.comments','uses'=>'VideoController@addComments'));
    Route::post('video/comments/{videoid}/confirm', array('as'=>'video.comments.confirm','uses'=>'VideoController@confirmComments'));
    Route::post('video/comment', array('as'=>'video.comment.delete','uses'=>'VideoController@deleteComments'));
    Route::post('video/updatecomment', array('as'=>'video.updatecomment','uses'=>'VideoController@upudateComments'));

    Route::get('videos/review', array('as'=>'videos.review','uses'=>'VideoController@review'));
    Route::get('videos/reviewed', array('as'=>'videos.reviewed','uses'=>'VideoController@reviewedMentor'));
    
    Route::get('password/{id}', array('as'=>'mentor.password','uses'=>'UserController@updatePassword'));
    Route::post('updatepassword/{id}', array('as'=>'mentor.updatepassword','uses'=>'UserController@updatePasswordSave'));
    Route::get('archieve', array('as'=>'mentor.archieve','uses'=>'VideoController@showarchiverMentor'));
});


//Admin routers

Route::group(['prefix' => 'admin','middleware' => ['auth','administrator']], function () {

    Route::get('dashboard', array('as'=>'admin.dashboard','uses'=>'AdminController@dashboard'));
    Route::get('profile', array('as'=>'admin.profile','uses'=>'AdminController@profile'));
    Route::post('profile',  array('as'=>'admin.profile','uses'=>'AdminController@updateProfile'));

    Route::get('videos', array('as'=>'admin.videos','uses'=>'VideoController@adminIndex'));
    Route::get('video/{videoid}', array('as'=>'admin.video.show','uses'=>'VideoController@adminShow'));
    Route::delete('video/{usersid}/delete', array('as'=>'admin.video.delete','uses'=>'VideoController@adminDelete'));

    //coupons Management
    Route::get('coupons', array('as'=>'admin.coupon','uses'=>'CouponController@adminIndex'));
    Route::get('coupon/{couponid}/edit', array('as'=>'admin.coupon.show','uses'=>'CouponController@adminShow'));
    Route::get('coupon/create', array('as'=>'admin.coupon.create','uses'=>'CouponController@adminCreate'));
    Route::post('coupon/{couponid}/update', array('as'=>'admin.coupon.update','uses'=>'CouponController@adminUpdate'));
    Route::post('coupon/store', array('as'=>'admin.coupon.store','uses'=>'CouponController@adminStore'));
    Route::delete('coupon/{couponid}/delete', array('as'=>'admin.coupon.delete','uses'=>'CouponController@adminDelete'));
    
    
    Route::get('users', array('as'=>'admin.users','uses'=>'UserController@adminIndex'));
    Route::get('users/create', array('as'=>'admin.users.create','uses'=>'UserController@adminCreate'));
    Route::post('users/store', array('as'=>'admin.users.store','uses'=>'UserController@adminStore'));
    Route::get('users/{usersid}/edit', array('as'=>'admin.users.edit','uses'=>'UserController@adminEdit'));
    Route::post('users/{usersid}/update', array('as'=>'admin.users.update','uses'=>'UserController@adminUpdate'));
    Route::delete('users/{usersid}/delete', array('as'=>'admin.users.delete','uses'=>'UserController@adminDelete'));


    
    Route::get('mentors', array('as'=>'admin.mentors','uses'=>'MentorController@adminIndex'));
    Route::get('mentors/{usersid}/edit', array('as'=>'admin.mentors.edit','uses'=>'MentorController@adminEdit'));
    Route::get('mentors/{usersid}/password', array('as'=>'admin.mentors.password','uses'=>'MentorController@adminPassword'));
    Route::get('mentors/create', array('as'=>'admin.mentors.create','uses'=>'MentorController@adminCreate'));
    Route::post('mentors/save', array('as'=>'admin.mentors.save','uses'=>'MentorController@adminSave'));
    Route::post('mentors/{usersid}/update', array('as'=>'admin.mentors.update','uses'=>'MentorController@adminUpdate'));
    Route::post('mentors/{usersid}/updatepassword', array('as'=>'admin.mentors.updatepassword','uses'=>'MentorController@adminUpdatePassword'));
    Route::delete('mentors/{usersid}/delete', array('as'=>'admin.mentors.delete','uses'=>'MentorController@adminDelete'));
    
    //Category Information
    Route::get('category', array('as'=>'admin.category','uses'=>'CategoryController@adminIndex'));
    Route::get('category/create', array('as'=>'admin.category.create','uses'=>'CategoryController@adminCreate'));
    Route::post('category/store', array('as'=>'admin.category.store','uses'=>'CategoryController@adminStore'));
    Route::get('category/{categoryid}/edit', array('as'=>'admin.category.edit','uses'=>'CategoryController@adminEdit'));
    Route::post('category/{categoryid}/update', array('as'=>'admin.category.update','uses'=>'CategoryController@adminUpdate'));
    Route::delete('category/{categoryid}/delete', array('as'=>'admin.category.delete','uses'=>'CategoryController@adminDelete'));
    
    //Package Information
    Route::get('package', array('as'=>'admin.package','uses'=>'PackageController@adminIndex'));
    Route::get('package/create', array('as'=>'admin.package.create','uses'=>'PackageController@adminCreate'));
    Route::post('package/store', array('as'=>'admin.package.store','uses'=>'PackageController@adminStore'));
    Route::get('package/{categoryid}/edit', array('as'=>'admin.package.edit','uses'=>'PackageController@adminEdit'));
    Route::post('package/{categoryid}/update', array('as'=>'admin.package.update','uses'=>'PackageController@adminUpdate'));
    Route::delete('package/{packageid}/delete', array('as'=>'admin.package.delete','uses'=>'PackageController@adminDelete'));


});



    /*App::missing(function($exception)
    {
        return Response::view('error', array(), 404);
    });*/


