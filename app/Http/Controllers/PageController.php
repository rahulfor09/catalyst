<?php

namespace catalyst\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Auth;
use catalyst\Http\Requests;
use Mail;
use catalyst\User;

class PageController extends Controller
{
    
    public function __construct(){
		parent::__construct();
	}
 
    public function newsletter(Request $request)
    {
        $data = $request->all();
	    $rules = array(
				'txtemail' => 'required|email'
		     	);
	    $validator = Validator::make($data, $rules);
	    if ($validator->fails()){
	      
	      return response()->json(
	      		array(
	      			"status"=>"failed",
	      			"message"=> "Validation Failed",
	      			'errors'=> $validator->errors()
	      			)
	      		);
	    } else {

	    	return response()->json(
	    			array(
		      			"status"=>"success",
		      			"message"=> "You have successfully subscribe."
	      			)
	    		);
	    }
        
    }


    public function contactUs(Request $request)
    {
        $data = $request->all();
	    $rules = array(
				'first_name' => 'required',
				'last_name' => 'required',
				'phone_no' => 'required',
				'contact_email' => 'required|email',
				'comment' => 'required|min:10',
		     	);
	    $validator = Validator::make($data, $rules);
	    if ($validator->fails()){
	      
	      return response()->json(
	      		array(
	      			"status"=>"failed",
	      			"message"=> "Validation Failed",
	      			'errors'=> $validator->errors()
	      			)
	      		);
	    } else {


	    	//Send mail to user
	    	$user = $request->all();
	    	//Admin Mail
	        Mail::send('emails.contactus', ['user' => $user], function ($message) use ($user)
	         {	            
	            $message->to("moudeepta@thepauseplay.com", "The Mirror")->subject('Contact by '.$user["first_name"]);
	        });

	        //User Mail
	        Mail::send('emails.contact_thanks', ['user' => $user], function ($message) use ($user)
	         {	            
	            $message->to($user["contact_email"], $user["first_name"])->subject('Thank you for contacting');
	        });

	    	return response()->json(
	    			array(
		      			"status"=>"success",
		      			"message"=> "Thanks for feedback, We will contact you soon."
	      			)
	    		);
	    }
        
    }

    public function contact(Request $request){
    	return view('pages.contact');
    }

    public function about(Request $request){

    	$data=array();
        $data["ourmentor"]=User::where('user_type', "M")->paginate(15);

    	return view('pages.about',compact('data'));
    }
}
