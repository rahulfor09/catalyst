<?php

namespace catalyst\Http\Controllers;

use catalyst\Coupon;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
use Validator;
use function redirect;
use function view;
use catalyst\Package;
use catalyst\Couponuse;

class CouponController extends Controller
{
	public function __construct(){
        parent::__construct();
		//$this->middleware('auth', ['except' => ['index','show']]);
		$this->middleware('auth', ['except' => []]);
        //$this->afterFilter("no-cache", ["only"=>["create","show","process","detail"]]);
        //\Artisan::call('cache:clear');
        //\Cache::flush();
	}

    public function index(Request $request) {
        $userid = Auth::user()->id;
    	//$couponsdatas=Coupon::select()->leftJoin('recent_review_views', 'coupons.id', '=', 'recent_review_views.video_id')->where("coupons.user_id","=",$userid)->where("recent_review_views.user_id","=",null)->orderby("created_at","desc")->paginate(10);
    	$couponsdatas=Coupon::select()->orderby("created_at","desc")->paginate(10);

        //echo "<pre>ddddd";  print_r($couponsdatas); die;
	return view('coupons.index', compact('couponsdatas'));
    }





    public function create(Request $request) {
        //echo phpinfo();die;
    	$data=array();       
		return view('coupons.create', compact('data'));
    }


    public function show(Request $request,$id) {
        $data=array();
        $videoinfo = Coupon::where("id","=",$id)->firstOrFail();

        if(isset($videoinfo->id)){
            //$data["video"]=$videoinfo->first();
            $data["video"]=$videoinfo;
            $data["mentors"]=CouponReview::where("video_id","=",$id)->groupBy('mentor_id')->get();
            if(count($data["mentors"])<=0){
                return redirect()->route('video.process',$id)->with("message","Please select the mentor and plan.");
            }
            $data["video_comment"] = CouponComment::where(array("video_id"=>$id,"status"=>"1"))->orderby("created_at","DESC")->get();
            foreach($data["video_comment"] as &$dd){
                $dd->mentors=$dd->Mentor();
            }
            
        }else{
            

        }
        return view('coupons.show', compact('data'));
    }

    public function edit($id,$viewmode='default') {

        $data=array();
        $videoinfo = Coupon::where("id","=",$id);
        if($videoinfo->exists()){
            $data["video"]=$videoinfo->first();
            if($viewmode=='innerdetail'){
                return ($data["video"]);
            }
        }else{
            
        }
       
        return view('coupons.edit', compact('data')); 
    }



    
    public function detail(Request $request,$videoid) {
          
        $data=array();
        $userid = Auth::user()->id;
        $videoreview= CouponReview::where(array("video_id"=>$videoid,"mentor_id"=>$userid));
        $data["videoid"]=$videoid;

        if($videoreview->exists()){
            $data["video"] = Coupon::where("id","=",$videoid)->first();
            $data["latestadded"] = CouponReview::where("mentor_id","=",$userid)->orderby("created_at","desc")->take(5)->get();

            $data["mentors"]=$videoreview->first();
            $data["categories"]=Coupon::where("status","1")->orderby("title",'ASC')->lists('title','id');

        }else{
            return redirect()->route('mentor.dashboard')->with("message","You are not authorize for access video");
        }
       
        return view('coupons.detail', compact('data'));
    }




    public function deleteComments(Request $request) {
        
        $postdata= $request->all();
        $commentid=isset($postdata["id"])?$postdata["id"]:"0";
        $userid = Auth::user()->id;
        $affectedRows = CouponComment::where(array("user_id"=>$userid,"id"=>$commentid))->delete();
        return \Response::json(array("status"=>$affectedRows));
    }

    public function confirmComments(Request $request,$videoid) {
        $userid = Auth::user()->id;
        $affectedRows = CouponComment::where(array("video_id"=>$videoid,"user_id"=>$userid))->update(array("status"=>"1"));

        if($affectedRows>0){

            $videodata= Coupon::where("id","=",$videoid)->first();
            $userinfo= $videodata->Owner()->first();
            //print_r($userinfo); die;

            //Send mail to user
                Mail::send('emails.usernotification', ['user' => $userinfo], function ($message) use ($userinfo)
                 {
                    $message->to($userinfo->email, $userinfo->name)->subject('Coupon Commented on TheMirror');
                });
        }

        return \Response::json(array("status"=>$affectedRows));
    }


    //Admin function list

    public function adminIndex(Request $request) {

        $userid = Auth::user()->id;
        //$couponsdatas = Coupon::orderby("created_at","desc")->paginate(10);
        $couponsdatas=Coupon::select()
              ->orderby("created_at","desc")->paginate(10);
        return view('coupons.admin.index', compact('couponsdatas'));
    }

    public function adminCreate()
    {
        $data=array();
        $packagePlan = array();
        $planList = Package::select()->get();
        foreach($planList as $k=> $plan){
            $packagePlan[$k]['id'] = $plan['id'];
            $packagePlan[$k]['title'] = $plan['title'];
        }
        return view('coupons.admin.create', compact('data','packagePlan'));
    }
    public function adminShow(Request $request,$id) {
          
        $data=array();
        $packagePlan = array();
        $planList = Package::select()->get();
        foreach($planList as $k=> $plan){
            $packagePlan[$k]['id'] = $plan['id'];
            $packagePlan[$k]['title'] = $plan['title'];
        }
        $videoinfo = Coupon::where("id","=",$id);
        if($videoinfo->exists()){
            $data["coupon"]=$videoinfo->first();
        }else{
            
        }
        return view('coupons.admin.show', compact('data','packagePlan'));
    }

    public function adminUpdate(Request $request,$id)
    {
        $data = $request->all();
        $rules = array(
                'code' => 'required',
                );
        $validator = Validator::make($data, $rules);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{

            //$categorydata = $request->only('code');
            $catfind = Coupon::whereId($id)->firstOrFail();
            $catfind->fill($data);
            $catfind->save();

            return redirect()->route('admin.coupon')->with("message","Coupon has been successfully updated.");
        }
    }
    
    public function adminStore(Request $request)
    {
        $data = $request->all();
        $rules = array(
                'code' => 'required',
                'description' => 'required',
				'package_type' => 'required',
                'no_of_use' => 'required',
                'from_date' => 'required',
                'to_date' => 'required',
                );
        $validator = Validator::make($data, $rules);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{

            $reqdata = $request->only('code','description','package_type','no_of_use','from_date','to_date');

            $catfind = new Coupon();
            $catfind->fill($reqdata);
            $catfind->save();
            return redirect()->route('admin.coupon')->with("message","Coupon has been successfully save.");
        }

    }
 
    //check code for coupon
    public function checkCoupon(Request $request) {
      //getting user id limit resctriction
      $userid = Auth::user()->id;
      $data = $request->all();
      $couponinfo = Coupon::where(array("code"=>$data['code'],'status'=>1))
              ->whereDate('to_date', '>=', Carbon::now())
              ->first();
      if($couponinfo != ''){
            $no_of_use = $couponinfo->no_of_use;
            $coupon_id = $couponinfo->id;
            $packageName = $couponinfo->package_type;
            if(isset($couponinfo->id)){
                //check for no. of use
                $timesofUse = Couponuse::where(array("user_id"=>$userid,'coupon_id'=>$coupon_id))
                    ->count();
                if($timesofUse >=$no_of_use){
                    return response()->json(['status' => 'fail']);
                }
                else{
                    return response()->json(['status' => 'success','packageName'=>$packageName]);
                }
            }
            else{
                return response()->json(['status' => 'fail']);
            }
        }else{
            return response()->json(['status' => 'fail']);
        }
    }
    
    public function adminDelete(Request $request,$videoid)
    {
        $videoinfo = Coupon::where(array("id"=>$videoid))->firstOrFail();
        if(isset($videoinfo->id))
        {
            $videoinfo->delete();
            return redirect()->back()->with("message","Coupon has been deleted successfull");
        }    

    }
    
    
}
