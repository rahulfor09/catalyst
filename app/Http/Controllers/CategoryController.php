<?php

namespace catalyst\Http\Controllers;

use Illuminate\Http\Request;
use catalyst\Http\Requests;

use Validator;

use catalyst\Category;

class CategoryController extends Controller
{
    public function __construct(){
       parent::__construct();
    }
    
    /**
     * Display a home of the resource.
     *
     * @return Response
     */

    public function adminIndex()
    {
        $data=array();
        $alldata=Category::orderby("created_at","desc")->paginate(10);
        return view('category.admin.index', compact('alldata'));
    }

    public function adminCreate()
    {
        $data=array();
        return view('category.admin.create', compact('data'));
    }

    public function adminStore(Request $request)
    {
        $data = $request->all();
        $rules = array(
                'title' => 'required',
                );
        $validator = Validator::make($data, $rules);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{

            $categorydata = $request->only('title');

            $catfind = new Category();
            $catfind->fill($categorydata);
            $catfind->save();
            return redirect()->route('admin.category')->with("message","Category has been successfully save.");
        }

    }
 


    public function adminEdit($id) {

        $data=array();
        $datainfo = Category::where("id","=",$id);
        if($datainfo->exists()){
            $data["category"]=$datainfo->first();
        }
       
        return view('category.admin.edit', compact('data')); 
    }

     public function adminUpdate(Request $request,$id)
    {
        $data = $request->all();
        $rules = array(
                'title' => 'required',
                );
        $validator = Validator::make($data, $rules);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{

            $categorydata = $request->only('title');
            $catfind = Category::whereId($id)->firstOrFail();
            $catfind->fill($categorydata);
            $catfind->save();

            return redirect()->route('admin.category')->with("message","Category has been successfully updated.");
        }
    }


    public function adminDelete(Request $request,$id)
    {
        $catinfo = Category::where(array("id"=>$id))->firstOrFail();
        if(isset($catinfo->id)){
            $catinfo->delete();
            return redirect()->route("admin.category")->with("message","Category has been deleted successfull");
        }else{
            return redirect()->back()->with("errormessage","Category has deletion failed.");
        }
    }

}
