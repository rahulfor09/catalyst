<?php

namespace catalyst\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Mail;
use catalyst\Http\Requests;
use Illuminate\Support\Facades\Hash;
use catalyst\User;
use catalyst\Profile;
use catalyst\Video;
use catalyst\Coupon;
use catalyst\CoachMapCoachee;
use catalyst\CoachMapRegister;
use catalyst\Payment;
use catalyst\Helpers\ItfVideo;

class UserController extends Controller {

    public function __construct() {
        parent::__construct();
        $this->middleware('auth', ['except' => ['user_login', 'login', 'register', 'registerForm','forgotPassword', 'profileDetail','userNextForm']]);
    }

    public function user_login(Request $request) {
        
        return view('users.user_login');
    }

    public function login(Request $request) {

        

        $data = $request->all();
       // echo"<pre>";print_r($data);die();
        $rules = array(
            'email' => 'required',
            'password' => 'required|min:6',
        );


        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
        
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $userdata = $request->only('email', 'password');
                            if (Auth::validate( $userdata)) {

                        $user = Auth::getLastAttempted();
                        if ($user->active) {
                            Auth::login($user, $request->has('remember'));

                         return redirect()->intended($this->redirectPath());
                        } else {

                        
        }

                    if ($user->user_type == "M")
                     
                         return view('mentors.dashboard');

                    elseif ($user->user_type == "C")
                      
                        return view('mentors.admin.dashboard');
                    else
                      
                         return view('users.dashboard');
                    return redirect()->route('admin.mentors')->with("message", "You have succesfully login.");
                    
                }
          
            else {
              
                 return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors([
                'approve' => 'Invalid username and Password.',]);

            }
        }
    }

     public function registerForm(Request $request){
        $coachList = User::where('user_type','C')->where('status',1)->get();
        return view('users.user_register', compact('coachList'));
     }
    
    public function register(Request $request) {

        $data = $request->all();
        //echo "<pre>";print_r($data);die;
       $rules = array(
            'name' => 'required',
            'coach_sponsor' => 'required',
            'company' => 'required',
            'phone_no' => 'required|max:10',
            'email' => 'required|email|unique:users',
            'password' => 'required|same:confirm-password|min:6',
            'confirm-password' =>'required|min:6',
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
            
        } else {

            $data = $request->only('name', 'coach_sponsor', 'company', 'phone_no', 'email', 'password');
            $data["user_type"] = ($request->get("accounttype") == "C") ? "C" : "U";

            $tmpdata = $data;

            $data['password'] = Hash::make($data['password']);
            $data['username'] = "C" . time() . ItfVideo::randomString(5);
            // echo "<pre>";print_r($data);die;
            $user = User::create($data);

            if(isset($user->id)){
                foreach($request->input('coach_name') as $coachId){

                    $saveData = [
                        'coachee_id'=>$user->id,
                        'coach_id'=>$coachId,
                        'status'=> '1'
                    ];
                $userMapToCoach = CoachmapCoachee::create($saveData);
                }
                

/*Mail::send('emails.register', ['user' => $tmpdata], function ($message ) use ($user)
              {
                 $message->to($user["email"], $user["coachee"])->subject('Welcome to Catalyst');
             });*/




                
                if(isset($userMapToCoach->id)){
                    $data['coachee_id']=$user->id;
                    //echo "<pre>";print_r($data);die;
                    return view('users.client_information_form',array('data'=>$data));
                }
                
                
            }else{
                return redirect()->route("user.registes")->with("errormessage", "Something went wrong to save the cochee data.");
            }

            if ($data["user_type"] == "C") {
                $profile = Profile::create(array("description" => ""));
                $profile->user()->associate($user);
                $profile->save();
            }

            //Auto Login
            $userdata = $request->only('email', 'password');
            if (Auth::validate($userdata)) {
                if (Auth::attempt($userdata, $request->has('remember'))) {
                    
                }
            }
            //Auto Login End
          //  Send mail to user
          //echo "<pre>";print_r($data);die;   
           Mail::send('emails.register', ['user' => $tmpdata], function ($message ) use ($user)
              {
                 $message->to($user["email"], $user["coachee"])->subject('Welcome to Catalyst');
             });

            return response()->json(array("status" => "success"));
        }
    }



     /*public function usernextform(Request $request) {

        $data = $request->all();
        //echo "<pre>";print_r($data);die;
        $rules = array(
            'name' => 'required',
            'coach_sponsor' => 'required',
            'company' => 'required',
            'phone_no' => 'required|max:10',
            'email' => 'required|email',
            'password' => 'required|same:confirm-password|min:6',
            'confirm-password' =>'required|min:6',
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
            
        } else {

            $data = $request->only('name', 'coach_sponsor', 'company', 'phone_no', 'email', 'password');
            $data["user_type"] = ($request->get("accounttype") == "C") ? "C" : "U";

            $tmpdata = $data;

            $data['password'] = Hash::make($data['password']);
            $data['username'] = "C" . time() . ItfVideo::randomString(5);
            // echo "<pre>";print_r($data);die;
            $user = User::create($data);

            if(isset($user->id)){
                foreach($request->input('coach_name') as $coachId){

                    $saveData = [
                        'coachee_id'=>$user->id,
                        'coach_id'=>$coachId,
                        'status'=> '1'
                    ];
                $userMapToCoach = CoachmapCoachee::create($saveData);
                }
                if(isset($userMapToCoach->id)){
                    return view('users.client_information_form');
                }
                
                
            }else{
                return redirect()->route("user.registes")->with("errormessage", "Something went wrong to save the cochee data.");
            }

            if ($data["user_type"] == "C") {
                $profile = Profile::create(array("description" => ""));
                $profile->user()->associate($user);
                $profile->save();
            }

            //Auto Login
            $userdata = $request->only('email', 'password');
            if (Auth::validate($userdata)) {
                if (Auth::attempt($userdata, $request->has('remember'))) {
                    
                }
            }
            //Auto Login End
          //  Send mail to user
            Mail::send('emails.register', ['user' => $tmpdata], function ($message) use ($user)
              {
                 $message->to($user["email"], $user["coachee"])->subject('Welcome to Catalyst');
             });

            return response()->json(array("status" => "success"));
        }
    }
    */


     public function usernextform(Request $request) {

           $data = $request->all();
       //echo "<pre>WWWW";print_r($data);die;
        $rules = array(
            'home_address' => 'required',
            'work-experience' => 'required',
            'contact_preference' => 'required',
            'dob' => 'required',
            //'Hobbies' => '',
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
            
        } else {
            $tmpdata = $data;
            $data = $request->only( 'coachee_id','manager_name', 'home_address','work_address','work_email','contact_preference','jobTitle','work-experience','Time-experience','dob','maritialstatus','hobbies');
            
           // echo "<pre>";print_r($data);die;
            


          
         if($tmpdata['coachee_id']>0){
                
                $userMapToCoach = CoachMapRegister::create($data);
                
                if(isset($userMapToCoach->id)){
                    return view('users.thankyou');
                }
                
                
            else{
                return redirect()->route("users.client_information_form")->with("errormessage", "Something went wrong to save the cochee data.");
            }

         
    }
    }
    }

    public function logout() {
        Auth::logout();
        return \Redirect::to('/');
    }

    public function dashboard() {

        $data = array();
        $userid = Auth::user()->id;
        $data["latestadded"] = '';
        //  $data["latestadded"] = Video::select(array('videos.*','video_reviewed.created_at as assingdated'))
        //                  ->leftJoin('recent_review_views', 'videos.id', '=', 'recent_review_views.video_id')
        //                  ->leftJoin('video_reviewed', 'videos.id', '=', 'video_reviewed.video_id')
        //                  ->where("videos.user_id","=",$userid)
        //                  ->where("recent_review_views.user_id","=",null)
        //                  ->groupby("videos.id")
        //                  ->orderby("videos.created_at","desc")->take(11)->get();
        $data["reviewedfile"] = '';
        //  $data["reviewedfile"] = Video::select(array("videos.*",'recent_review_views.last_comment','video_reviewed.created_at as assingdated'))
        //                                     ->join('video_reviewed', 'videos.id', '=', 'video_reviewed.video_id')
        //                                     ->join('recent_review_views', 'videos.id', '=', 'recent_review_views.video_id')
        //                                     ->where("videos.user_id","=",$userid)
        //                                     ->orderby("created_at","desc")
        // 									->take(11)->get();
        $data['couponCode'] = '';
        //  $data['couponCode'] = Coupon::select(array('code','videos.id as videoId'))
        // 						 ->join('coupon_use','coupons.id','=','coupon_use.coupon_id')
        // 						 ->join('videos','coupon_use.video_id','=','videos.id')
        // 						 ->where('coupon_use.user_id',"=",$userid)
        // 						 ->groupby('videos.id')
        // 						 ->get();
        $data['payment'] = '';
        //  $data['payment'] = Payment::select(array('status','videos.id as videoId','razor_payment_id','amount','payments.created_at'))
        // 						 ->join('videos','payments.video_id','=','videos.id')
        // 						 ->where('payments.user_id',"=",$userid)
        // 						 ->get();
        $data['mentors'] = '';
        //  $data['mentors'] = Video::select(array('users.id as userId','users.name as mentorName','users.last_name as mentorlastName','videos.id as videoId'))
        // 						 ->join('video_reviewed','videos.id','=','video_reviewed.video_id')
        // 						 ->join('users','users.id','=','video_reviewed.mentor_id')
        // 						->where('users.user_type','=','M')
        // 						->get();
        return view('users.dashboard', compact('data'));
    }

    public function forgotPassword(Request $request) {

        $data = $request->all();
        $rules = array('email' => 'required',);
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return response()->json(array(
                        "status" => "failed",
                        "message" => "Please enter the email id",
                        'errors' => $validator->errors()
                            )
            );
        } else {

            $resinfo = array(
                "status" => "failed",
                "message" => "Your email id is not available."
            );

            $emailid = $request->only('email');
            $userinfo = User::where('email', $emailid)->first();

            if (isset($userinfo->id)) {
                $newpassword = ItfVideo::randomString(6);
                //echo $newpassword;
                $userinfo->fill(array("password" => Hash::make($newpassword)))->save();
                //print_r($userinfo); die;
                //Send mail to user
                /*Mail::send('emails.forgot', ['user' => $userinfo, 'newpassword' => $newpassword], function ($message) use ($userinfo, $newpassword) {
                    $message->to($userinfo->email, $userinfo->name)->subject('Your New Password for catalyst');
                });*/

                $resinfo = array(
                    "status" => "success",
                    "message" => "Your password has been sent on your register email."
                );
            }
            return response()->json($resinfo);
        }
    }

    public function userProfile(Request $request) {

        $data = array();
        $id = Auth::user()->id;
        $data["userinfo"] = User::where("id", "=", $id)->first();
        return view('users.user_profile', compact('data'));
    }

    public function updateUserProfile(Request $request) {

        $data = $request->all();
        $rules = array(
            'name' => 'required',
            'last_name' => 'required',
            'phone_no' => 'required|min:10',
        );
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $id = Auth::user()->id;
            $userdata = $request->only('name', 'last_name', 'phone_no', 'company');
            $user = User::whereId($id)->firstOrFail();
            $user->fill($userdata);
            $user->save();
            return redirect()->back()->with("message", "Profile has been successfully updated.");
        }
    }

    public function profilePhoto(Request $request) {

        $data = array();
        $postdata = $request->all();

        $rules = array('profile_file_upload' => 'required|max:300000000|mimes:png,jpg,jpeg,gif',);
        $validator = Validator::make($postdata, $rules);
        if ($validator->fails()) {
            return response()->json(array("status" => "failed", "message" => "Profile image uploading failed.", "errors" => $validator->errors()));
        }

        try {
            $id = Auth::user()->id;
            $profile_file_upload = array_get($postdata, 'profile_file_upload');
            $extension = $profile_file_upload->getClientOriginalExtension();

            $profilephotoname = "profile" . $id . '.' . $extension;
            $destinationPath = storage_path() . '/app/public/users';
            $upload_success = $profile_file_upload->move($destinationPath, $profilephotoname);

            if ($upload_success) {
                User::where("id", $id)->update(array("user_photo" => $profilephotoname));
            }

            return response()->json(array("status" => "success", "imagename" => $profilephotoname));
        } catch (Exception $e) {
            return response()->json(array("status" => "error", "message" => "Profile image uploading failed", "error" => $e));
        }
    }

    public function profileDetail(Request $request, $username) {

        $data = array();
        $data["userinfo"] = User::where("username", "=", $username)->first();
        if (isset($data["userinfo"]->user_type) and $data["userinfo"]->user_type == "M")
            return view('mentors.profile_detail', compact('data'));
        else
            return view('users.profile_detail', compact('data'));
    }

    //Admin List of function

    public function adminIndex(Request $request) {

        $userinfo = User::where("user_type", "=", "N")->orderby("created_at", "desc")->paginate(10);
        return view('users.admin.index', compact('userinfo'));
    }

    public function adminCreate() {
        $data = array();
        return view('users.admin.create', compact('data'));
    }

    public function adminStore(Request $request) {
        $data = $request->all();
        $rules = array(
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone_no' => 'required',
        );
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $reqdata = $request->only('name', 'last_name', 'email', 'phone_no');

            $catfind = new User();
            $catfind->fill($reqdata);
            $catfind->save();
            return redirect()->route('admin.users')->with("message", "User has been successfully created.");
        }
    }

    public function adminEdit($id) {

        $data = array();
        $datainfo = User::where("id", "=", $id);
        if ($datainfo->exists()) {
            $data["user"] = $datainfo->first();
        }

        return view('users.admin.edit', compact('data'));
    }

    public function adminUpdate(Request $request, $id) {
        $data = $request->all();
        $rules = array(
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'phone_no' => 'required',
        );
        $validator = Validator::make($data, $rules);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $reqdata = $request->only('name', 'last_name', 'email', 'phone_no', 'status');
            $catfind = User::whereId($id)->firstOrFail();
            $catfind->fill($reqdata);
            $catfind->save();

            return redirect()->route('admin.users')->with("message", "User has been successfully updated.");
        }
    }

    public function adminDelete(Request $request, $id) {
        $catinfo = User::where(array("user_type" => "N", "id" => $id))->firstOrFail();
        //getting uploaded videos
        if (isset($catinfo->id)) {
            $anyVideoExist = Video::select(array('videos.id'))
                    ->where(array("videos.user_id" => $catinfo->id))
                    ->firstOrFail();
            if ((isset($anyVideoExist->id)) && ($anyVideoExist->id > 0)) {
                return redirect()->route("admin.users")->with("message", "User deletion failed. User has uploaded video");
            } else {
                $catinfo->delete();
                return redirect()->route("admin.users")->with("message", "User has been deleted successfull");
            }
        } else {
            return redirect()->route("admin.users")->with("errormessage", "User has deletion failed.");
        }
    }

    public function updatePassword($id = 0) {
        $data = array();
        $datainfo = User::where("id", "=", $id);
        if ($datainfo->exists()) {
            $data["userinfo"] = $datainfo->first();
        }
        return view('users.update_password', compact('data'));
    }

    public function updatePasswordSave(Request $request, $id = 0) {
        $data = $request->all();
        $rules = array(
            'password' => 'required|same:confirm-password|min:6',
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $data['password'] = Hash::make($data['password']);
            $validator = Validator::make($data, $rules);

            $userdata = $request->only('password');

            $user = User::whereId($id)->firstOrFail();
            $user->fill($data);
            $user->save();
            if ($user->user_type === 'M')
                return redirect()->route('mentor.profile')->with("message", "Password has been successfully changed.");
            else
                return redirect()->route('user.dashboard')->with("message", "Password has been successfully changed.");
        }
    }

}
