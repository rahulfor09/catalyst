<?php

namespace catalyst\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Mail;
use catalyst\Http\Requests;
use Illuminate\Support\Facades\Hash;
use catalyst\User;
use catalyst\Profile;
use catalyst\Video;
use catalyst\VideoReview;
use catalyst\RecentReviewView;
use catalyst\Helpers\ItfVideo;

class MentorController extends Controller {

    public function __construct() {
        parent::__construct();
        $this->middleware('auth', ['except' => []]);
    }

    public function logout() {
        Auth::logout();
        return \Redirect::to('/');
    }

    public function dashboard() {

        $data = array();
        $userid = Auth::user()->id;
        //$data["latestadded"] = VideoReview::where("mentor_id","=",$userid)->orderby("created_at","desc")->take(5)->get();

        $data["latestadded"] = VideoReview::select('video_reviewed.*')
                        ->leftJoin('recent_review_views', function($joins) {
                            $joins->on('video_reviewed.video_id', '=', 'recent_review_views.video_id')
                            ->on('video_reviewed.mentor_id', '=', 'recent_review_views.user_id');
                        })->where("video_reviewed.mentor_id", "=", $userid)
                        ->where('recent_review_views.user_id', '=', null)
                        ->where('recent_review_views.video_id', '=', null)
                        ->orderby("created_at", "desc")->take(5)->get();

        $data["reviewedfile"] = RecentReviewView::select(array('recent_review_views.last_comment', 'video_reviewed.*'))
                        ->join('video_reviewed', 'recent_review_views.video_id', '=', 'video_reviewed.video_id')
                        ->join('videos', 'videos.id', '=', 'recent_review_views.video_id')
                        ->where("recent_review_views.user_id", "=", $userid)->groupBy('video_id')->orderby("recent_review_views.last_comment", "desc")->take(11)->get();

        //echo "<pre>"; print_r($data["reviewedfile"]); die;

        return view('mentors.dashboard', compact('data'));
    }

    public function profile(Request $request) {

        $data = array();
        $id = Auth::user()->id;
        $data["userinfo"] = User::where("id", "=", $id)->first();
        return view('mentors.profile', compact('data'));
    }

    public function updateProfile(Request $request) {

        $data = $request->all();
        $rules = array(
            'name' => 'required',
            'last_name' => 'required',
            'phone_no' => 'required|min:10',
            'description' => 'required|min:5',
        );
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $id = Auth::user()->id;
            $userdata = $request->only('name', 'last_name', 'phone_no');
            $user = User::whereId($id)->firstOrFail();
            $user->fill($userdata);
            $user->save();

            $profile = Profile::whereUserId($id)->firstOrFail();
            $profile->fill(array("description" => $data["description"], "post_title" => $data["post_title"], "total_experience" => $data["total_experience"]));
            $profile->save();
            return redirect()->back()->with("message", "Profile has been successfully updated.");
        }
    }

    //Mentor of Admin

    public function adminIndex(Request $request) {

        $userinfo = User::where("user_type", "=", "M")->orderby("created_at", "desc")->paginate(10);
        return view('mentors.admin.index', compact('userinfo'));
    }

    public function adminEdit($id) {

        $data = array();
        $datainfo = User::where("id", "=", $id);
        if ($datainfo->exists()) {
            $data["userinfo"] = $datainfo->first();
        }

        return view('mentors.admin.edit', compact('data'));
    }

    //password change
    public function adminPassword($id) {
        $data = array();
        $datainfo = User::where("id", "=", $id);
        if ($datainfo->exists()) {
            $data["userinfo"] = $datainfo->first();
        }
        return view('mentors.admin.password', compact('data'));
    }
    
    //reset password
    public function adminUpdatePassword(Request $request, $id) {
        $data = $request->all();
        $rules = array(
            'password' => 'required|same:confirm-password|min:6',
        );

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $data['password'] = Hash::make($data['password']);
            $validator = Validator::make($data, $rules);

            $userdata = $request->only('password');
            //$userdata = $request->only('name','last_name','phone_no');
            $user = User::whereId($id)->firstOrFail();
            $user->fill($data);
            $user->save();
            $profile = Profile::whereUserId($id)->firstOrFail();
            $profile->save();
            return redirect()->route('admin.mentors')->with("message", "Password has been successfully changed.");
        }
    }

    // create mentor now
    public function adminCreate($id = 0) {
        $data = array();
        $datainfo = User::where("id", "=", $id);
        if ($datainfo->exists()) {
            $data["userinfo"] = $datainfo->first();
        }
        return view('mentors.admin.create', compact('data'));
    }

    public function adminUpdate(Request $request, $id) {
        $data = $request->all();
        $rules = array(
            'name' => 'required',
            'last_name' => 'required',
            'phone_no' => 'required|min:10',
            'description' => 'required|min:5',
            //'email' => 'required|email|unique:users,email',
            'email' => 'required|email',
        );
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $userdata = $request->only('name', 'last_name', 'email', 'phone_no','status');
            //$userdata = $request->only('name','last_name','phone_no');
            $user = User::whereId($id)->firstOrFail();
            $user->fill($userdata);
            $user->save();

            $profile = Profile::whereUserId($id)->firstOrFail();
            $profile->fill(array("description" => $data["description"], "post_title" => $data["post_title"], "total_experience" => $data["total_experience"]));
            $profile->save();


            return redirect()->route('admin.mentors')->with("message", "User has been successfully updated.");
        }
    }

    public function adminSave(Request $request) {
        $data = $request->all();
        $rules = array(
            'name' => 'required',
            'last_name' => 'required',
            'phone_no' => 'required|min:10',
            'description' => 'required|min:5',
            'email' => 'required|email|unique:users,email'
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $data['password'] = Hash::make($data['password']);
            $data['username'] = "TM" . time() . ItfVideo::randomString(5);
            $tmpdata = $data;
            $validator = Validator::make($data, $rules);
            $userdata = $request->only('name', 'last_name', 'email', 'phone_no', 'user_type', 'username', 'password');
            //$userdata = $request->only('name','last_name','phone_no');
            $user = new User();
            $user->fill($data);
            $user->save();
            $profile = new Profile();

            $profile = Profile::create(array("description" => $data["description"], "post_title" => $data["post_title"], "total_experience" => $data["total_experience"]));
            $profile->user()->associate($user);
            $profile->save();

            //Send mail to user
            Mail::send('emails.register', ['user' => $tmpdata], function ($message) use ($user) {
                $message->to($user["email"], $user["name"])->subject('Welcome to TheMirror');
            });
            return redirect()->route('admin.mentors')->with("message", "User has been successfully updated.");
        }
    }

    public function adminDelete(Request $request, $id) {
        $catinfo = User::where(array("user_type" => "M", "id" => $id))->firstOrFail();

        if (isset($catinfo->id)) {
            $catinfo->delete();
            return redirect()->route("admin.mentors")->with("message", "Mentor has been deleted successfull");
        } else {
            return redirect()->back()->with("errormessage", "Mentor has deletion failed.");
        }
    }

}
