<?php

namespace catalyst\Http\Controllers;

use Illuminate\Http\Request;

use catalyst\Http\Requests;
use catalyst\Http\Controllers\Controller;
use catalyst\User;
use catalyst\Package;
use catalyst\Order;
use catalyst\OrderDetail;
use catalyst\VideoReview;
use catalyst\Payment;
use Response;
use Mail;
use Auth;
class RazorpayController extends Controller
{
    public function payment(Request $request){
        $requestData = $request->all();
        $packages = array();
        $mentors = array();
        foreach($requestData['formInputs'] as $data){
            if($data['name'] == "packages[]"){
                $packages[]= $data['value'];
            }
            if($data['name'] == "mentors[]"){
                $mentors[] = $data['value'];
            }
            // $package[$key] => $value;
        }
        if($requestData['razorpay_payment_id'] == ""){
            return response()->json([
                'status' => 'fail',
                'msg' => 'Payment Failed. Please try again.'
            ]);
        }
        $paymentData = [
            'razor_payment_id'  =>  $requestData['razorpay_payment_id'],
            'video_id'          =>  $requestData['videoId'],
            'user_id'           =>  Auth::user()->id,
            'amount'            =>  $requestData['totalAmount'],
            'status'            =>  'paid',
        ];
        $paymentId = Payment::insertGetId($paymentData);

        // print_r($paymentId); die;
        $videoid = $requestData['videoId'];
        $totalprice="0";
        $totalcharacters="0";

        $packagesinfo = Package::whereIn('id',$packages)->get();
        
        //Add Mentors
        foreach($mentors as $mentor){
            $videoreview=array(
                        "video_id"=>$videoid,
                        "mentor_id"=>$mentor,
                        "user_id"=>Auth::user()->id
                    );
            $videoreviewid =VideoReview::create($videoreview);
        }

        //Packages order information
        foreach($packagesinfo as $pkginfo){
                $totalprice+=$pkginfo->price;
                $totalcharacters+=$pkginfo->total_character;
        }

        $orderinfo=array(
                    'video_id'=>$videoid, 
                    'total_price'=>$totalprice, 
                    'total_characters'=>$totalcharacters,
                    'total_mentor'=>count($mentors)
                );

        $orderid =Order::create($orderinfo);

        //Order Detail

        foreach($packagesinfo as $pkginfo){
            $orderdetailinfo=array(
                    'order_id'=>$orderid->id, 
                    'package_id'=>$pkginfo->id, 
                    'title'=>$pkginfo->title,
                    'price'=>$pkginfo->price,
                    'total_characters'=>$pkginfo->total_character
                );
            $orderdetailid =OrderDetail::create($orderdetailinfo);
        }

        //Send mail to the Mentor
        $mentorsinfo = User::whereIn('id',$mentors)->where("user_type","M")->get();
        $currentuserinfo = User::where("id","=",Auth::user()->id)->first();
        foreach($mentorsinfo as $mentordata){
             Mail::send('emails.mentornotify', ['user' => $mentordata,'sender'=>$currentuserinfo], function ($message) use ($mentordata){
                 $message->to($mentordata->email, $mentordata->name)->subject('The Mirror/Video Review Notification');
             });
        }
        
        return response()->json(['id' => $videoid, 'status' => 'success']);
        // return redirect()->route('video.show', ['id' => $videoid])->with("message","Payment complete successfully and video has been sent for review.");
    }
}
