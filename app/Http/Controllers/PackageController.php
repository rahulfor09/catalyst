<?php

namespace catalyst\Http\Controllers;

use Illuminate\Http\Request;
use catalyst\Http\Requests;
use Validator;

use catalyst\Package;

class PackageController extends Controller
{
    public function __construct(){
       parent::__construct();
    }
    
    /**
     * Display a home of the resource.
     *
     * @return Response
     */

    public function adminIndex()
    {
        $data=array();
        $alldata=Package::orderby("created_at","desc")->paginate(10);
        return view('package.admin.index', compact('alldata'));
    }

    public function adminCreate()
    {
        $data=array();
        return view('package.admin.create', compact('data'));
    }

    public function adminStore(Request $request)
    {
        $data = $request->all();
        $rules = array(
                'title' => 'required',
                'description' => 'required',
                'price' => 'required',
                'total_character' => 'required',
                );
        $validator = Validator::make($data, $rules);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{

            $reqdata = $request->only('title','description','price','total_character');

            $catfind = new Package();
            $catfind->fill($reqdata);
            $catfind->save();
            return redirect()->route('admin.package')->with("message","Package has been successfully save.");
        }

    }
 


    public function adminEdit($id) {

        $data=array();
        $datainfo = Package::where("id","=",$id);
        if($datainfo->exists()){
            $data["package"]=$datainfo->first();
        }
       
        return view('package.admin.edit', compact('data')); 
    }

    public function adminUpdate(Request $request,$id)
    {
        $data = $request->all();
        $rules = array(
                'title' => 'required',
                );
        $validator = Validator::make($data, $rules);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{

            $reqdata = $request->only('title','description','price','total_character');
            $catfind = Package::whereId($id)->firstOrFail();
            $catfind->fill($reqdata);
            $catfind->save();

            return redirect()->route('admin.package')->with("message","Package has been successfully updated.");
        }
    }


    public function adminDelete(Request $request,$id)
    {
        $catinfo = Package::where(array("id"=>$id))->firstOrFail();
        if(isset($catinfo->id)){
            $catinfo->delete();
            return redirect()->route("admin.package")->with("message","Package has been deleted successfull");
        }else{
            return redirect()->back()->with("errormessage","Package has deletion failed.");
        }
    }

}