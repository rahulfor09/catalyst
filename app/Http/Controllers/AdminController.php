<?php


namespace catalyst\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Auth;
use Mail;
use catalyst\Http\Requests;
use Illuminate\Support\Facades\Hash;

use catalyst\User;
use catalyst\Profile;
use catalyst\Video;
use catalyst\VideoReview;
use catalyst\RecentReviewView;

use catalyst\Helpers\ItfVideo;

class AdminController extends Controller
{
	public function __construct(){
		parent::__construct();
		$this->middleware('auth', ['except' => []]);
	}
    

	public function logout() {
		  Auth::logout();
		  return \Redirect::to('/'); 
	}

	public function dashboard() {
		 
		 $data=array();
		 $userid = Auth::user()->id;
		 //$data["latestadded"] = VideoReview::where("mentor_id","=",$userid)->orderby("created_at","desc")->take(5)->get();
		 
		 $data["latestadded"] = VideoReview::select('video_reviewed.*')->leftJoin('recent_review_views', function($joins){

		 		$joins->on('video_reviewed.video_id', '=', 'recent_review_views.video_id')
		 			->on('video_reviewed.mentor_id', '=', 'recent_review_views.user_id');

		  })
                        //->where("video_reviewed.mentor_id","=",$userid)
		 	->where('recent_review_views.user_id','=',null)
		 	->where('recent_review_views.video_id','=',null)
		 	->orderby("created_at","desc")->take(5)->get();

		 //$data["reviewedfile"] = RecentReviewView::where("user_id","=",$userid)->orderby("last_comment","desc")->take(5)->get();
		 $data["reviewedfile"] = RecentReviewView::orderby("last_comment","desc")->take(5)->get();

		 //echo "<pre>"; print_r($data["reviewedfile"]); die;

		 return view('admin.dashboard', compact('data'));
	}
	

	public function profile(Request $request){

		$data=array();
		$id = Auth::user()->id;
		$data["userinfo"] = User::where("id","=",$id)->first();
		return view('admin.profile', compact('data'));
	}

	public function updateProfile(Request $request){

		$data = $request->all();
	    $rules = array(
				'name' => 'required',
				'last_name' => 'required',
				'phone_no' => 'required|min:10'
		     	);
	    $validator = Validator::make($data, $rules);

		if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{

        	$id = Auth::user()->id;
        	$userdata = $request->only('name','last_name','phone_no');
        	$user = User::whereId($id)->firstOrFail();
        	$user->fill($userdata);
        	$user->save();

        	return redirect()->back()->with("message","Profile has been successfully updated.");
        }
	}

}
