<?php

namespace catalyst\Http\Controllers;

use Illuminate\Http\Request;

use catalyst\Http\Requests;
use Validator;
use catalyst\Video;
use catalyst\VideoReview;
use catalyst\RecentReviewView;
use catalyst\User;
use catalyst\Package;
use catalyst\Order;
use catalyst\OrderDetail;
use catalyst\VideoComment;
use catalyst\Category;
use Auth;
use catalyst\Helpers\ItfVideo;
use Response;
use Mail;
use catalyst\Couponuse;
use Carbon\Carbon;
use catalyst\Coupon;
use catalyst\Payment;
class VideoController extends Controller
{
	public function __construct(){
        parent::__construct();
		//$this->middleware('auth', ['except' => ['index','show']]);
		$this->middleware('auth', ['except' => []]);
        //$this->afterFilter("no-cache", ["only"=>["create","show","process","detail"]]);
        //\Artisan::call('cache:clear');
        //\Cache::flush();
	}

    public function index(Request $request) {

        $userid = Auth::user()->id;
    	$videosdatas=Video::select("videos.*")->leftJoin('recent_review_views', 'videos.id', '=', 'recent_review_views.video_id')->where("videos.user_id","=",$userid)->where("recent_review_views.user_id","=",null)->orderby("created_at","desc")->paginate(10);

       //echo "<pre>ddddd";  print_r($videosdatas); die;
		return view('videos.index', compact('videosdatas'));
    }

    public function reviewed(Request $request) {

        $userid = Auth::user()->id;
        $videosdatas=Video::select("videos.*")->join('recent_review_views', 'videos.id', '=', 'recent_review_views.video_id')->where("videos.user_id","=",$userid)->orderby("created_at","desc")->paginate(10);
        
        //echo "<pre>";  print_r($videosdatas); die;

        return view('videos.reviewed', compact('videosdatas'));
    }


    public function review(Request $request) {

        $userid = Auth::user()->id;
        $videosdatas=VideoReview::select('video_reviewed.*')->leftJoin('recent_review_views', function($joins){

                $joins->on('video_reviewed.video_id', '=', 'recent_review_views.video_id')
                    ->on('video_reviewed.mentor_id', '=', 'recent_review_views.user_id');

          })->where("video_reviewed.mentor_id","=",$userid)
            ->where('recent_review_views.user_id','=',null)
            ->where('recent_review_views.video_id','=',null)
            ->orderby("created_at","desc")->paginate(10);

       //echo "<pre>";  print_r($videosdatas); die;
        return view('videos.review', compact('videosdatas'));
    }

    public function reviewedMentor(Request $request) {

        $userid = Auth::user()->id;
        $videosdatas=RecentReviewView::where("user_id","=",$userid)->orderby("last_comment","desc")->paginate(10);
        
        //echo "<pre>";  print_r($videosdatas); die;

        return view('videos.reviewedMentor', compact('videosdatas'));
    }

    public function create(Request $request) {
        //echo phpinfo();die;
    	$data=array();       
		return view('videos.create', compact('data'));
    }


    public function show(Request $request,$id) {
        $data=array();
        $videoinfo = Video::where("id","=",$id)->firstOrFail();

        if(isset($videoinfo->id)){
            //$data["video"]=$videoinfo->first();
            $data["video"]=$videoinfo;
            $data["mentors"]=VideoReview::where("video_id","=",$id)->groupBy('mentor_id')->get();
            if(count($data["mentors"])<=0){
                return redirect()->route('video.process',$id)->with("message","Please select the mentor and plan.");
            }
            $data["video_comment"] = VideoComment::where(array("video_id"=>$id,"status"=>"1"))->orderby("created_at","DESC")->get();
            foreach($data["video_comment"] as &$dd){
                $dd->mentors=$dd->Mentor();
            }
            
        }else{
            

        }
        return view('videos.show', compact('data'));
    }

    public function edit($id,$viewmode='default') {

        $data=array();
        $videoinfo = Video::where("id","=",$id);
        if($videoinfo->exists()){
            $data["video"]=$videoinfo->first();
            if($viewmode=='innerdetail'){
                return ($data["video"]);
            }
        }else{
            
        }
       
        return view('videos.edit', compact('data')); 
    }

    public function process(Request $request,$videoid) {

        $data=array();
        $data["videoid"]=$videoid;
        $data["userInfo"]= Auth::user();
        $videoinfo = Video::where(array("id" =>$videoid,"user_id"=>Auth::user()->id));
        if($videoinfo->exists()){
            $data["video"]=$videoinfo->first();
            $data["mentors"] = User::where(array("user_type"=>"M",'status'=>1))->orderBy('name', 'asc')->get();
            $data["packages"] = Package::orderBy('price', 'asc')->get();

        }else{
            return redirect()->route('user.dashboard')->with("message","You are not authorize for access video");
        }
       
        return view('videos.process', compact('data')); 
    }


    public function processing( Request $request,$videoid){

        $datainfo=array(
                    'packages' => $request->get("packages"),
                    'mentors' => $request->get("mentors"),
                );

        $rules = array(
                    'packages' => 'required|min:1',
                    'mentors' => 'required|min:1',
                );
        $validator = Validator::make($datainfo, $rules);
        
        if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());
        }else{
                
            $data = $request->all();
            if(isset($data['couponcode'])){
                $couponinfo = Coupon::where(array("code"=>$data['couponcode'],'status'=>1))
                ->whereDate('to_date', '>=', Carbon::now())
                ->first();
                if(isset($couponinfo->id))
                {
                    //coupon valide
                    $ipaddressvalue = $_SERVER['SERVER_ADDR'];
                    $dataForCapture = array('coupon_id'=>$couponinfo->id,
                                            'user_id'=>Auth::user()->id,
                                            'video_id'=>$videoid,
                                            'ip_adddress'=>$ipaddressvalue
                                        );
                    Couponuse::create($dataForCapture);
                }
            }
            
            $totalprice="0";
            $totalcharacters="0";

            $packagesinfo = Package::whereIn('id',$data["packages"])->get();
            
            //Add Mentors
            foreach($data["mentors"] as $mentor){
                $videoreview=array(
                            "video_id"=>$videoid,
                            "mentor_id"=>$mentor,
                            "user_id"=>Auth::user()->id
                        );
                $videoreviewid =VideoReview::create($videoreview);
            }

            //Packages order information
            foreach($packagesinfo as $pkginfo){
                    $totalprice+=$pkginfo->price;
                    $totalcharacters+=$pkginfo->total_character;
            }

            $orderinfo=array(
                        'video_id'=>$videoid, 
                        'total_price'=>$totalprice, 
                        'total_characters'=>$totalcharacters,
                        'total_mentor'=>count($data["mentors"])
                    );

            $orderid =Order::create($orderinfo);

            //Order Detail

            foreach($packagesinfo as $pkginfo){
                $orderdetailinfo=array(
                        'order_id'=>$orderid->id, 
                        'package_id'=>$pkginfo->id, 
                        'title'=>$pkginfo->title,
                        'price'=>$pkginfo->price,
                        'total_characters'=>$pkginfo->total_character
                    );
                $orderdetailid =OrderDetail::create($orderdetailinfo);
            }

            //Send mail to the Mentor
            $mentorsinfo = User::whereIn('id',$data["mentors"])->where("user_type","M")->get();
            $currentuserinfo = User::where("id","=",Auth::user()->id)->first();
            //echo "<pre>ddd";print_r($mentorsinfo);die;
            foreach($mentorsinfo as $mentordata){
                Mail::send('emails.mentornotify', ['user' => $mentordata,'sender'=>$currentuserinfo], function ($message) use ($mentordata){
                    $message->to($mentordata->email, $mentordata->name)->subject('The Mirror/Video Review Notification');
                });
            }
            
            return redirect()->route('video.show', ['id' => $videoid])->with("message","Video has been sent for review.");
            //echo "<pre>"; print_r($packagesinfo); print_r($data); print_r($videoreview); die;   
            
        }
    }
    
    //function for export comment
    public function export( Request $request){
        $headers = array(
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=file.csv",
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
        );
        $reviews = array(1);
        //$columns = array('Comment', 'Time', 'Mentor', 'Cateogory');
        $columns = array('Time', 'Cateogory', 'Comment', 'Mentor');
        $data = $request->all(); 
        $allcomments = $this->videoComments($request,$data['video_id'],'','array');
        //echo "<pre>";print_r($allcomments);die;
        $detailofview = $this->edit($data['video_id'],'innerdetail');
        
        $file = fopen('php://output', 'w');
        
        $titleArray = array("Video Title", $detailofview->title);
        fputcsv($file,$titleArray );
        fputcsv($file, array("Description", $detailofview->description));
        fputcsv($file, array("Background", $detailofview->background));
        fputcsv($file, array("Ask question", $detailofview->ask_question));
        fputcsv($file, array("Video Thumb", url('/')."/site/videoimg/".$detailofview->video_image));
        fputcsv($file, array());
        fputcsv($file, $columns);

        foreach($allcomments as $comment) {
            //fputcsv($file, array($comment['comments'],$comment['video_time'],$comment['mentor']->name." ".$comment['mentor']->last_name,$comment['category']->title));
            fputcsv($file, array(number_format(($comment['video_time']/60),2),$comment['category']->title,$comment['comments'],$comment['mentor']->name." ".$comment['mentor']->last_name));
        }
        fclose($file);
        $downloadFileName = str_replace(" ","_",$detailofview->title);
        $downloadFileName.='.csv';
        header('Content-Disposition: attachment; filename="'.$downloadFileName.'";');
        die;
        //die("Done");
        //return Response::stream($file, 200, $headers);
    }


    public function store(Request $request)
    {
    	$postdata = $request->all();
    	$rules = array( 
                    //'video_file_upload' => 'required|max:300000000|mimes:mp4,mkv,flv,mov,mpeg,m4v,3gp',
                    'video_file_upload' => 'required|mimes:mp4,mkv,flv,movie,mpeg,m4v,3gp,qt,mov',
                    'background' => 'required',
                    'description' => 'required',
                    'ask_question' => 'required',
                );

    	$validator = Validator::make($postdata, $rules);
    	if ($validator->fails()) {
    		return response()->json(array("status"=>"failed","message"=>"Video uploading failed.","errors"=>$validator->errors()));
    	}


        try{
        	 $video_file_upload = array_get($postdata,'video_file_upload');
        	 $extension = $video_file_upload->getClientOriginalExtension(); 

             $titlename = preg_replace('/[^A-Za-z0-9]/', ' ',str_replace($extension,'',$video_file_upload->getClientOriginalName()));
             $content_filename = "I".rand(11111, 99999)."TF".time();
             $video_filename = $content_filename . '.' . $extension;
        	 $video_thumbnail = $content_filename . '.png';

             $destinationPath = storage_path() . '/app/public/videos';
        	 $thumbPath = storage_path() . '/app/public/videoimg/'.$video_thumbnail;

        	 $upload_success = $video_file_upload->move($destinationPath, $video_filename);

             ItfVideo::createThumb($destinationPath."/".$video_filename,$thumbPath);
             //ItfVideo::createVideo($destinationPath."/".$video_filename,$destinationPath."/webm/".$content_filename . '.webm');

             //ffmpeg -i input-file.mp4 -c:v libvpx -crf 10 -b:v 1M -c:a libvorbis output-file.webm

             $videodata=array(
                    "video_image"=>$video_thumbnail,
                    "title"=>$titlename,
                    "video_file"=>$video_filename,
                    "background"=>$postdata["background"],
                    "description"=>$postdata["description"],
                    "ask_question"=>$postdata["ask_question"],
                    "user_id"=>Auth::user()->id
                );
             $videoinfo = Video::create($videodata);

        	return response()->json(array("status"=>"success","message"=>"Video has been uploaded","redirect"=>route("video.process",$videoinfo->id)));
        }catch(Exception $e){
            return response()->json(array("status"=>"error","message"=>"Video upload failed","error"=>$e));
        }
    }

    public function destroy(Request $request,$videoid)
    {
        
        $userid = Auth::user()->id;
        $videoinfo = Video::where(array("id"=>$videoid,"user_id"=>$userid))->firstOrFail();

        if(isset($videoinfo->id)){
            $videocomment = VideoComment::where(array("video_id"=>$videoid))->first();
            if(isset($videocomment->id)){
                return redirect()->back()->with("errormessage","Video allready use in other resource.");
            }else{

                VideoReview::where(array("video_id"=>$videoid))->delete();
                @unlink(storage_path() . '/app/public/videos/'.$videoinfo->video_file);
                @unlink(storage_path() . '/app/public/videoimg/'.$videoinfo->video_image);
                $videoinfo->delete();
                return redirect()->back()->with("message","Video has been deleted successfull");
            }
        }    

    }


    public function detail(Request $request,$videoid) {
          
        $data=array();
        $userid = Auth::user()->id;
        $videoreview= VideoReview::where(array("video_id"=>$videoid,"mentor_id"=>$userid));
        $data["videoid"]=$videoid;

        if($videoreview->exists()){
            $data["video"] = Video::where("id","=",$videoid)->first();
            $data["latestadded"] = VideoReview::where("mentor_id","=",$userid)->orderby("created_at","desc")->take(5)->get();

            $data["mentors"]=$videoreview->first();
            $data["categories"]=Category::where("status","1")->orderby("title",'ASC')->lists('title','id');

        }else{
            return redirect()->route('mentor.dashboard')->with("message","You are not authorize for access video");
        }
       
        return view('videos.detail', compact('data'));
    }

    public function comments(Request $request,$videoid) {

        $userid = Auth::user()->id;
        $data["video_comment"] = VideoComment::where(array("video_id"=>$videoid,"user_id"=>$userid))->orderby("created_at","DESC")->get();
        $allcomment = array();
        foreach($data["video_comment"] as $k=>$res)
        {

            $allcomment[$k]=array("id"=>$res->id,"comments"=>$res->comments,"video_time"=>$res->video_time,"total_text"=>$res->total_text,"status"=>$res->status);
            $allcomment[$k]["category"]=$res->Category();
            $allcomment[$k]["mentor"]=$res->Mentor();
        }
        return \Response::json($allcomment);
    }

    public function videoComments(Request $request,$videoid,$mentorid="",$returntype="json") {

        $userid = Auth::user()->id;
        //die($mentorid); user_id
        if(!empty($mentorid))
            $data["video_comment"] = VideoComment::where(array("video_id"=>$videoid,'user_id'=>$mentorid))->orderby("video_time","ASC")->get();
        else
            $data["video_comment"] = VideoComment::where(array("video_id"=>$videoid))->orderby("video_time","ASC")->get();

        $allcomment = array();
        foreach($data["video_comment"] as $k=>$res)
        {
            $allcomment[$k]=array("id"=>$res->id,"comments"=>$res->comments,"video_time"=>$res->video_time,"total_text"=>$res->total_text,"status"=>$res->status);
            $allcomment[$k]["category"]=$res->Category();
            $allcomment[$k]["mentor"]=$res->Mentor();
        }
        if($returntype=='json'){
            return \Response::json($allcomment);
        }
        else{
            return $allcomment;
        }
    }

    public function addComments(Request $request,$videoid) {
        
        $postdata= $request->all();

        $userid = Auth::user()->id;
        $data=array(
                "category_id"=>$postdata["category"],
                "video_id"=>$videoid,
                "user_id"=>$userid,
                "video_time"=>$postdata["currentTimes"],
                "comments"=>$postdata["yourcomment"],
                "total_text"=>strlen($postdata["yourcomment"]),
            );
        $comments=VideoComment::create($data);
        return \Response::json(array("commentid"=>$comments->id));
    }
    public function upudateComments(Request $request) {
        
        $postdata= $request->all();
        //echo "<pre>";print_r($postdata);die;
        $userid = Auth::user()->id;
        $data=array(
                "comments"=>$postdata["savetext"],
            );
        $affectedRows = VideoComment::where(array("id"=>$postdata['currentId']))->update($data);
        return \Response::json(array("commentid"=>true));
    }

    public function deleteComments(Request $request) {
        
        $postdata= $request->all();
        $commentid=isset($postdata["id"])?$postdata["id"]:"0";
        $userid = Auth::user()->id;
        $affectedRows = VideoComment::where(array("user_id"=>$userid,"id"=>$commentid))->delete();
        return \Response::json(array("status"=>$affectedRows));
    }

    public function confirmComments(Request $request,$videoid) {
        $userid = Auth::user()->id;
        $affectedRows = VideoComment::where(array("video_id"=>$videoid,"user_id"=>$userid))->update(array("status"=>"1"));

        if($affectedRows>0){

            $videodata= Video::where("id","=",$videoid)->first();
            $userinfo= $videodata->Owner()->first();
            //print_r($userinfo); die;

            //Send mail to user
                Mail::send('emails.usernotification', ['user' => $userinfo], function ($message) use ($userinfo)
                 {
                    $message->to($userinfo->email, $userinfo->name)->subject('The Mirror/Video Reviewed');
                });
        }

        return \Response::json(array("status"=>$affectedRows));
    }


    //Admin function list

    public function adminIndex(Request $request) {

        $userid = Auth::user()->id;
        $data=array();
        //$videosdatas = Video::orderby("created_at","desc")->paginate(10);
        
            $videosdatas=Video::select(array("videos.*",'users.name','users.last_name','users.company','recent_review_views.last_comment','video_reviewed.created_at as assingdated'))
                ->leftJoin('recent_review_views', 'videos.id', '=', 'recent_review_views.video_id')
                ->leftJoin('video_reviewed', 'videos.id', '=', 'video_reviewed.video_id')
                ->join('users','users.id','=','videos.user_id')
                ->orderby("videos.created_at","desc")->paginate(10);

            $data['couponCode'] = Coupon::select(array('code','coupon_use.created_at','videos.id as videoId'))
                                ->join('coupon_use','coupons.id','=','coupon_use.coupon_id')
                                ->join('videos','coupon_use.video_id','=','videos.id')
                                ->groupby('videos.id')
                                ->get();
								 
		    $data['payment'] = Payment::select(array('status','videos.id as videoId','razor_payment_id','amount','payments.created_at'))
								 ->join('videos','payments.video_id','=','videos.id')
                                 ->get(); 
        return view('videos.admin.index', compact('videosdatas','data'));
    }


    public function adminShow(Request $request,$id) {
          
        $data=array();
        $videoinfo = Video::where("id","=",$id);
        if($videoinfo->exists()){
            $data["video"]=$videoinfo->first();
            $data["mentors"]=VideoReview::where("video_id","=",$id)->get();
            $data["video_comment"] = VideoComment::where(array("video_id"=>$id,"status"=>"1"))->orderby("created_at","DESC")->get();
            
        }else{
            
        }
        return view('videos.admin.show', compact('data'));
    }


    public function adminDelete(Request $request,$videoid)
    {
        $videoinfo = Video::where(array("id"=>$videoid))->firstOrFail();
        if(isset($videoinfo->id))
        {
            VideoComment::where(array("video_id"=>$videoid))->delete();
            VideoReview::where(array("video_id"=>$videoid))->delete();
            unlink(storage_path() . '/app/public/videos/'.$videoinfo->video_file);
            unlink(storage_path() . '/app/public/videoimg/'.$videoinfo->video_image);
            $videoinfo->delete();
            return redirect()->back()->with("message","Video has been deleted successfull");
        }    

    }
    
    public function showarchive(){
        $data=array();
        return view('videos.archive', compact('data'));
    }
    
    public function showarchiverMentor(){
        $data=array();
        return view('videos.archivementor', compact('data'));
    }

	public function myvideos(Request $request){
        $userid = Auth::user()->id;
        $data=array();
        $videosdatas=Video::select(array("videos.*",'users.name','users.last_name','users.company','recent_review_views.last_comment','video_reviewed.created_at as assingdated'))
            ->leftJoin('recent_review_views', 'videos.id', '=', 'recent_review_views.video_id')
            ->leftJoin('video_reviewed', 'videos.id', '=', 'video_reviewed.video_id')
            ->join('users','users.id','=','videos.user_id')
            ->where('videos.user_id','=',$userid)
            ->orderby("videos.created_at","desc")->paginate(10);

        $data['couponCode'] = Coupon::select(array('code','coupon_use.created_at','videos.id as videoId'))
                            ->join('coupon_use','coupons.id','=','coupon_use.coupon_id')
                            ->join('videos','coupon_use.video_id','=','videos.id')
                            ->groupby('videos.id')
                            ->get();
                                
        $data['payment'] = Payment::select(array('status','videos.id as videoId','razor_payment_id','amount','payments.created_at'))
                                ->join('videos','payments.video_id','=','videos.id')
                                ->get(); 
       
        return view('videos.myvideos',compact('videosdatas','data'));
    }
}
