<?php

namespace catalyst\Http\Controllers;

use Illuminate\Http\Request;
use catalyst\Http\Requests;

use catalyst\User;

class HomeController extends Controller
{
    public function __construct(){
       parent::__construct();
    }
    
    /**
     * Display a home of the resource.
     *
     * @return Response
     */

    public function index()
    {
        $data=array();
        $data["ourmentor"]=User::where('user_type', "M")->paginate(15);
        // dd($data);
        return view('homes.index', compact('data'));
    }

}
