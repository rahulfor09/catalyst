<?php

namespace catalyst\Http\Middleware;

use Closure;
use Auth;

class Mentors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->user_type!="M"){
            return redirect()->guest('/');
        }
        return $next($request);
    }
}
