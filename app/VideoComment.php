<?php

namespace catalyst;

use Illuminate\Database\Eloquent\Model;

class VideoComment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'video_id', 'user_id', 'category_id', 'comments', 'video_time','total_text','status',
    ];

    public function Category()
    {
        return $this->hasOne('catalyst\Category','id','category_id')->select(['id','title'])->first();
    }

    public function Mentor()
    {
        return $this->hasOne('catalyst\User','id','user_id')->select(['name','last_name','email','phone_no'])->first();
    }

    public function Video()
    {
        return $this->hasOne('catalyst\Video','id','video_id')->first();
    }
}
