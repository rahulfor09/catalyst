<?php namespace catalyst;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model {

    protected $table = 'payments';
}