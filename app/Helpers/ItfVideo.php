<?php // Code within app\Helpers\ItfVideo.php

namespace catalyst\Helpers;

class ItfVideo
{
    public static function createThumb($videopath="",$thumnailpath="")
    {
        //$FFMPEGSTR = "/usr/local/bin/ffmpeg";
        $FFMPEGSTR = "/usr/bin/ffmpeg";
        $res = exec($FFMPEGSTR.' -i '.$videopath.' -ss 00:00:02 -vframes 1 -filter:v scale="620:-1"  -vcodec png -y '.$thumnailpath.'  2> /dev/null');
        return $res;
    }

    public static function randomString($length=3) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
     }
}