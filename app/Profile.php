<?php namespace catalyst;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model {

    protected $table = 'profiles';

    protected $fillable = [ 'post_title','total_experience','description', 'user_id' ];

    public function user()
    {
        return $this->belongsTo('catalyst\User');
    }

}