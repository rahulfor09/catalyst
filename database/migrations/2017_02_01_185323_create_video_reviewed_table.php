<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoReviewedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_reviewed', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('video_id')->nullable();
            $table->bigInteger('mentor_id')->nullable();
            $table->tinyInteger('status')->default("1");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('video_reviewed');
    }
}
