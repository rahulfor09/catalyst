<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('order_id')->nullable();
            $table->bigInteger('package_id')->nullable();
            $table->string('title')->nullable();
            $table->string('price')->nullable();
            $table->string('total_characters')->nullable();

            $table->tinyInteger('status')->default("1");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders_detail');
    }
}
