<?php $__env->startSection('title', 'Page Not Found'); ?>
<?php $__env->startSection('content'); ?>
<section>
  <div class="container ">
       <div class="text-center marginbottom mt20">
            <div class="title-heading mb40">
                <h3 class="heading-color">Page not found</h3>
                <div class="title-border">
                    <div class="title-box"></div>
                </div>
            </div>
            <p> <img src="<?php echo e(asset('images/404.png')); ?>"></p>
        <h3 class="italic mb10">We're sorry, the page you were looking for isn't found here. The link you followed may either be broken or no longer exists. Please try again, or take a look at our site. </h3>
    </div>
  </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>