<?php $__env->startSection('content'); ?>

<style>
	
	ul.mirror_details li{ color:#606060;}
	
</style>
<table width="100%" border="0" cellpadding="20" cellspacing="0" bgcolor="#f8f8f8" style="color:#181818; font-size:14px; border:1px solid #d3d3d3;" height="auto"> 
<tr>
	<td>
		<!-- <h1 style="color:#034e78">Welcome <?php echo e($user["name"]); ?>,</h1> -->
<!--		<h1 style="color:#034e78">Welcome To The Mirror </h1>-->
		<p style="margin:1em 0;padding:0;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">Hi <?php echo e($user["name"]); ?>,</p>

		<p style="margin:1em 0;padding:0;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">You have successfully registered. Welcome to The  community!</p>

		<p style="margin:1em 0;padding:0;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">Your Catalyst account allows you to:</p>

		<?php if($user['user_type'] == 'M'): ?>
		<ul class="mirror_details">
			<li>You can review and give feedback on the videos uploaded by users.</li>
			<li>Watch video and correct the problems.</li>
			<li>Submit your reviews to users.</li>
		</ul>
		<?php else: ?>
		<ul class="mirror_details">
<!--			<li>You can upload your videos to The Mirror. </li>-->
			<li>Upload your videos to Catalyst. </li>
			<li>Select your coach(s) and get feedback from selected coach(s)</li>
			<li>Watch your feedback with your selected coach(s)</li>
		</ul>
		<?php endif; ?>

		<p></p>

<!--		<a href="<?php echo e(url('login')); ?>" style="word-wrap:break-word;color:#6dc6dd;font-weight:normal;text-decoration:underline" target="_blank">Log in to manage your The Mirror account</a>
		<p></p>-->

<!--		<p style="margin:1em 0;padding:0;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">Any questions? <a href="http://themirror.maynardleighonline.in/faq" style="word-wrap:break-word;color:#6dc6dd;font-weight:normal;text-decoration:underline" target="_blank">Visit our help pages</a> to find the answers. </p>-->

<!--		<p style="margin:1em 0;padding:0;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">Kind Regards,</p>-->

		<p style="margin:1em 0;padding:0;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">Happy Reflecting!</p>
		<p style="margin:1em 0;padding:0;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">Catalyst Team</p>

		<a href="<?php echo e(url('login')); ?>" style=" font-size:17px; line-height:69px; text-decoration:none; padding:15px; color:#fff; background:#00618b;">Sign In To Your Account</a>
	</td>
</tr>
</table>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.mail', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>