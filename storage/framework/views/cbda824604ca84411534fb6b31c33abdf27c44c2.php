<?php $__env->startSection('title', 'information'); ?>
<?php $__env->startPush('scripts-footer'); ?>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('content'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <!-- Begin # DIV Form -->
        <div id="div-forms" >
        <?php //echo "<pre>Herro";print_r($data);die; ?>
            <!-- Begin | Register Form -->
            <?php echo Form::open(['url' => 'clientInfo','method' => 'post', 'name' => 'register-form','id' => 'register-form']); ?>

                <div class="modal-body">
                    <div id="div-register-msg">
                        <div id="icon-register-msg" class="fa fa-chevron-right"></div>
                        <!--<span id="text-register-msg">CLIENT INFORMATION FORM</span>-->
                    </div>

                    <div id="messagedt"></div>
                    <div class="clearfix"></div>
                    
                    <div class="row marginbtn">
                    <div class="col-md-12">
                    <div class="col-md-3 col-sm-3">
                        <div class="row">
<!--                            <label>Account Type : </label>-->
                        </div>
                    </div>
                    </div>
                    </div>
                    <div class="clearfix"></div>
                    <label> Date:</label>	
                    <label id="date"></label><br>
                    <!--<p id="demo"></p>-->
                    <label>Coachee Name:</label><br>
                     <input id="CoacheeName" name="Coachee" class="form-control" type="text" placeholder="Coachee Name" value="<?php echo $data['name'];?>">
                     <input type="hidden" name="coachee_id" value="<?php echo $data['coachee_id']??'';?>">
                     
                    <label>Your manager's Name:</label>
                    <input id="companyName" name="manager_name" class="form-control" type="text" placeholder="manager's name" >
                        
                    <label>Address:</label><br>
                    <textarea name="home_address" rows="5" cols="30" placeholder="Home Address"></textarea>
                    <textarea name="work_address" rows="5" cols="30" placeholder="Work Address" ></textarea>   
                    <br>
                   
                    <label>Email:</label>
                    <fieldset>
                        <label for="Personal">Personal:</label>
                        <input type="email" id="Personal" name="Personal" placeholder="default@gmail.com"value="<?php echo $data['email'];?>"/>&nbsp;

                        <label for="Personal">Work:</label>
                        <input type="email" id="Work" name="work_email" placeholder="default@gmail.com" />
                    </fieldset><br>
                    

                    <label>Contact Preferences*:</label>
                    <select id="Contact" name="contact_preference"> 
                        <option value="Phone">Phone</option>
                        <option value="Email">Email</option></select><br>
                        
                        
                    <label>Company Name*</label>
                    <input id="CompanyName" name="Company-name" class="form-control" type="text" placeholder="Company-name"value="<?php echo $data['company'];?>"/>
 
                    
                    <br>
                    <label>Job Title*:</label>
                    
                    <select id="JobTitle" name="jobTitle">
                        <option value="1">JobTitle1</option>
                        <option value="2">JobTitle2</option>
                        <option value="3">JobTitle3</option>
                        <option value="4">JobTitle4</option>
                    </select>
                    <br>
                    <label>Total Work Experience*:</label>
                    <input id="workexperience" name="work-experience" type="number" placeholder="Work-Experience" > YEAR/S
                    
                    <br><br>
                    
                    <label>Time Spent Present Company*:</label>
                    <input id="Time" name="Time-experience" type="number" placeholder="Time Spent" > YEAR/S <br><br>
                    
                    <label>Date Of birth*:</label>
                    <!--<input id="dob" name="dob" type="calendar" placeholder="DOB" >--> 
                    <input type="date" data-date-inline-picker="true" name="dob">
                    <br><br>
                     <label>Marital Status*:</label>
                      <select id="Maritialstatus" name="maritialstatus">
                        <option value="Single">Single</option>
                        <option value="Married">Married</option>
                        <option value="Separated">Separated</option>
                        <option value="Widow">Widow</option>
                        <option value="Widower">Widower</option>
                      </select><br>
                     <label>Hobbies/Activities/Interests*:</label>
                     <textarea id="w3mission" rows="4" cols="50" name="hobbies"></textarea>

                     
                     <br><br>
                    
                     
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                        <p><font color="red">Notes-The records are non-editable &nbsp &nbsp &nbsp;* Mandatory fields</font></p>
                    
                      <br>
                </div>
                <div class="modal-footer modalfooter-bg">
                </div>
            <?php echo Form::close(); ?>

            <!-- End | Register Form -->
            
        </div>
        <!-- End # DIV Form -->
    </div>
    </div>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<!--<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery("#register-form").submit(function(){
            jQuery('label.error').remove();
            jQuery(".form-control").removeClass("errors");

            jQuery.ajax({
                url:"<?php echo url('register'); ?>",
                method:"POST",
                dataType:"json",
                data:jQuery("#register-form").serialize(),
                success:function(res){
                    console.log(res);
                        
                    if(res.status=="success"){
                        window.location.href="<?php echo url('/'); ?>";
                    }else{
                            
                        jQuery("#messagedt").html('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+res.message+'</div>');
                        jQuery.each(res.errors, function(i, v) {
                            var msg = '<label class="error" for="'+i+'">'+v+'</label>';
                            jQuery('input[name="' + i + '"]').addClass('errors').after(msg);
                        });
                    }
                }
            });
            return false;
        });


    });
        
//function checkInp(){
//var x=document.forms["myForm"]["age"].value;
//if (isNaN(x)) 
//{
//alert("Must input numbers");
//return false;
//}
//}
//        
//    var num = "1234aaaa";
//    var regx = /^[0-9]+$/;
    alert(regx.test(num));

</script>-->

<script>
    var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

</script>
<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>
<script>
n =  new Date();
y = n.getFullYear();
m = n.getMonth() + 1;
d = n.getDate();
document.getElementById("date").innerHTML = m + "/" + d + "/" + y;
</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>