<?php $__env->startSection('title', 'Register'); ?>
<?php $__env->startPush('scripts-footer'); ?>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <!-- Begin # DIV Form -->
        <div id="div-forms" >
        
            <!-- Begin | Register Form -->
            <?php echo Form::open(['url' => 'register','method' => 'post', 'name' => 'register-form','id' => 'register-form']); ?>

                <div class="modal-body">
                    <div id="div-register-msg">
                        <div id="icon-register-msg" class="fa fa-chevron-right"></div>
                        <span id="text-register-msg">Sign Up</span>
                    </div>

                    <div id="messagedt"></div>
                    <div class="clearfix"></div>
                    
                    <div class="row marginbtn">
                    <div class="col-md-12">
                    <div class="col-md-3 col-sm-3">
                        <div class="row">
<!--                            <label>Account Type : </label>-->
                        </div>
                    </div>

                    <div class="col-md-9col-sm-9">
                        <div class="row">

                            <div class="radio radio-primary radio-inline hide">
                                <input id="userradio" value="U" name="accounttype" type="radio" checked="">
                                <label for="userradio"> User </label>
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>
                    <div class="clearfix"></div>
				
                    <label>Coachee Name*</label>
                    <input id="coachee" name="name" class="form-control" type="text" placeholder="Coachee Name" value="<?php echo e(old('name')); ?>">
                    <?php if($errors->has('name')): ?>
                    <div class="error alert alert-danger"><?php echo e($errors->first('name')); ?></div>
                    <?php endif; ?>
                    <label>Coach Sponsor*</label><br>                        
                    <div class="row">
                    <div class="col-md-4">
                    <div class="funkyradio">
                        <div class="funkyradio-default">
                            <input type="radio" name="coach_sponsor" value="SELF" id="radio1"/>
                            <label for="radio1">SELF</label>
                        </div>
                    
                    </div>
                    </div>
                    <div class="col-md-4">
                    <div class="funkyradio">
                        <div class="funkyradio-primary">
                            <input type="radio" name="coach_sponsor" value="HR"id="radio2"  checked/>
                            <label for="radio2">HR</label>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-4">
                    <div class="funkyradio">
                        <div class="funkyradio-success">
                            <input type="radio" name="coach_sponsor" value="OTHERS" id="radio3" />
                            <label for="radio3">OTHERS</label>
                        </div>
                    </div>
                    </div>
                    <?php if($errors->has('coach_sponsor')): ?>
                    <div class="error alert alert-danger"><?php echo e($errors->first('coach_sponsor')); ?></div>
                    <?php endif; ?>

                    </div>
                    <label>Company Name</label>
                    <input id="companyName" name="company" class="form-control" type="text" placeholder="Company Name" value="<?php echo e(old('company')); ?>">
                    <?php if($errors->has('company')): ?>
                    <div class="error alert alert-danger"><?php echo e($errors->first('company')); ?></div>
                    <?php endif; ?>

                    <label>Coach Name*</label>
                    <select class="multiselect-ui form-control" multiple name="coach_name[]">
                    <?php  if(isset($coachList)){
                       
                        foreach($coachList as $coach) {?>
                        <option value="<?php echo e($coach->id); ?>" ><?php echo e($coach->coachee_name); ?> <?php echo e($coach->last_name); ?></option>
                        <?php } 
                            }else{ ?>
                        <option value="">Select Coach</option>
                        <?php }  ?>
                        
                    </select>
                    <label>Phone No*</label>
                    <input id="phoneNo" name="phone_no" class="form-control" type="text" placeholder="Phone Number" maxlength="10" pattern="[6789][0-9]{9}" value="<?php echo e(old('phone_no')); ?>">
                    <?php if($errors->has('phone_no')): ?>
                    <div class="error alert alert-danger"><?php echo e($errors->first('phone_no')); ?></div>
                    <?php endif; ?>


                    <label>Email Address*</label>
                    <input id="emailAddress" name="email" class="form-control" type="text" placeholder="Email Address" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" value="<?php echo e(old('email')); ?>">
                     <?php if($errors->has('email')): ?>
                    <div class="error alert alert-danger"><?php echo e($errors->first('email')); ?></div>
                    <?php endif; ?>

                    
                    <label>Password*</label>
                    <input id="password" name="password" class="form-control" type="password" placeholder="Password" value="<?php echo e(old('password')); ?>">
                    <?php if($errors->has('password')): ?>
                    <div class="error alert alert-danger"><?php echo e($errors->first('password')); ?></div>
                    <?php endif; ?>

                    
                    <label>Confirm Password*</label>
                    <input id="confirmpassword" name="confirm-password" class="form-control" type="password" placeholder="Confirm Password" >
                    <?php if($errors->has('confirm-password')): ?>
                    <div class="error alert alert-danger"><?php echo e($errors->first('confirm-password')); ?></div>
                    <?php endif; ?>

                    <label>* Mandatory fields</label>
                     <div class="mt20">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Sign Up</button>
                        
                    </div>
                </div>
                <div class="modal-footer modalfooter-bg">
                   
                   <div class="text-center">
                        
                        If you are register please Click here&nbsp;<a href="<?php echo url('login'); ?>" data-href="<?php echo url('login'); ?>" id="register_login_btn" class="btn-link semi_bold itfs-pop-up">LOGIN</a>
                   <!--
                        <button id="register_lost_btn" type="button" class="btn btn-link">Lost Password?</button>-->
                    </div>
                </div>
            <?php echo Form::close(); ?>

            <!-- End | Register Form -->
            
        </div>
        <!-- End # DIV Form -->
    </div>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        /*jQuery(document).ready(function(){
            jQuery("#register-form").submit(function(){
                jQuery('label.error').remove();
                jQuery(".form-control").removeClass("errors");

                jQuery.ajax({
                    url:"<?php echo url('register'); ?>",
                    method:"POST",
                    dataType:"json",
                    data:jQuery("#register-form").serialize(),
                    success:function(res){
                        console.log(res);
                        
                        if(res.status=="success"){
                            window.location.href="<?php echo url('/'); ?>";
                        }else{
                            
                            jQuery("#messagedt").html('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+res.message+'</div>');
                            jQuery.each(res.errors, function(i, v) {
                                var msg = '<label class="error" for="'+i+'">'+v+'</label>';
                                jQuery('input[name="' + i + '"]').addClass('errors').after(msg);
                            });
                        }
                    }
                });
                return false;
            });


        });*/
    </script>
</div>
<script>
    var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}
</script>
<style>
    .multiselect {
        width: 200px;
    }

    .selectBox {
        position: relative;
    }

    .selectBox select {
        width: 100%;
        font-weight: bold;
    }

    .overSelect {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
    }

    #checkboxes {
        display: none;
        border: 1px #dadada solid;
    }

    #checkboxes label {
        display: block;
    }

    #checkboxes label:hover {
        background-color: #1e90ff;
    }    


    .funkyradio div {
        clear: both;
        overflow: hidden;
    }

    .funkyradio label {
        width: 100%;
        border-radius: 3px;
        border: 1px solid #D1D3D4;
        font-weight: normal;
    }

    .funkyradio input[type="radio"]:empty,
    .funkyradio input[type="checkbox"]:empty {
        display: none;
    }

    .funkyradio input[type="radio"]:empty ~ label,
    .funkyradio input[type="checkbox"]:empty ~ label {
        position: relative;
        line-height: 2.5em;
        text-indent: 3.25em;
        /*margin-top: 2em;*/
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .funkyradio input[type="radio"]:empty ~ label:before,
    .funkyradio input[type="checkbox"]:empty ~ label:before {
        position: absolute;
        display: block;
        top: 0;
        bottom: 0;
        left: 0;
        content: '';
        width: 2.5em;
        background: #D1D3D4;
        border-radius: 3px 0 0 3px;
    }

    .funkyradio input[type="radio"]:hover:not(:checked) ~ label,
    .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
        color: #888;
    }

    .funkyradio input[type="radio"]:hover:not(:checked) ~ label:before,
    .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
        content: '\2714';
        text-indent: .9em;
        color: #C2C2C2;
    }

    .funkyradio input[type="radio"]:checked ~ label,
    .funkyradio input[type="checkbox"]:checked ~ label {
        color: #777;
    }

    .funkyradio input[type="radio"]:checked ~ label:before,
    .funkyradio input[type="checkbox"]:checked ~ label:before {
        content: '\2714';
        text-indent: .9em;
        color: #333;
        background-color: #ccc;
    }

    .funkyradio input[type="radio"]:focus ~ label:before,
    .funkyradio input[type="checkbox"]:focus ~ label:before {
        box-shadow: 0 0 0 3px #999;
    }

    .funkyradio-default input[type="radio"]:checked ~ label:before,
    .funkyradio-default input[type="checkbox"]:checked ~ label:before {
        color: #333;
        background-color: #ccc;
    }

    .funkyradio-primary input[type="radio"]:checked ~ label:before,
    .funkyradio-primary input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #337ab7;
    }

    .funkyradio-success input[type="radio"]:checked ~ label:before,
    .funkyradio-success input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #5cb85c;
    }

    .funkyradio-danger input[type="radio"]:checked ~ label:before,
    .funkyradio-danger input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #d9534f;
    }

    .funkyradio-warning input[type="radio"]:checked ~ label:before,
    .funkyradio-warning input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #f0ad4e;
    }

    .funkyradio-info input[type="radio"]:checked ~ label:before,
    .funkyradio-info input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #5bc0de;
    }
</style>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>