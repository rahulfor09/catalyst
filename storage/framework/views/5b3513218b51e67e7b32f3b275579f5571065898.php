<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta  name="viewport" content="width=device-width, initial-scale=1">
<title>The Mirror</title>
<style>
.logo img{ max-width:100%;}
</style>
</head>
<body>
<table width="100%" border="0" align="center" cellpadding="20" cellspacing="0" style="max-width:700px; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;color:#fff; font-size:14px;">
  <tr bgcolor="#034e78" height="75px;">
      <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="logo">
                <img src="<?php echo e(asset('images/maillogo.png')); ?>"/>
            </td>
             <td align="right"><a href="mailto:mirror@thepauseplay.com" target="_blank" style="color:#FFF; text-decoration:none;">mirror@thepauseplay.com</a><br> 
                 <a href="tel:+011-41062441/2" style="color:#FFF; text-decoration:none;">011-41062441/2</a>
                 </td>
          </tr>
        </table>
     </td>
  </tr>
 
  <tr bgcolor="#dedede">
    <td>
   		<?php echo $__env->yieldContent('content'); ?>
   	</td>
  </tr>
  <tr bgcolor="#014d6e">
    <td> 
        <p>Copyright ©<?php echo date("Y"); ?> Mirror. All rights reserved. 
  </tr>
</table>
</body>
</html>
