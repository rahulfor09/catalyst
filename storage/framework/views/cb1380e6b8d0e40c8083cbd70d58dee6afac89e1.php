<?php $__env->startSection('title', 'Register'); ?>
<?php $__env->startPush('scripts-footer'); ?>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <!-- Begin # DIV Form -->
        <div id="div-forms" >
        
            <!-- Begin # Login Form -->
            <?php echo Form::open(['url' => 'login','method' => 'post', 'name' => 'login-form','id' => 'login-form']); ?>

            
                
                <div class="modal-body">
                    <div id="div-login-msg">
                        <div id="icon-login-msg" class="fa fa-chevron-right"></div>
                        <span id="text-login-msgs">Type your User Id and password.</span>
                        <div class="clearfix"></div>
                        
                    </div>
                    <div id="messagedt">
                        
                    <?php if($errors->has('approve')): ?>
                    <div class="error alert alert-danger"><?php echo e($errors->first('approve')); ?></div>
                    <?php endif; ?>



                    </div>
                    <div class="clearfix"></div>

                    <label>User Id</label>
                    <input id="login_username" name="email" class="form-control" type="text" placeholder="info@example.com" value="<?php echo e(old('email')); ?>">
                    <?php if($errors->has('email')): ?>
                    <div class="error alert alert-danger"><?php echo e($errors->first('email')); ?></div>
                    <?php endif; ?>

                    
                    <label>Password</label>
                    <input id="login_password" name="password" class="form-control" type="password" placeholder="Password" >
                     <?php if($errors->has('password')): ?>
                    <div class="error alert alert-danger"><?php echo e($errors->first('password')); ?></div>
                    <?php endif; ?>
                    <div class="checkbox">
                        <label>
                            <input class="normarcheckbox" type="checkbox" name="remember" id="btn_remembers" > Remember me
                        </label>
                         <a href="<?php echo e(url('/forgotpassword')); ?>">Forgot Your Password?</a>
                    </div> 
                       
                    <div class="mt20">
                        <?php echo e(csrf_field()); ?>

                        <button type="submit" name="login" class="btn btn-primary btn-lg btn-block">Login</button>
                    </div>
                </div>
                 
                <div class="modal-footer modalfooter-bg">
                  
                    <div class="text-center">
                        If you are not register please Click here&nbsp;<a href="<?php echo url('register'); ?>" data-href="<?php echo url('register'); ?>" id="login_register_btn" class="btn-link semi_bold itfs-pop-up">Sign Up</a>
                    </div>
                </div>
           <?php echo Form::close(); ?>

            <!-- End # Login Form -->
            
        </div>
        <!-- End # DIV Form -->
    </div>
    <script type="text/javascript">
    /*    jQuery(document).ready(function(){

            jQuery("#login-form").submit(function(){
                
                jQuery.ajax({
                    url:"<?php echo url('login'); ?>",
                    method:"POST",
                    dataType:"json",
                    data:jQuery("#login-form").serialize(),
                    success:function(res){
                        //console.log(res);
                        if(res.status=="success"){
                            window.location.href=res.redirect;
                        }else{
                            jQuery("#messagedt").html('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+res.message+'</div>');
                        }
                    }
                });
                return false;
            });


        });*/
    </script>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>