@extends('layouts.admin')
@section('title', 'Coupon')
@section('content')
<div class="row">

<div class="borderwhite">
                    <div class="bordergrey">
                        <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>All Coupons
                        <div class="pull-right"><a href="{{route('admin.coupon.create')}}" class="btn btn-primary">Add Coupon</a></div>
                        <div class="border-lightgrey mt5"></div>    
                        </div>

                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Code</th>
                                    <th>#Use</th>
                                    <th>Valid From</th>
                                    <th>Valid To</th>
                                    <th class="actionblock">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php //echo "<pre>qqq";print_r($couponsdatas);die; ?>
                                @foreach ($couponsdatas as $couponsdata)  
                                  <tr>
                                    <td>
                                       <?php if(!empty($couponsdata->coupon_image)){ ?>
                                            <img src="{{asset('site/couponimg/'.$couponsdata->coupon_image) }}" width="50" height="50"  class="img-circle" />
                                        <?php } ?>
                                    </td>
                                    <td>{{$couponsdata->code}}</td>
                                    <td>{{$couponsdata->no_of_use}}</td>
                                    <td>{{date('d M Y', strtotime($couponsdata->from_date))}}</td>
                                    <td>{{date('d M Y', strtotime($couponsdata->to_date))}}</td>
                                    <td>
                                        <a href="{{ route('admin.coupon.show',$couponsdata->id) }}" class="mb10 btn btn-primary"> <i class="fa fa-edit">&nbsp;</i></a>
                                                    {{ Form::open(['method' => 'DELETE','route' => ['admin.coupon.delete', $couponsdata->id],'class'=>'form-btns itfdeletefrm']) }}
                                                        {{ Form::hidden('id', $couponsdata->id) }}
                                                        {!! Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger','type'=>'submit']) !!}
                                                    {{ Form::close() }}
                                    </td>
                                  </tr>
                                   @endforeach 
                                </tbody>
                              </table>
                              <div class="clearfix"></div>

                        </div>
                        
                        <div class="mb10">
                            <nav aria-label="...">
                                <?php echo $couponsdatas->render(); ?>
                            </nav>
                         </div>
                         <div class="clearfix"></div>
                    </div>
                </div>
   
</div>

@stop