@extends('layouts.admin')
@section('title', 'Coupon')
@section('content')
<div class="borderwhite">
    <div class="bordergrey">
        <div class="col-md-12">
            <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>Edit Coupon
                <div class="border-lightgrey mt5"></div>    
            </div>

                <div class="col-md-12">
                   
                    <div class="col-md-12"> 
                           {!! Form::open(['route' => ['admin.coupon.update',$data["coupon"]->id],'method' => 'post', 'name' => 'coupon-form','id' => 'coupon-form','class'=>'form-horizontal']) !!}
                            
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>Code</label>
                                <input type="text" id="code" name="code" class="form-control"  placeholder="Code" value="{{ (old('code'))? old('code') : $data['coupon']->code}}"/>
                                @if ($errors->has('code'))<span class="itferror">{{ $errors->first('code') }}</span>@endif
                            </div>
                            <div class="col-md-6">
                                <label>No of use</label>
                                <input type="text" id="no_of_use" name="no_of_use" class="form-control"  placeholder="No of use" value="{{ (old('no_of_use'))? old('no_of_use') : $data['coupon']->no_of_use}}"/>
                                @if ($errors->has('no_of_use'))<span class="itferror">{{ $errors->first('no_of_use') }}</span>@endif
                            </div>
                            
                        </div>
                           
                        <div class="form-group">
                            
                            <div class="col-md-12">
                                <label>Description</label>
                                <input type="text" id="description" name="description" class="form-control"  placeholder="Description" value="{{ (old('description'))? old('description') : $data['coupon']->description}}"/>
                                @if ($errors->has('description'))<span class="itferror">{{ $errors->first('description') }}</span>@endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>From date</label>
                                <input autocomplete="off" type="text" id="from" name="from_date" class="form-control"  placeholder="From date" value="{{ (old('from_date'))? old('from_date') : $data['coupon']->from_date}}"/>
                                @if ($errors->has('from_date'))<span class="itferror">{{ $errors->first('from_date') }}</span>@endif
                            </div>
                            <div class="col-md-6">
                                <label>To date</label>
                                <input autocomplete="off" type="text" id="to" name="to_date" class="form-control"  placeholder="To date" value="{{ (old('to_date'))? old('to_date') : $data['coupon']->to_date}}"/>
                                @if ($errors->has('to_date'))<span class="itferror">{{ $errors->first('to_date') }}</span>@endif
                            </div>
                        </div>
                       
                        <div class="clearfix "></div>
                        <div class="form-group">
                            <div class="col-md-6 col-xs-6">
                                <input type="submit" class="btn btn-primary" value="Update" name="uploadbtn" id="uploadbtn">
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="clearfix"></div>
            </div>
        </div> 
        <div class="clearfix"></div>
     </div>
</div> 
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(function(){
        $("#to").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#from").datepicker({ dateFormat: 'yy-mm-dd' }).bind("change",function(){
            var minValue = $(this).val();
            minValue = $.datepicker.parseDate("yy-mm-dd", minValue);
            minValue.setDate(minValue.getDate()+1);
            $("#to").datepicker( "option", "minDate", minValue );
        })
    }); 
</script>
  
@stop
