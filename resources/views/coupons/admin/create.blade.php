@extends('layouts.admin')
@section('title', 'Coupon')
@section('content')
<div class="borderwhite">
    <div class="bordergrey">
        <div class="col-md-12">
            <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>Add Coupon
                <div class="border-lightgrey mt5"></div>    
            </div>

                <div class="col-md-12">
                   
                    <div class="col-md-12"> 
                        {!! Form::open(['route' => 'admin.coupon.store','method' => 'post', 'name' => 'coupon-form','id' => 'coupon-form','class'=>'form-horizontal']) !!}
                            
                        <div class="form-group">
                            <div class="col-md-12">
                            <label>Code</label>
                            <input type="text" id="title" name="code" class="form-control"  placeholder="Code" value="{{old('title')}}" />
                            @if ($errors->has('title'))<span class="itferror">{{ $errors->first('code') }}</span>@endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                            <label>Description</label>
                            <textarea id="description" name="description" class="form-control" placeholder="Description" rows="3" >{{old('description')}}</textarea>
                            @if ($errors->has('description'))<span class="itferror">{{ $errors->first('description') }}</span>@endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12">
                            <label>No. of use</label>
                            <input type="text" id="no_of_use" name="no_of_use" class="form-control"  placeholder="No of use" value="{{old('no_of_use')}}" />
                            @if ($errors->has('no_of_use'))<span class="itferror">{{ $errors->first('no_of_use') }}</span>@endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                            <label>From date</label>
                            <input autocomplete="off" type="text" id="from" name="from_date" class="form-control"  placeholder="From date" value="{{old('from_date')}}" />
                            @if ($errors->has('from_date'))<span class="itferror">{{ $errors->first('from_date') }}</span>@endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                            <label>To date</label>
                            <input autocomplete="off" type="text" id="to" name="to_date" class="form-control"  placeholder="To date" value="{{old('to_date')}}" />
                            @if ($errors->has('to_date'))<span class="itferror">{{ $errors->first('to_date') }}</span>@endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                            <label>Status</label>
                            <input type="radio" id="status" name="status" class=""  placeholder="To date" value="{{old('status')}}" value="1" checked> Active
                            <input type="radio" id="status" name="status" class=""  placeholder="To date" value="{{old('status')}}" value="0" > Inactive
                            @if ($errors->has('status'))<span class="itferror">{{ $errors->first('status') }}</span>@endif
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-6 col-xs-6">
                                <input type="submit" class="btn btn-primary" value="Save" name="uploadbtn" id="uploadbtn">
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="clearfix"></div>
            </div>
        </div> 
        <div class="clearfix"></div>
     </div>
</div> 
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
 $(function(){
        $("#to").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#from").datepicker({ dateFormat: 'yy-mm-dd' }).bind("change",function(){
            var minValue = $(this).val();
            minValue = $.datepicker.parseDate("yy-mm-dd", minValue);
            minValue.setDate(minValue.getDate()+1);
            $("#to").datepicker( "option", "minDate", minValue );
        })
    }); 
  </script>
@stop
