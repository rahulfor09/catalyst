@extends('layouts.mail')
@section('content')
<table width="100%" border="0" cellpadding="20" cellspacing="0" bgcolor="#f8f8f8" style="color:#181818; font-size:14px; border:1px solid #d3d3d3;" height="auto"> 
<tr>
	<td>
		<h1 style="color:#034e78">Hi {{$user["name"]}},</h1>
		<p>Your video has been reviewed.</p>
		<p>Please login and view.</p>
		<a href="{{url('login')}}" style=" font-size:17px; line-height:69px; text-decoration:none; padding:15px; color:#fff; background:#00618b;">Sign In To Your Account</a>
	</td>
</tr>
</table>
@stop
