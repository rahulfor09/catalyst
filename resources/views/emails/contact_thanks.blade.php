@extends('layouts.mail')
@section('content')

<style>
	
	ul.mirror_details li{ color:#606060;}
	
</style>
<table width="100%" border="0" cellpadding="20" cellspacing="0" bgcolor="#f8f8f8" style="color:#181818; font-size:14px; border:1px solid #d3d3d3;" height="auto"> 
<tr>
	<td>		
		<h1 style="color:#034e78">Welcome To The Mirror </h1>
		<p style="margin:1em 0;padding:0;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">Hi {{$user["first_name"]}},</p>
		<p style="margin:1em 0;padding:0;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">Thanks for contacting us, Our administrator will contact you back soon. Thank you for your patience.</p>		
		
	</td>
</tr>
</table>
@stop
