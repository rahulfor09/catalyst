@extends('layouts.admin')
@section('title', 'Category')
@section('content')
<div class="borderwhite">
    <div class="bordergrey">
        <div class="col-md-12">
            <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>Add  Category
                <div class="border-lightgrey mt5"></div>    
            </div>

                <div class="col-md-12">
                   
                    <div class="col-md-12"> 
                        {!! Form::open(['route' => 'admin.category.store','method' => 'post', 'name' => 'video-form','id' => 'video-form','class'=>'form-horizontal']) !!}
                            
                        <div class="form-group">
                            <div class="col-md-12">
                            <label>Title</label>
                            <input type="text" id="title" name="title" class="form-control"  placeholder="Title" value="{{old('last_name')}}" />
                            @if ($errors->has('title'))<span class="itferror">{{ $errors->first('title') }}</span>@endif
                            </div>
                        </div>
                       
                        
                        <div class="form-group">
                            <div class="col-md-6 col-xs-6">
                                <input type="submit" class="btn btn-primary" value="Save" name="uploadbtn" id="uploadbtn">
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="clearfix"></div>
            </div>
        </div> 
        <div class="clearfix"></div>
     </div>
</div> 
@stop