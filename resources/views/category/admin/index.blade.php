@extends('layouts.admin')
@section('title', 'Category')
@section('content')
<div class="row">

<div class="borderwhite">
                    <div class="bordergrey">
                        <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>All Category
                        <div class="pull-right"><a href="{{route('admin.category.create')}}" class="btn btn-primary">Add Category</a></div>
                        <div class="border-lightgrey mt5"></div>    
                        </div>

                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th>Name</th>
                                    <th>Created</th>
                                    <th class="actionblock">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach ($alldata as $info)  
                                  <tr>
                                    <td>{{$info->title}}</td>
                                    <td>{{$info->created_at->format('d M Y')}}</td>
                                    <td>
                                        <a href="{{ route('admin.category.edit',$info->id) }}" class="mb10 btn btn-primary"> <i class="fa fa-edit">&nbsp;</i></a>
                                                    {{ Form::open(['method' => 'DELETE','route' => ['admin.category.delete', $info->id],'class'=>'form-btns itfdeletefrm']) }}
                                                        {{ Form::hidden('id', $info->id) }}
                                                        {!! Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger','type'=>'submit']) !!}
                                                    {{ Form::close() }}
                                    </td>
                                  </tr>
                                   @endforeach 
                                </tbody>
                              </table>
                              <div class="clearfix"></div>

                        </div>
                        
                        <div class="mb10">
                            <nav aria-label="...">
                                <?php echo $alldata->render(); ?>
                            </nav>
                         </div>
                         <div class="clearfix"></div>
                    </div>
                </div>
   
</div>

@stop