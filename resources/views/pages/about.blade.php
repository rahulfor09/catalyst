@extends('layouts.default')
@section('title', 'About Us')

@push('scripts-footer')
<script>
    jQuery(document).ready(function($){
		jQuery('.multi-item-carousel').carousel({ interval: false });
		jQuery('.multi-item-carousel .item').each(function(){
		  var next = jQuery(this).next();
		  if (!next.length) { next = jQuery(this).siblings(':first'); }
		  next.children(':first-child').clone().appendTo(jQuery(this));
		  
		  if (next.next().length>0) {
			next.next().children(':first-child').clone().appendTo(jQuery(this));
		  } else {
			jQuery(this).siblings(':first').children(':first-child').clone().appendTo(jQuery(this));
		  }
		});
	});
</script>
@endpush

@section('content')
<section>
    <div class="inner-banner">
    	<div class="left-inner"></div>
        <div class="right-bg right-inner">
        	<img src="images/profile-bg.jpg"/>
        
        </div>
        <div class="inner-banner-content">
        	<div class="col-md-5">
            	
                <div class=" mt40 res-text-center480 res-width480">
               <h1>About Us</h1>
               <div class="borderwhite-b10 mb10"></div>
                <p><strong>"IT'S IN OUR BONES".</strong></p>

<p>Maynard Leigh delivers outstanding behavioural change programmes in the areas of Communication, Leadership and Teams. Since 1989 we've been pioneering the use of inspiring ideas from theatre, blended with psychological insight and frameworks for organisational change, to create highly intensive development in companies and in people.

To know more about us, please visit our website – <i class="fa fa-globe">&nbsp;</i><a target="_blank" href="http://www.maynardleigh.com" class="text_wht">www.maynardleigh.com</a>  </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
  <div class="container ">
	   <div class="text-center marginbottom margintop from-bottom">
    		<div class="title-heading mb40">
                <h3 class="heading-color">The Mirror</h3>
                <h4 class="heading-color">Reflecting your potential</h4>
                <div class="title-border">
                    <div class="title-box"></div>
                </div>
            </div>
    	<p class="small-text italic mb10">"MaynardLeigh introduces a cloud based feedback software that supports learners in creating powerful presentations.</p>

<p class="small-text italic mb10">Mirror adds immense value by the simple yet powerful means of making space for feedback. In order to receive feedback, participants can now upload videos of presentations they have made.</p>

<p class="small-text italic mb10">They can also choose consultants from MaynardLeigh from whom they would like to receive feedback.</p>

<p class="small-text italic mb10">This tool is a medium through which observations can be shared in an effective and efficient manner.</p>

<p class="small-text italic mb10">Mirror allows for:</p>

<p class="small-text italic mb10">- Recording and sharing moment-to-moment feedback</p>

<p class="small-text italic mb10">- A play-pause facility which makes it easier to comprehend feedback</p>

<p class="small-text italic mb10">- Storing analysis for future reference  "</p>

       
    </div>
  </div>
</section>

<!-- <section class="blue-bgIm">
  <div class="container ">
  	<div class="mt90 mb90 from-bottom">
    		<div class="title-heading mb30 from-bottom">
                <h3 class="white-text">Living Our Values</h3>
                <div class="title-border2">
                    <div class="title-box"></div>
                </div>
            </div>
            <p class=" text-center white-text">Our Purpose is to Unlock People’s Potential and Inspire Greater Impact.
As a company, we also have a higher purpose than just to make a profit or pay the rent! Maynard Leigh exists to promote meaning, humanity and vitality in the workplace.
Our five core values have underpinned our ethos and culture.
</p>
    		
            <div class="clearfix"></div>
    </div>
  </div>
</section> -->

<?php /* ?>
<section>
  <div class="container ">
	 <div class="mt80 mb80 from-bottom">
    		<div class="title-heading  mb30">
                <h3 class="heading-color ">Our Client</h3>
                <div class="title-border">
                    <div class="title-box"></div>
                </div>
            </div>
          
     			<div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="carousel slide multi-item-carousel" id="theCarousel">
                        <div class="carousel-inner">

                        @foreach ($data["ourmentor"] as $kk=>$mprofile) 
                          <div class="item {{($kk==0)?'active':''}}">
                              <div class="col-xs-4">
                                <a href="{!! route('profiledetail',$mprofile->username) !!}">
                                  <?php if(!empty($mprofile->user_photo)){ ?>
                                        <img height="300" src="{{asset('siteimage/users/'.$mprofile->user_photo)}}" class="img-responsive imgclient"/>
                                    <?php }else{ ?>
                                        <img height="300" src="{{asset('images/profile.png')}}" class="img-responsive imgclient"/>
                                    <?php } ?>
                                </a>
                              </div>
                          </div>
                        @endforeach 
                          <!--  Example item end -->
                        </div>
                        <a class="left carousel-control" href="#theCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                        <a class="right carousel-control" href="#theCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                      </div>
                    </div>
                  </div>
    		</div>           
    	</div>
    </div>
</section>
<?php */ ?>
@stop