@extends('layouts.default')
@section('title', 'Contact Us')

@push('scripts-footer')
<script>
    jQuery(document).ready(function($){
jQuery("#frmcontact").submit(function(){
                jQuery.ajax({
                    url:"{{url('contactus')}}",
                    method:"POST",
                    dataType:"json",
                    data:jQuery("#frmcontact").serialize(),
                    success:function(res){
                        console.log(res);
                        $('label.error').remove();
                        $(".form-control").removeClass("errors");
                        if(res.status=="success"){
                            jQuery("#contactmsg").html(res.message);
                            jQuery("#contactmsg").removeClass("hide");
                            jQuery("#contactmsg").addClass("alert alert-success");
                            jQuery("#frmcontact")[0].reset();

                        }else{
                            $.each(res.errors, function(i, v) {
                                var msg = '<label class="error" for="'+i+'">'+v+'</label>';
                                $('input[name="' + i + '"], select[name="' + i + '"], textarea[name="' + i + '"]').addClass('errors').after(msg);
                            });
                        }
                    }
                });
                return false;
            });
 		});
</script>
@endpush

@section('content')  
<section>
	<div class="inner-banner">
		<div class="left-inner"></div>
		<div class="right-bg right-inner">
			<img src="images/bg_contacts.jpg"/>

		</div>
		<div class="inner-banner-content">
			<div class="col-md-5">

				<div class=" mt40 res-text-center480 res-width480">
					<h1>Contact Us</h1>
					<div class="borderwhite-b10 mb10"></div>
					<h4 class="mt30">India Office Details</h4>
					<p>C-35 Hauz Khas, New Delhi -110048.</p>
					<p><i class="fa fa-phone">&nbsp;</i><a href="tel:+011-41062441/2" class="text_wht">011-41062441/2</a></p> 
					<p><i class="fa fa-envelope">&nbsp;</i><a href="mailto:themirror@maynardleigh.in" class="text_wht">themirror@maynardleigh.in</a></p>
                    <p><i class="fa fa-globe">&nbsp;</i><a target="_blank" href="http://www.maynardleigh.com" class="text_wht">www.maynardleigh.com</a></p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="margintop marginbottom">
	<div class="container from-bottom">
		<div class="title-heading mb30">
			<h3 class="heading-color">Enquiry</h3>
			<div class="title-border">
				<div class="title-box"></div>
			</div>
		</div>
		<!-- <p class="small-text italic text-center">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p> -->
		<p class="small-text italic text-center hide" id="contactmsg"></p>
		<div class="width600 ">
			<form class="form_layout1  mt30 " name="frmcontact" id="frmcontact" method="post" accept-charset="UTF-8">
                    <div class="col-md-6">
                        <div class="form-group">
                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <input type="text" class="form-control" id="phone_no" name="phone_no" placeholder="Phone No">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <input type="text" class="form-control" id="contact_email" name="contact_email" placeholder="Email Address">
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                          <textarea class="form-control" rows="5" id="comment" name="comment" placeholder="Message"></textarea>
                        </div>
                    </div>
                     <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary btn-lg ">Send Message</button>
                    </div>
                </form>
		</div>
		<div class="clearfix"></div>
	</div>
</section>

<!-- <section class="blue-bgIm">
	<div class="container ">
		<div class="mt90 mb90 from-bottom">
			<div class="title-heading mt30 mb30 from-bottom">
				<h3 class="white-text">Our  Vission</h3>
				<div class="title-border2">
					<div class="title-box"></div>
				</div>
			</div>
			<p class=" text-center white-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>

			<div class="clearfix"></div>
		</div>
	</div>
</section> -->
@stop