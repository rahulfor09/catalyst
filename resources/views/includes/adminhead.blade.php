<meta charset="utf-8">
<title>@yield('title','Catalyst')</title>
<meta name="description" content="Catalyst">
<meta name="author" content="Catalyst for data">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,300,400,500,600,700&display=swap" rel="stylesheet"> 
<!--<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>-->

<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{URL::asset('css/bootstrap-theme.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/vendors/owl.carousel/owl.carousel.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/vendors/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/vendors/revolution/css/settings.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/vendors/revolution/css/layers.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/vendors/revolution/css/navigation.css')}}" rel="stylesheet">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/theme.css') }}" rel="stylesheet">

@stack('scripts-headers') 

