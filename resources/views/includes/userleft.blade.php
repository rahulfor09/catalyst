@push('scripts-footer')
<script>
    jQuery(document).ready(function(){
       jQuery("#profile_file_upload").on("change",function(){
            jQuery("#profile_img_form").ajaxSubmit({ 
                    target:   "#itftargetLayer", 
                    dataType: "json",
                    beforeSend:function(){ 
                        jQuery("#itf_progress_bar_block").removeClass("hide");
                        jQuery("#itf_progress_bar").html("0%"); 
                    },
                    beforeSubmit: function() {},
                    uploadProgress: function (event, position, total, percentComplete){                             
                        jQuery("#itf_progress_bar").css({width:percentComplete+'%'}).html(percentComplete+"%");
                    },
                    success:function (resdt){
                        jQuery("#itf_progress_bar_block").addClass("hide");    
                        if(resdt.status=="success"){
                          jQuery(".pimgchange>img").attr("src","{{asset('siteimage/users')}}/"+resdt.imagename+"?t="+new Date());
                        }
                    },
                    resetForm: false 
                });
                return false;
            });
        });
</script>
@endpush
<div class="borderwhite">
    <div class="bordergrey">
        <div class="nav-side-menu">
            <div class="text-center">
                <div class="circle130 pimgchange">
                    <?php if(!empty(Auth::user()->user_photo)): ?>
                        <img src="{{asset('siteimage/users/'.Auth::user()->user_photo)}}" />
                    <?php else: ?>
                        <img src="{{asset('images/profile.png')}}"/>
                    <?php endif; ?>
                    <span class="profileimgchnage">
                    {!! Form::open(['route' => array('profilephoto'),'method' => 'post', 'name' => 'profile_img_form','id' => 'profile_img_form']) !!}<input name="profile_file_upload" id="profile_file_upload" class="file" type="file">{!! Form::close() !!}
                    <i class="fa fa-camera-retro fa-lg"></i></span>
                </div>
               	<div class="mt10 mb20"><?php echo Auth::user()->name; ?></div>
           </div>
            <i class="fa fa-bars  toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>                          
            <div class="menu-list">
                <ul id="menu-content" class="menu-content collapse out">
                    <li><a href="{!! url('dashboard') !!}">Dashboard</a></li>
                    <!-- <li ><a href="{!! route('video.create') !!}">Upload Video</a></li> -->
<!--                     <li><a href="{{ route('video.index') }}">Uploaded Videos</a></li>                         
                     <li><a href="{{ route('video.reviewed') }}">Reflection Videos</a></li>-->
                     <!-- <li><a href="{!! route('video.archieve') !!}" title="Archived Videos">Archived Videos </a></li> -->
                     <li><a href="{!! url('logout') !!}">Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>	