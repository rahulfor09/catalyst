<!--Header-->
<header class="top-bar row">
        <div class="container">
                <div class="welcome-message">Welcome to Catalyst</div>
                <ul class="nav top-nav">
                        <li class="email"><a href="mailto:support@thepauseplay.com"><i class="fa fa-envelope-o"></i>support@thepauseplay.com</a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                        <li class="tel"><a href="#"><i class="fa fa-phone"></i>+91 8093203113</a></li>
                </ul>
        </div>
</header>

<!--Navigation-->
  	<nav class="navbar navbar-default navbar-static-top">
  		<div class="container">
  			<div class="navbar-header">
  				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_nav" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
			 	</button>
			 	<a href="{!! url('/') !!}" class="navbar-brand"><img src="{{ asset('images/logo.png') }}"/></a>
  			</div>
			<div class="collapse navbar-collapse" id="main_nav">
				<a href="{!! url('/register') !!}" class="btn btn-outline blue pull-right hidden-xs hidden-sm">free join</a>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown active">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
						<ul class="dropdown-menu">
							<li><a href="index.html">Home</a></li>
							<li><a href="index2.html">Home 2</a></li>
							<li><a href="index3.html">Home 3</a></li>
						</ul>
					</li>
					<li><a href="about.html">About</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services</a>
						<ul class="dropdown-menu">
							<li><a href="service.html">services</a></li>
							<li><a href="single-service.html">Details 1</a></li>
							<li><a href="single-service2.html">Details 2</a></li>
							<li><a href="single-service3.html">Details 3</a></li>
							<li><a href="single-service4.html">Details 4</a></li>
							<li><a href="single-service5.html">Details 5</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cases</a>
						<ul class="dropdown-menu">
							<li><a href="case-study.html">Case Studies</a></li>
							<li><a href="single-case.html">Details</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages</a>
						<ul class="dropdown-menu">
							<li><a href="blog.html">Blog</a></li>
							<li><a href="portfolio.html">Portofolio</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Shop</a>
						<ul class="dropdown-menu">
							<li><a href="shop.html">Products</a></li>
							<li><a href="single-product.html">Details</a></li>
							<li><a href="checkout.html">Checkout</a></li>
							<li><a href="cart.html">cart</a></li>
						</ul>
					</li>
					<li><a href="contact.html">contact</a></li>
				</ul>
			</div>
  		</div>
  	</nav>


<?php /*
<a id="toggle" href="#">
	<i class="fa fa-bars"></i>
</a>
<main id="content">
	<div id="overlay"></div>
	<header>
		<div class="header">
			<div class="container from-top">
				<div class="">
					<div class="col-md-3 col-sm-3 col-xs-12">
						<div class="logo">
							<div class="logo-inner">
								<a href="{!! url('/') !!}">
									<img src="{{ asset('images/logo.png') }}"/>
								</a>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-md-9 col-sm-9 col-xs-12>">

						<?php if(Auth::check()): ?>
							<div class="avtar menu">
								<ul>
									<li>
										<div class="dropdown">
											<a id="dLabel" href="#itf" class="semi_bold" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
											<?php echo Auth::user()->name; ?>
												<span class="caret"></span>
												<?php if(!empty(Auth::user()->user_photo)){ ?>
													<img src="{{asset('siteimage/users/'.Auth::user()->user_photo)}}" class="circle50 hidden-xs"/>
												<?php }else{ ?>
													<img src="{{asset('images/profile.png')}}" class="circle50 hidden-xs"/>
												<?php } ?>
											</a>

											<ul class="dropdown-menu" aria-labelledby="dLabel">
												
												<?php if(Auth::user()->user_type=="M"): ?>
													<li><a href="{!! route('mentor.dashboard') !!}" >Dashboard</a></li>
													<li><a href="{!! route('mentor.profile') !!}" >Edit profile</a></li>
												<?php elseif(Auth::user()->user_type=="A"): ?>
													<li><a href="{!! route('admin.dashboard') !!}" >Dashboard</a></li>
													<li><a href="{!! route('admin.profile') !!}" >Edit profile</a></li>
												<?php else: ?>
													<li><a href="{!! route('user.dashboard') !!}" >Dashboard</a></li>
													<li><a href="{!! route('user.profile') !!}" >Edit profile</a></li>
												<?php endif; ?>
												<li><a href="{!! url('logout') !!}" >Logout</a></li>
											</ul>
										</div>
									</li>
								</ul>
		                    </div>
		                 <?php endif; ?>
		                 
						<nav id="menu">
							<ul>
								<li><a href="{!! url('/') !!}" >Home</a></li>
								<li><a href="#" >About Us</a></li>
								<li><a href="#" >Contact Us</a></li>
								<?php if(Auth::check()): ?>
								<?php else: ?>
									<li><a data-href="{!! url('login') !!}" class="semi_bold itfs-pop-up" role="button" id="itfuserlogins">Sign in / Register</a></li>
								<?php endif; ?>
							</ul>
						</nav>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</header>
 * 
 */?>