<!--Footer-->
  	<footer class="row site-footer">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-7 col-md-push-5 half-side">
  					<div class="footer-contact row footer-widget right-box">
  						<h3 class="footer-title">Contact Us</h3>
  						<ul class="nav">
  							<li class="tel"><a href="#"><i class="fa fa-phone"></i>+91 8093203113</a></li>
  							<li class="email"><a href="mailto:support@thepauseplay.com"><i class="fa fa-envelope-o"></i>support@thepauseplay.com</a></li>
  							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
  							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
  							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
  							<li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
  						</ul>
  					</div>
  					<div class="footer-newsletter row footer-widget right-box">
  						<h3 class="footer-title">newsletter sign up</h3>
  						<form class="form-inline newsletter-form">
							<input type="text" class="form-control" placeholder="Name">
							<input type="email" class="form-control" placeholder="Email">
							<button type="submit" class="btn btn-primary">submit</button>
						</form>
  					</div>
  				</div>
  				<div class="col-md-5 col-md-pull-7 half-side">
  					<div class="footer-about left-box footer-widget row">
  						<h3 class="footer-title">About Us</h3>
  						<p>Lorem ipsum dolor sit amet, consectetur  some dymmy adipiscing elit. Nam turpis quam, sodales in text she ante sagittis, varius efficitur mauris. Nam turpis quam, sodales in text should be able. to...</p>
  					</div>
  					<div class="copyright-row left-box footer-widget row">
                                            ©Copyright <?php echo date('Y'); ?> Pause Play. All Rights Reserved. Designed by <a target="_blank" href="https://itfosters.com/">IT Fosters</a>
  					</div>
  				</div>
  			</div>
  		</div>
  	</footer>
    
    <script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"  type="text/javascript"></script>
    <script src="{{URL::asset('js/bootstrap.min.js')}}"  type="text/javascript"></script>
    <script src="{{URL::asset('vendors/magnific-popup/jquery.magnific-popup.min.js')}}"  type="text/javascript"></script>
    <script src="{{URL::asset('vendors/owlcarousel/owl.carousel.min.js')}}"  type="text/javascript"></script>
    <script src="{{URL::asset('vendors/couterup/jquery.counterup.min.js')}}"  type="text/javascript"></script>
    <script src="{{URL::asset('vendors/waypoint/waypoints.min.js')}}"  type="text/javascript"></script>
    <script src="{{URL::asset('vendors/revolution/js/jquery.themepunch.tools.min.js?rev=5.0')}}"  type="text/javascript"></script>
    <script src="{{URL::asset('vendors/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0')}}"  type="text/javascript"></script>
    <script src="{{URL::asset('vendors/revolution/js/extensions/revolution.extension.video.min.js')}}"  type="text/javascript"></script>
    <script src="{{URL::asset('vendors/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"  type="text/javascript"></script>
    <script src="{{URL::asset('vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"  type="text/javascript"></script>
    <script src="{{URL::asset('vendors/revolution/js/extensions/revolution.extension.navigation.min.js')}}"  type="text/javascript"></script>
    <script src="{{URL::asset('js/theme.js')}}"  type="text/javascript"></script>



@stack('scripts-footer') 