<a id="toggle" href="#">
	<i class="fa fa-bars"></i>
</a>
<main id="content">
	<div id="overlay"></div>
	<header>
		<div class="header">
			<div class="container from-top">
				<div class="">
					<div class="col-md-3 col-sm-3 col-xs-12">
						<div class="logo">
							<div class="logo-inner">
								<a href="{!! url('/') !!}">
									<!-- <img src="{{ asset('images/logo.png') }}"/> -->
								</a>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-md-9 col-sm-9 col-xs-12>">

						<div class="avtar menu">
							<ul>
								<li>
									<div class="dropdown">
										<a id="dLabel" href="#itf" class="semi_bold" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
										<?php echo Auth::user()->name; ?>
											<span class="caret"></span>
											<?php if(!empty(Auth::user()->user_photo)){ ?>
												<img src="{{asset('siteimage/users/'.Auth::user()->user_photo)}}" class="circle50 hidden-xs"/>
											<?php }else{ ?>
												<img src="{{asset('images/profile.png')}}" class="circle50 hidden-xs"/>
											<?php } ?>
										</a>

										<ul class="dropdown-menu" aria-labelledby="dLabel">
											
											<?php if(Auth::user()->user_type=="M"): ?>
												<li><a href="{!! route('mentor.dashboard') !!}" >Dashboard</a></li>
												<li><a href="{!! route('mentor.profile') !!}" >Edit profile</a></li>
											<?php elseif(Auth::user()->user_type=="A"): ?>
												<li><a href="{!! route('admin.dashboard') !!}" >Dashboard</a></li>
												<li><a href="{!! route('admin.profile') !!}" >Edit profile</a></li>
											<?php else: ?>
												<li><a href="{!! route('user.dashboard') !!}" >Dashboard</a></li>
												<li><a href="{!! route('user.profile') !!}" >Edit profile</a></li>
											<?php endif; ?>
											<li><a href="{!! url('logout') !!}" >Logout</a></li>
										</ul>
									</div>
								</li>
							</ul>
	                    </div>

						<nav id="menu">
							<ul>
								<li><a href="{!! url('/') !!}" >Home</a></li>
								<li><a href="#" >About Us</a></li>
								<li><a href="#" >Contact Us</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</header>