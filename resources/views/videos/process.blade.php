@extends('layouts.user')
@section('title', 'Video Show')

@push('scripts-headers')
<style type="text/css" href="{{URL::asset('css/videoplay.css')}}" rel="stylesheet"></style>
@endpush

@push('scripts-footer')

<script src="{{URL::asset('js/angular.min.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/angular-sanitize.min.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itfvideo.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-controls.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/youtube.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-poster.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-overlay-play.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-buffering.js')}}"  type="text/javascript"></script>
<style>
    .fa-check-circle {
  color: green;
}
    .fa-times-circle {
  color: red;
}

    .razorpay-payment-button{
        position: absolute;
        right: 65px;
        bottom: 7px;
    }
</style>
<script type="text/javascript">
'use strict';
        angular.module('itfmirrors',
            ["ngSanitize","com.2fdevs.videogular","com.2fdevs.videogular.plugins.controls","info.vietnamcode.nampnq.videogular.plugins.youtube","com.2fdevs.videogular.plugins.poster","com.2fdevs.videogular.plugins.overlayplay","com.2fdevs.videogular.plugins.buffering"],
            function($interpolateProvider) {
                $interpolateProvider.startSymbol('<%');
                $interpolateProvider.endSymbol('%>');
            })
            .filter('itfDurations',function(){
                return function (s) {
                    var ms = s % 1000;
                    s = (s - ms) / 1000;
                    var secs = s % 60;
                    s = (s - secs) / 60;
                    var mins = s % 60;
                    var hrs = (s - mins) / 60;
                    secs=(secs>9)?secs:"0"+secs;
                    mins=(mins>9)?mins:"0"+mins;

                    if(hrs>0)
                    return hrs + ':' + mins + ':' + secs; 
                        else       
                    return mins + ':' + secs;        
                };
            })
            .controller('HomeCtrl',
                ["$sce", function ($sce) {
                    this.config = {
                        preload: "none",
                        sources: [
                            {src: $sce.trustAsResourceUrl("{{ asset("site/videos/".$data['video']->video_file) }}"), type: "video/mp4"},
                        ],
                        theme: {
                            url: "{{URL::asset('css/itfvideo.css')}}"
                        },
                        plugins: {
                            poster: "{{ asset("site/videoimg/".$data['video']->video_image) }}",
                            controls: {
                                autoHide: false,
                                autoHideTime: 5000
                            }
                        }
                    };
                    this.onPlayerReady = function onPlayerReady(API) { this.API = API; };
                }]
            );

jQuery(document).ready(function(){
    
    function calculatePrice(){
        var totalmentors = jQuery('.mentorslists:input:checkbox:checked').length;
        var totalprices=0;
        var couponCount = jQuery('#coupon-code').val();

        jQuery('.packagelist:input:checkbox:checked').each(function(i,j){
            totalprices+=parseFloat(parseFloat(totalmentors) * parseFloat(jQuery(j).attr("data-price")));
        });
        if(couponCount == 0){
            jQuery("#totalprices").html("Rs. "+totalprices);
            jQuery('#totalPrices').val(totalprices);
            jQuery("#payment-btn").show();
            // jQuery("#payment-form").find('.razorpay-payment-button').addClass('btn bg-primary');
           // jQuery("#payment-form script").attr('data-amount',totalprices);
            jQuery("#subnmit-review").hide();
        }else{
            jQuery("#totalprices").html("Rs. 0");
            jQuery('#totalPrices').val(totalprices);
            jQuery("#payment-btn").hide();
            jQuery("#subnmit-review").show();
        }
        
    }
    jQuery('.packagelist,.mentorslists').change(function(){ calculatePrice();});
    
    //validation of package

    jQuery(document).on('change', '.mentorslists', function() {
        if(!($(":checkbox[name='packages[]']").is(":checked")))
        {
            alert("Please select package first !");
            $(this).prop('checked', false);
            $(window).scrollTop($('#totalpricesection').offset().top);
        }
    });
    jQuery(document).on('click', '.bg-primary', function() {
        if(!($(":checkbox[name='packages[]']").is(":checked")))
        {
            alert("Please select package first !");
            $(this).prop('checked', false);
            $(window).scrollTop($('#totalpricesection').offset().top);
            return false;
        }
        if(!($(":checkbox[name='mentors[]']").is(":checked")))
        {
            alert("Please select mentors !");
            $(this).prop('checked', false);
            $(window).scrollTop($('#totalpricesection').offset().top);
            return false;
        }
        var couponValue = jQuery("#coupon-code").val();
        if(couponValue == 0){
            jQuery("#payment-btn").show();
            jQuery("#subnmit-review").hide();
            
        }else{
            jQuery("#payment-btn").hide();
            jQuery("#subnmit-review").show();
            
        }
    });
    jQuery(document).on('keyup', '#couponcode', function() {
        var pricecodevalue = jQuery("#totalprices").html();
        var CodeValue = jQuery('#couponcode').val();
        jQuery.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        type: "POST",
        url: '/couponcheck',
        data: {'code':CodeValue}
        }) 
        .done(function(success) {
            if(success.status=='success'){
               //jQuery('#responsevalue').html(" ");
               jQuery('.fa-check-circle').show();
               jQuery("#totalprices").html('Rs. 0');
               jQuery('.fa-times-circle').hide();
                //if(success.packageName == 'Executive'){
                if(success.packageName == 'Executive-100 words'){
                    $('#packages1').prop('checked',true);
                }
                //if(success.packageName == 'Middle'){
                if(success.packageName == 'Middle-500 words'){
                    $('#packages2').prop('checked',true);
                }
                //if(success.pacakgeName == 'Elite'){
                if(success.packageName == 'Elite - 700 words'){
                    $('#packages3').prop('checked',true);
                }
               $('.packageslist').hide();
               $('#coupon-code').val(1);
               jQuery("#payment-btn").hide();
               jQuery("#subnmit-review").show();
       
            }else{
               //jQuery('#responsevalue').html("");
            //    $('#packages3').prop('checked', false);
               $('.packageslist').show();
               $('#packages1').prop('checked',false);
               $('#packages2').prop('checked',false);
               $('#packages3').prop('checked',false);
               jQuery('.fa-check-circle').hide();
               jQuery("#totalprices").html(pricecodevalue);
               jQuery('.fa-times-circle').show();
               jQuery('#coupon-code').val(0);
               jQuery("#payment-btn").show();
               jQuery("#subnmit-review").hide();
               
            }
        })
       
    });
});
</script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
    var SITEURL = '{{URL::to('')}}';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    }); 
    $('body').on('click', '#pay-with-razor', function(e){
    var totalAmount = $("#totalPrices").val();
    var video_id =  "<?php echo $data["video"]->id ?>";
    var inputs = $('#video-form').serializeArray();
    var options = {
    //"key": "rzp_test_s6jSbXL0nxYKQ4",
    "key": "rzp_live_RC8hMFCElX3wcF",
    "amount": (totalAmount*100), // 2000 paise = INR 20
    "name": "Maynardleigh Associates",
    "description": "Payment",
    "image": "https://sixer.maynardleighonline.in/assests/img/default.jpgg",
    "handler": function (response){
            $.ajax({
            url: '/video/payment',
            type: 'post',
            dataType: 'json',
            data: {
            razorpay_payment_id: response.razorpay_payment_id , 
                totalAmount : totalAmount,
                videoId : video_id,
                formInputs : inputs,

            }, 
            success: function (data) {
    
                if(data.status == 'success'){
                    alert("Payment complete successfully and video has been sent for review");
                    window.location.href = SITEURL+'/video/'+data.id;
                }
            }
        });
        
    },
    "prefill": {
        "contact": '<?php echo $data['userInfo']->phone_no ?>',
        "email":   '<?php echo $data['userInfo']->email ?>',
    },
    "theme": {
        "color": "#528FF0"
    }
    };
    var rzp1 = new Razorpay(options);
    rzp1.open();
    e.preventDefault();
    });
    /*document.getElementsClass('buy_plan1').onclick = function(e){
    rzp1.open();
    e.preventDefault();
    }*/
</script>
@endpush
@section('content')
<div class="borderwhite">
    <div class="bordergrey">
        <div class="col-md-12">
            <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>{{ $data["video"]->title }}
                <div class="border-lightgrey mt5"></div>    
            </div>

            {!! Form::open(['route' => array('video.processing',$data["videoid"]),'method' => 'post', 'name' => 'video-form','id' => 'video-form','class'=>'form-horizontal']) !!}
                <div class="col-md-8">
                    <div class="row">
                        
                        <!--h5 class="semi_bold">{{ $data["video"]->title }}&nbsp;</h5 -->
                        <?php //print_r($data["errors"]); ?>

                        <div class="rows_content">
                            <label><strong>Context :</strong></label>
                            <div class="contensts"><p>{{$data['video']->background}}</p></div>
                        </div>

                        <div class="rows_content">
                            <label><strong>Description :</strong></label>
                            <div class="contensts"><p>{{$data['video']->description}}</p></div>
                        </div>

                        <div class="rows_content mb10">
                            <label><strong>Purpose :</strong></label>
                            <div class="contensts"><p>{{$data['video']->ask_question}}</p></div>
                        </div>

                        <b>Added:</b>  {{ date("d M,Y",strtotime($data["video"]->created_at)) }}
                        <!-- div class="borderdash-lightgrey mt10 mb10"></div -->
                        <div class="clearfix mb10"></div>
                        <div class="video-area mr10">
                            <div ng-app="itfmirrors" class="plaeryblock">
                                <div ng-controller="HomeCtrl as controller" class="videogular-container">
                                    <videogular vg-theme="controller.config.theme.url" vg-player-ready="controller.onPlayerReady($API)">
                                        <vg-media vg-src="controller.config.sources"></vg-media>

                                        <vg-controls vg-autohide="controller.config.plugins.controls.autoHide" vg-autohide-time="controller.config.plugins.controls.autoHideTime">
                                            <vg-play-pause-button></vg-play-pause-button>
                                            
                                            <vg-scrub-bar>
                                                <vg-scrub-bar-current-time></vg-scrub-bar-current-time>
                                            </vg-scrub-bar>
                                            
                                            
                                            <vg-time-display><% timeLeft|itfDurations %></vg-time-display>
                                        
                                            <vg-volume>
                                                <vg-mute-button></vg-mute-button>
                                                <vg-volume-bar></vg-volume-bar>
                                            </vg-volume>
                                            <vg-fullscreen-button></vg-fullscreen-button>
                                        </vg-controls>
                                        <vg-overlay-play></vg-overlay-play>
                                        <vg-poster vg-url='controller.config.plugins.poster' class="itfpostals"></vg-poster>
                                        <vg-buffering></vg-buffering>
                                    </videogular>
                                </div>
                            </div>

                        </div>
                        
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="row">
                        <div class="semi_bold grandtotalprice" id="totalpricesection">Total Price : <span id="totalprices">Rs. 00.00</span></div>
                        <input  type="hidden" name="totalPrice" id="totalPrices" value="">
                        <h5 class="semi_bold">Select Package</h5>
                        <div class="borderdash-lightgrey mt10"></div>
                        
                        
                        <h5 class="semi_bold">Do you have Promo code?</h5>
                        <div class="list-menu ">
                        <input placeholder="Promo code put here." class="packagelist" id="couponcode" value=""  name="couponcode"  type="text">
                        <input type="hidden" name="couponcount" id="coupon-code" value="0">
                        <span id="responsevalue">
                           <i class="fa fa-check-circle fa-3x" style="display:none;" aria-hidden="true" ></i>
                           <i class='fa fa-times-circle fa-3x' style="display:none;" aria-hidden="true"></i>
                        </div>
                           
                        
                        <div class="list-menu packageslist">
                            <ul>
                            @foreach ($data["packages"] as $package)  
                                <li>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-9 col-sm-9 col-xs-9">                                              
                                                <div class="small-text">
                                                    <a href="#package"  data-toggle="modal" data-target="#{{$package->id}}">{{$package->title}} ( Get detail )</a>
                                                </div>
                                                
                                                <!-- making Model start-->
                                                
                                                <div class="modal fade" id="{{$package->id}}" role="dialog">
                                                    <div class="modal-dialog modal-sm">
                                                        <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Package Detail</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Package Name: <strong>{{$package->title}}</strong></p>
                                                            <p>Description: <strong>{{$package->description}}</strong></p>
                                                            <p>Cost : <strong>Rs.{{$package->price}}</strong></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- making Model end-->
                                                
                                                <label class="semi_bold">Rs. {{$package->price}}</label>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                                <div class="squaredTwo">
                                                    <input class="packagelist" id="packages{{$package->id}}" value="{{$package->id}}" data-price="{{$package->price}}" name="packages[]" type="checkbox">
                                                    <label for="packages{{$package->id}}"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach     
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>


                    <div class="row">
                        <h5 class="semi_bold mt40">Select Mentors</h5>
                        <div class="borderdash-lightgrey mt10"></div>
                        <div class="list-menu">
                            <ul>
                                @foreach ($data["mentors"] as $mentro)
                                <?php $profileinfo =$mentro->profile()->first(); ?>
                                <li>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <a href="{!! route('profiledetail',$mentro->username) !!}" target="_blank">
                                                <?php if(!empty($mentro->user_photo)){ ?>
                                                    <img src="{{asset('siteimage/users/'.$mentro->user_photo)}}" class="circle50"/>
                                                <?php }else{ ?>
                                                    <img src="{{asset('images/profile.png')}}" class="circle50"/>
                                                <?php } ?>
                                            </a>
                                        </div>

                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <label class="semi_bold"><a href="{!! route('profiledetail',$mentro->username) !!}" target="_blank">{{$mentro->name}}</a></label>
                                                <div class="small-text">Experience: {{(int)$profileinfo->total_experience}} Years</div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                                <div class="squaredTwo">
                                                    <input class="mentorslists" id="mentorsch{{$mentro->id}}" value="{{$mentro->id}}" name="mentors[]" type="checkbox">
                                                    <label for="mentorsch{{$mentro->id}}"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                 @endforeach     
                                
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div> 


                <div class="clearfix"></div>
               
                
                 <div class="col-md-12">
                    
                    <div class="mb10 mt30 text-right">
                        <button class="btn bg-primary" id="subnmit-review">Submit for Review</button>
                    </div>
                    <div class="mb10 mt30 text-right" style="display:none" id="payment-btn">
                        <button class="btn bg-primary" id="pay-with-razor">Pay with Razorpay</button>
                    </div>
                </div>

            {!! Form::close() !!}

        </div> 
        <div class="clearfix"></div>
     </div>
@stop