@extends('layouts.admin')
@section('title', 'Video Show')

@push('scripts-headers')
<style type="text/css" href="{{URL::asset('css/videoplay.css')}}" rel="stylesheet"></style>
@endpush

@push('scripts-footer')

<script src="{{URL::asset('js/angular.min.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/angular-sanitize.min.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itfvideo.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-controls.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/youtube.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-poster.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-overlay-play.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-buffering.js')}}"  type="text/javascript"></script>

<script type="text/javascript">
'use strict';
        angular.module('itfmirrors',
            ["ngSanitize","com.2fdevs.videogular","com.2fdevs.videogular.plugins.controls","info.vietnamcode.nampnq.videogular.plugins.youtube","com.2fdevs.videogular.plugins.poster","com.2fdevs.videogular.plugins.overlayplay","com.2fdevs.videogular.plugins.buffering"],
            function($interpolateProvider) {
                $interpolateProvider.startSymbol('<%');
                $interpolateProvider.endSymbol('%>');
            })
            .filter('itfDurations',function(){
                return function (s) {
                    var ms = s % 1000;
                    s = (s - ms) / 1000;
                    var secs = s % 60;
                    s = (s - secs) / 60;
                    var mins = s % 60;
                    var hrs = (s - mins) / 60;
                    secs=(secs>9)?secs:"0"+secs;
                    mins=(mins>9)?mins:"0"+mins;

                    if(hrs>0)
                    return hrs + ':' + mins + ':' + secs; 
                        else       
                    return mins + ':' + secs;        
                };
            })
            .controller('HomeCtrl',
                ["$sce","$scope", function ($sce,$scope) {

                    //$templateCache.removeAll();
                    $scope.commenttitle={}
                    var allcomment=<?php echo json_encode($data["video_comment"]); ?>;
                    var allcommenttime ={}
                    var i=0,kk=-1;
                    for(i=0;i<allcomment.length;i++){
                        if(allcommenttime[parseInt(allcomment[i].video_time)]==undefined){
                            allcommenttime[parseInt(allcomment[i].video_time)]={};
                        }
                        allcommenttime[parseInt(allcomment[i].video_time)][allcomment[i].category_id]=allcomment[i].comments;
                    }
                    //console.log(allcommenttime);

                    this.onUpdateTime=function(currentTime,duration){
                        if(allcommenttime[parseInt(currentTime)]!=undefined && kk!=parseInt(currentTime)){
                            this.API.pause();
                            kk=parseInt(currentTime);
                            console.log(allcommenttime[parseInt(currentTime)]);
                            $scope.commenttitle=allcommenttime[parseInt(currentTime)]
                        }
                    };

                    this.config = {
                        preload: "none",
                        sources: [
                            {src: $sce.trustAsResourceUrl("{{ asset("site/videos/".$data['video']->video_file) }}"), type: "video/mp4"},
                        ],
                        theme: {
                            url: "{{URL::asset('css/itfvideo.css')}}"
                        },
                        plugins: {
                            poster: "{{ asset("site/videoimg/".$data['video']->video_image) }}",
                            controls: {
                                autoHide: false,
                                autoHideTime: 5000
                            }
                        }
                    };
                    this.onPlayerReady = function onPlayerReady(API) { this.API = API; };

                    this.playPositionTime=function(tt){
                        this.API.seekTime(tt);
                        //this.API.play();
                     }
                }]
            );
</script>
@endpush

@section('content')
<div class="borderwhite" ng-app="itfmirrors">
    <div class="bordergrey" ng-controller="HomeCtrl as controller" >
        <div class="col-md-12">
            <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>{{ $data["video"]->title }} 

                <span class="dateinfo pull-right"><b>Added:</b>  {{ date("d M,Y",strtotime($data["video"]->created_at)) }}</span>
                <div class="border-lightgrey mt5"></div>    
            </div>

            
                <div class="col-md-7">
                    <div class="row"> 
                        
                        <div class="clearfix mb10"></div>
                        <div class="video-area">
                            <div  class="plaeryblock">
                                <div class="videogular-container">
                                    <videogular vg-theme="controller.config.theme.url" vg-player-ready="controller.onPlayerReady($API)" vg-update-time="controller.onUpdateTime($currentTime, $duration)">
                                        <vg-media vg-src="controller.config.sources" ></vg-media>

                                        <vg-controls vg-autohide="controller.config.plugins.controls.autoHide" vg-autohide-time="controller.config.plugins.controls.autoHideTime">
                                            <vg-play-pause-button></vg-play-pause-button>
                                            
                                            <vg-scrub-bar>
                                                <vg-scrub-bar-current-time></vg-scrub-bar-current-time>
                                            </vg-scrub-bar>
                                            
                                            
                                            <vg-time-display><% timeLeft|itfDurations %></vg-time-display>
                                        
                                            <vg-volume>
                                                <vg-mute-button></vg-mute-button>
                                                <vg-volume-bar></vg-volume-bar>
                                            </vg-volume>
                                            <vg-fullscreen-button></vg-fullscreen-button>
                                        </vg-controls>
                                        <vg-overlay-play></vg-overlay-play>
                                        <vg-poster vg-url='controller.config.plugins.poster' class="itfpostals"></vg-poster>
                                        <vg-buffering></vg-buffering>

                                    </videogular>
                                </div>
                            </div>
                            <div class="video_comments">
                                <ul>
                                    <li ng-repeat="comtt in commenttitle"><% comtt %></li>
                                </ul>
                            </div>
                        </div>


                        

                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="mt30"> 
                                <div class="regular-black_head"> 
                                    <i class="fa fa-comment ">&nbsp;</i>Your recent comments
                                    <div class="borderdash-lightgrey mt5"></div>    
                                </div>
                                <?php if(isset($data["video_comment"]) and count($data["video_comment"])>0){?>

                                    @foreach ($data["video_comment"] as $comments)

                                    <div class="mt10 mb10">  
                                        <span class="blackTex pull-right"><i class="fa fa-play" role="button" ng-click="controller.playPositionTime({{$comments->video_time}})"></i></span>
                                        <span class="blackText italic">{{$comments->Category()->title}}</span>                    
                                        <p>{{$comments->comments}} <span class="badge ml10">{{gmdate("H:i:s", $comments->video_time)}}</span></p>
                                        <div class="borderdash-lightgrey"></div>
                                    </div>

                                    @endforeach   
                                <?php }else{ ?>
                                    <div class="mb30 text-center">Review is empty</div>
                                    <div class="borderdash-lightgrey mt5"></div>  
                                <?php } ?>
                               
                            </div>
                        </div>

                        
                    </div>
                </div>


                <div class="col-md-5">
                    <div class="rows">
                        <h4 class="semi_bold">Mentors</h4>
                        <div class="borderdash-lightgrey mt10"></div>
                        <div class="list-menu">
                            <ul>
                            @foreach ($data["mentors"] as $mentordt) 
                             <?php $mentorsinfo = $mentordt->User()->first(); ?>
                             <?php $mentorsinfodetail = $mentorsinfo->profile()->first(); ?>
                                <li>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <a href="{!! route('profiledetail',$mentorsinfo->username) !!}" target="_blank">
                                                <?php if(!empty($mentorsinfo->user_photo)){ ?>
                                                    <img src="{{asset('siteimage/users/'.$mentorsinfo->user_photo)}}" class="circle50"/>
                                                <?php }else{ ?>
                                                    <img src="{{asset('images/profile.png')}}" class="circle50"/>
                                                <?php } ?>
                                            </a>
                                        </div>

                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <label class="semi_bold"><a href="{!! route('profiledetail',$mentorsinfo->username) !!}" target="_blank">{{$mentorsinfo->name}}</a></label>
                                                <div class="small-text">Experience: {{(int)$mentorsinfodetail->total_experience}} years</div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </li>
                            @endforeach   
                               
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <h4 class="semi_bold">Video Full Detail</h4>
                        <div class="borderdash-lightgrey mt10"></div>
                        <div class="rows_content">
                            <label><strong>Context :</strong></label>
                            <div class="contensts"><p>{{$data['video']->background}}</p></div>
                        </div>

                        <div class="rows_content">
                            <label><strong>Description :</strong></label>
                            <div class="contensts"><p>{{$data['video']->description}}</p></div>
                        </div>

                        <div class="rows_content mb10">
                            <label><strong>Purpose :</strong></label>
                            <div class="contensts"><p>{{$data['video']->ask_question}}</p></div>
                        </div>

                        
                    </div>
                    <div class="clearfix"></div>
                </div> 


                <div class="clearfix"></div>
            

        </div> 
        <div class="clearfix"></div>
     </div>
</div> 
@stop