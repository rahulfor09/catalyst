@extends('layouts.admin')
@section('title', 'Video')
@section('content')
<div class="row">

<div class="borderwhite">
                    <div class="bordergrey">
                        <div class="regular-black_head"> <i class="fa fa-money">&nbsp;</i>Payment Details
                        <div class="border-lightgrey mt5"></div>    
                        </div>

                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th>S.no</th>
                                    <th>Date of transaction</th>
                                    <th>User's Name</th>
                                    <th>User's Company</th>
                                    <th>Video Title</th>
                                    <th>Payment/Coupon</th>
                                    <th>Amount</th>
                                    <th>Payment Status</th>
                                    <th class="actionblock">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <?php //echo "<pre>qqq";print_r($videosdatas);die; ?>
                                @foreach ($videosdatas as $key => $videosdata)
                                    <td>{{ ($videosdatas->currentpage()-1) * $videosdatas->perpage() + $key + 1 }}</td>
                                    <td>
                                        @foreach($data['payment'] as $payment)
                                            @if($videosdata->id == $payment['videoId'])
                                                <p><?php echo date("jS F, Y",strtotime($payment['created_at'])); ?></p>
                                            @endif
                                        @endforeach
                                        @foreach($data['couponCode'] as $coupon)
                                            @if($videosdata->id == $coupon['videoId'])
                                                <p><?php echo date("jS F, Y",strtotime($coupon['created_at'])); ?></p>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{$videosdata->name}} {{$videosdata->last_name}}</td>
                                    <td>{{$videosdata->company}}</td>
                                    <td>
                                        <?php
                                            $string = $videosdata->title;
                                            if (strlen($string) >50) {

                                                // truncate string
                                                $stringCut = substr($string, 0, 50);
                                                $endPoint = strrpos($stringCut, ' ');

                                                //if the string doesn't contain any space then it will cut without word basis.
                                                $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                                $string .= '....';
                                            }
                                            echo $string;
                                        ?>
                                    </td>
                                    <td>
                                        @foreach($data['payment'] as $payment)
                                            @if($videosdata->id == $payment['videoId'])
                                                <p>{{$payment['razor_payment_id']}}</p>
                                            @endif
                                        @endforeach
                                        @foreach($data['couponCode'] as $coupon)
                                            @if($videosdata->id == $coupon['videoId'])
                                                <p>{{$coupon['code']}}</p>
                                            @endif
                                        @endforeach
                                    </td>

                                      <!-- @foreach($data['payment'] as $payment)
                                          @if($videosdata->id == $payment['videoId'])
                                            <a href="#payment-details"  data-toggle="modal" data-target="#{{$payment['videoId']}}"><?php echo $payment['status']; ?></a>
                                            <div class="modal fade" id="{{$payment['videoId']}}" role="dialog">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Payment Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Payment Id: <strong>{{$payment['razor_payment_id']}}</strong></p>
                                                        <p>Amount: <strong>Rs.{{$payment['amount']}}</strong></p>
                                                        <p>Status : <strong>{{$payment['status']}}</strong></p>
                                                        <p>Transaction Date : <strong>{{$payment['created_at']}}</strong></p>
                                                        </div>
                                                        <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                          @endif
                                      @endforeach -->
                                    <td>
                                        @foreach($data['payment'] as $payment)
                                            @if($videosdata->id == $payment['videoId'])
                                                <p>{{$payment['amount']}}</p>
                                            @endif
                                        @endforeach
                                        @foreach($data['couponCode'] as $coupon)
                                            @if($videosdata->id == $coupon['videoId'])
                                                <p>N/A</p>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($data['payment'] as $payment)
                                            @if($videosdata->id == $payment['videoId'])
                                                <p>{{$payment['status']}}</p>
                                            @endif
                                        @endforeach
                                        @foreach($data['couponCode'] as $coupon)
                                            @if($videosdata->id == $coupon['videoId'])
                                                <p>N/A</p>
                                            @endif
                                        @endforeach
                                    </td>
                                    <!-- <td>
                                        <i class="fa fa-upload"></i>{{$videosdata->created_at->format('d M Y')}}<br/>
                                        @if(isset($videosdata->assingdated) and !empty($videosdata->assingdated)>0)
                                        <i class="fa fa-arrow-circle-right"></i>{{date('d M Y', strtotime($videosdata->assingdated))}}<br/>
                                        @endif
                                        @if(isset($videosdata->last_comment) and !empty($videosdata->last_comment)>0)
                                        <i class="fa fa-book"></i>{{date('d M Y', strtotime($videosdata->last_comment))}}
                                        @endif
                                    </td> -->
                                    <td>
                                        <a href="{{ route('admin.video.show',$videosdata->id) }}" class="mb10 btn btn-primary"> <i class="fa fa-eye">&nbsp;</i></a>
                                                    {{ Form::open(['method' => 'DELETE','route' => ['admin.video.delete', $videosdata->id],'class'=>'form-btns itfdeletefrm']) }}
                                                        {{ Form::hidden('id', $videosdata->id) }}
                                                        {!! Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger','type'=>'submit']) !!}
                                                    {{ Form::close() }}
                                    </td>
                                  </tr>
                                   @endforeach 
                                </tbody>
                              </table>
                              <div class="clearfix"></div>

                        </div>
                        
                        <div class="mb10">
                            <nav aria-label="...">
                                <?php echo $videosdatas->render(); ?>
                            </nav>
                         </div>
                         <div class="clearfix"></div>
                    </div>
                </div>
   
</div>

@stop