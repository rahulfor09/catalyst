@extends('layouts.user')
@section('title', 'Video Show')

@section('content')
<div class="borderwhite">
    <div class="bordergrey">
        <div class="col-md-12">
            <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>{{ $data["video"]->title }}
                <div class="border-lightgrey mt5"></div>    
            </div>

            {!! Form::open(['url' => 'video','method' => 'post', 'name' => 'video-form','id' => 'video-form','class'=>'form-horizontal']) !!}
                <div class="col-md-8">
                    <div class="row">
                        
                        <!--h5 class="semi_bold">{{ $data["video"]->title }}&nbsp;</h5 -->
                        

                        <div class="rows_content">
                            <label><strong>Context :</strong></label>
                            <div class="contensts"><p>{{$data['video']->background}}</p></div>
                        </div>

                        <div class="rows_content">
                            <label><strong>Description :</strong></label>
                            <div class="contensts"><p>{{$data['video']->description}}</p></div>
                        </div>

                        <div class="rows_content mb10">
                            <label><strong>Purpose :</strong></label>
                            <div class="contensts"><p>{{$data['video']->ask_question}}</p></div>
                        </div>

                        <b>Added:</b>  {{ date("d M,Y H:i:s A",strtotime($data["video"]->created_at)) }}
                        <!-- div class="borderdash-lightgrey mt10 mb10"></div -->
                        <div class="clearfix mb10"></div>
                        <div class="video-area">
                            <img src="{{ asset("site/videoimg/".$data['video']->video_image) }}" class="responsive">    
                        </div>
                        
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <h5 class="semi_bold mt40">Add Mentor</h5>
                        <div class="borderdash-lightgrey mt10"></div>
                        <div class="list-menu">
                            <ul>

                                <li>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <img src="{{ asset('images/images1.jpg') }}" class="circle50"/>
                                        </div>

                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <label class="semi_bold">Lorem Ipsum</label>
                                                <div class="small-text">Experience: 10years</div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                                <div class="squaredTwo">
                                                    <input id="mentorsch1" value="None" name="mentors[]" type="checkbox">
                                                    <label for="mentorsch1"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <img src="{{ asset('images/images1.jpg') }}" class="circle50"/>
                                        </div>

                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <label class="semi_bold">Lorem Ipsum</label>
                                                <div class="small-text">Experience: 10years</div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                                <div class="squaredTwo">
                                                    <input id="mentorsch2" value="None" name="mentors[]" type="checkbox">
                                                    <label for="mentorsch2"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>


                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div> 


                <div class="clearfix"></div>
                <div class="col-md-8">
                    <div class="mb10 mt30 text-right">
                        <button class="btn bg-primary">Submit for Review</button>
                    </div>
                </div>

            {!! Form::close() !!}

        </div> 
        <div class="clearfix"></div>
     </div>
</div> 
@stop