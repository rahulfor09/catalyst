@extends('layouts.user')
@section('title', 'Video Show')

@push('scripts-headers')
<style type="text/css">
.videogular-container {
    width: 100%;
    height: 320px;
    margin: auto;
}
@media (min-width: 1200px) {
    .videogular-container {
        width: 550px;
        height: 350px;
    }
    .videogular-container.audio {
        width: 550px;
        height: 50px;
    }
}
@media (min-width: 992px) and (max-width: 1199px) {
    .videogular-container {
        width: 300px;
        height: 250px;
    }

    .videogular-container.audio {
        width: 300px;
        height: 50px;
    }
}
@media (min-width: 768px) and (max-width: 991px) {
    .videogular-container {
        width: 250px;
        height: 190px;
    }
    .videogular-container.audio {
        width: 250px;
        height: 50px;
    }
}
</style>
@endpush
@push('scripts-footer')
<script src="{{URL::asset('js/angular.min.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/angular-sanitize.min.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itfvideo.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-controls.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/youtube.js')}}"  type="text/javascript"></script>
<script type="text/javascript">
    'use strict';
        angular.module('itfmirrors',
            ["ngSanitize","com.2fdevs.videogular","com.2fdevs.videogular.plugins.controls","info.vietnamcode.nampnq.videogular.plugins.youtube"],
            function($interpolateProvider) {
                $interpolateProvider.startSymbol('<%');
                $interpolateProvider.endSymbol('%>');
            })
            .filter('itfDurations',function(){
                return function (s) {
                    var ms = s % 1000;
                    s = (s - ms) / 1000;
                    var secs = s % 60;
                    s = (s - secs) / 60;
                    var mins = s % 60;
                    var hrs = (s - mins) / 60;
                    secs=(secs>9)?secs:"0"+secs;
                    mins=(mins>9)?mins:"0"+mins;

                    if(hrs>0)
                    return hrs + ':' + mins + ':' + secs; 
                        else       
                    return mins + ':' + secs;        
                };
            })
            .controller('HomeCtrl',
                ["$sce", function ($sce) {
                    this.config = {
                        preload: "none",
                        sources: [
                            {src: $sce.trustAsResourceUrl("{{ asset("site/videos/".$data['video']->video_file) }}"), type: "video/mp4"},
                        ],
                        theme: {
                            url: "{{URL::asset('css/itfvideo.css')}}"
                        },
                        plugins: {
                            controls: {
                                autoHide: false,
                                autoHideTime: 5000
                            }
                        }
                    };
                    this.onPlayerReady = function onPlayerReady(API) { this.API = API; };
                }]
            );
</script>
@endpush

@section('content')
<div class="borderwhite">
    <div class="bordergrey">
        <div class="col-md-12">
            <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>Video
                <div class="border-lightgrey mt5"></div>    
            </div>

            {!! Form::open(['url' => 'video','method' => 'post', 'name' => 'video-form','id' => 'video-form','class'=>'form-horizontal']) !!}
                <div class="col-md-8">
                    <div class="row">
                        
                        <h5 class="semi_bold">{{ $data["video"]->title }}&nbsp;</h5>
                        <b>Added:</b>  {{ date("d M,Y H:i:s A",strtotime($data["video"]->created_at)) }}
                        <div class="borderdash-lightgrey mt10 mb10"></div>
                        <div class="video-area">

                            <div ng-app="itfmirrors" class="plaeryblock">
                                <div ng-controller="HomeCtrl as controller" class="videogular-container">
                                    <videogular vg-theme="controller.config.theme.url" vg-player-ready="controller.onPlayerReady($API)">
                                        <vg-media vg-src="controller.config.sources"></vg-media>

                                        <vg-controls vg-autohide="controller.config.plugins.controls.autoHide" vg-autohide-time="controller.config.plugins.controls.autoHideTime">
                                            <vg-play-pause-button></vg-play-pause-button>
                                            
                                            <vg-scrub-bar>
                                                <vg-scrub-bar-current-time></vg-scrub-bar-current-time>
                                            </vg-scrub-bar>
                                            
                                            
                                            <vg-time-display><% timeLeft|itfDurations  %></vg-time-display>
                                        
                                            <vg-volume>
                                                <vg-mute-button></vg-mute-button>
                                                <vg-volume-bar></vg-volume-bar>
                                            </vg-volume>
                                            <vg-fullscreen-button></vg-fullscreen-button>
                                        </vg-controls>
                                    </videogular>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <h5 class="semi_bold mt40">Add Mentor</h5>
                        <div class="borderdash-lightgrey mt10"></div>
                        <div class="list-menu">
                            <ul>

                                <li>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <img src="{{ asset('images/images1.jpg') }}" class="circle50"/>
                                        </div>

                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <label class="semi_bold">Lorem Ipsum</label>
                                                <div class="small-text">Experience: 10years</div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                                <div class="squaredTwo">
                                                    <input id="mentorsch1" value="None" name="mentors[]" type="checkbox">
                                                    <label for="mentorsch1"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <img src="{{ asset('images/images1.jpg') }}" class="circle50"/>
                                        </div>

                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <label class="semi_bold">Lorem Ipsum</label>
                                                <div class="small-text">Experience: 10years</div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                                <div class="squaredTwo">
                                                    <input id="mentorsch2" value="None" name="mentors[]" type="checkbox">
                                                    <label for="mentorsch2"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>


                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div> 


                <div class="clearfix"></div>
                <div class="col-md-8">
                    <div class="mb10 mt30 text-right">
                        <button class="btn bg-primary">Submit for Review</button>
                    </div>
                </div>

            {!! Form::close() !!}

        </div> 
        <div class="clearfix"></div>
     </div>
</div> 
@stop