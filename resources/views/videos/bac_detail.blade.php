@extends('layouts.mentor')
@section('title', 'Video Show')

@push('scripts-headers')
<style type="text/css" href="{{URL::asset('css/videoplay.css')}}" rel="stylesheet"></style>
@endpush

@push('scripts-footer')

<script src="{{URL::asset('js/angular.min.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/angular-sanitize.min.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itfvideo.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-controls.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/youtube.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-poster.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-overlay-play.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-buffering.js')}}"  type="text/javascript"></script>

<script type="text/javascript">
'use strict';
        angular.module('itfmirrors',
            ["ngSanitize","com.2fdevs.videogular","com.2fdevs.videogular.plugins.controls","info.vietnamcode.nampnq.videogular.plugins.youtube","com.2fdevs.videogular.plugins.poster","com.2fdevs.videogular.plugins.overlayplay","com.2fdevs.videogular.plugins.buffering"],
            function($interpolateProvider) {
                $interpolateProvider.startSymbol('<%');
                $interpolateProvider.endSymbol('%>');
            })
            .filter('itfDurations',function(){
                return function (s) {
                    var ms = s % 1000;
                    s = (s - ms) / 1000;
                    var secs = s % 60;
                    s = (s - secs) / 60;
                    var mins = s % 60;
                    var hrs = (s - mins) / 60;
                    secs=(secs>9)?secs:"0"+secs;
                    mins=(mins>9)?mins:"0"+mins;

                    if(hrs>0)
                    return hrs + ':' + mins + ':' + secs; 
                        else       
                    return mins + ':' + secs;        
                };
            })
            .filter('itfDuration', function($filter)
            {
               return function(s) {
                    return new Date(1970, 0, 1).setSeconds(s);
                };
            })
            .controller('HomeCtrl',
                ["$sce","$scope","$http", function ($sce,$scope,$http) {

                    $scope.mycomments=[];
                    $scope.comments={};
                    $scope.post_comments=function(){

                        console.log($scope.comments);
                        
                        $http({
                            url: "{{route('video.comments',$data['video']->id)}}",
                            method: "POST",
                            data: $scope.comments
                        })
                        .then(function(response) {
                                $scope.loadComments();
                                $scope.comments.yourcomment='';
                        }, 
                        function(response) { });
                    }

                    this.onUpdateTime=function(currentTime,duration){
                        $scope.comments.currentTimes = currentTime;
                    };

                    this.config = {
                        preload: "none",
                        sources: [
                            {src: $sce.trustAsResourceUrl("{{ asset("site/videos/".$data['video']->video_file) }}"), type: "video/mp4"},
                        ],
                        theme: {
                            url: "{{URL::asset('css/itfvideo.css')}}"
                        },
                        plugins: {
                            poster: "{{ asset("site/videoimg/".$data['video']->video_image) }}",
                            controls: {
                                autoHide: false,
                                autoHideTime: 5000
                            }
                        }
                    };
                    this.onPlayerReady = function onPlayerReady(API) { this.API = API; };

                    $scope.loadComments=function(){
                        $http.get("{{route('video.comments',$data['video']->id)}}")
                        .then(function(response) {
                            $scope.mycomments = response.data;
                        });
                    }

                    $scope.loadComments();
                }]
            );
</script>
@endpush
@section('content')
<div class="col-md-8" ng-app="itfmirrors">
    <div class="borderwhite" ng-controller="HomeCtrl as controller">
        <div class="bordergrey">
            <div class="col-md-12">
                <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>{{ $data["video"]->title }}
                    <div class="border-lightgrey mt5"></div>    
                </div>

                
                    
                    <div class="col-md-12">
                        <div class="row">

                            <div class="rows_content">
                                <label><strong>Context :</strong></label>
                                <div class="contensts"><p>{{$data['video']->background}}</p></div>
                            </div>

                            <div class="rows_content">
                                <label><strong>Discription :</strong></label>
                                <div class="contensts"><p>{{$data['video']->description}}</p></div>
                            </div>

                            <div class="rows_content mb10">
                                <label><strong>Purpose :</strong></label>
                                <div class="contensts"><p>{{$data['video']->ask_question}}</p></div>
                            </div>

                            <b>Added:</b>  {{ date("d M,Y H:i:s A",strtotime($data["video"]->created_at)) }}
                            
                            <div class="clearfix mb10"></div>
                            <div class="video-area mr10">
                                <div  class="plaeryblock">
                                    <div class="videogular-container">
                                        <videogular vg-theme="controller.config.theme.url" vg-player-ready="controller.onPlayerReady($API)" vg-update-time="controller.onUpdateTime($currentTime, $duration)">
                                            <vg-media vg-src="controller.config.sources"></vg-media>

                                            <vg-controls vg-autohide="controller.config.plugins.controls.autoHide" vg-autohide-time="controller.config.plugins.controls.autoHideTime">
                                                <vg-play-pause-button></vg-play-pause-button>
                                                
                                                <vg-scrub-bar>
                                                    <vg-scrub-bar-current-time></vg-scrub-bar-current-time>
                                                </vg-scrub-bar>
                                                
                                                
                                                <vg-time-display><% timeLeft|itfDurations  %></vg-time-display>
                                            
                                                <vg-volume>
                                                    <vg-mute-button></vg-mute-button>
                                                    <vg-volume-bar></vg-volume-bar>
                                                </vg-volume>
                                                <vg-fullscreen-button></vg-fullscreen-button>
                                            </vg-controls>
                                            <vg-overlay-play></vg-overlay-play>
                                            <vg-poster vg-url='controller.config.plugins.poster' class="itfpostals"></vg-poster>
                                            <vg-buffering></vg-buffering>
                                        </videogular>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>


                    <div class="clearfix"></div>

                    {!! Form::open(['route' => array('video.detail',$data["videoid"]),'method' => 'post', 'name' => 'frmcomments','id' => 'frmcomments','class'=>'form-horizontal']) !!}
                    <div class="col-md-12 mt30">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="sel1">Current Video Time :</label>
                                <input type="text" ng-model="comments.currentTimes" class="form-control" disabled="disabled" >
                            </div>

                            <div class="col-md-8">
                                <label for="sel1">Select Category:</label>
                                    {{ Form::select('category', $data["categories"],null, ['class' => 'form-control','ng-model'=>'comments.category','required'=>'','ng-init'=>"comments.category=1"]) }}                        
                            </div>

                            <div class="col-md-12">
                                  <label>Your Comments</label>
                                  <textarea class="form-control" rows="2" id="comment" ng-model="comments.yourcomment" required="required"></textarea>
                            </div>

                            <div class="col-md-12 mt10 text-right">
                                <input type="button" class="btn bg-primary" ng-click="post_comments()" ng-disabled="frmcomments.$invalid" value="Submit">
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}


                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="mt30 "> 
                            <div class="regular-black_head"> 
                                <i class="fa fa-comment ">&nbsp;</i>Recent Your Comments
                                <div class="borderdash-lightgrey mt5"></div>    
                            </div>
                            <div class="mt10 mb10" ng-repeat="comt in mycomments">  
                                <span class="blackTex pull-right"><i class="fa fa-trash-o" role="button"></i></span>
                                <span class="blackText italic"><% comt.category.title %></span>                    
                                <p><% comt.comments %> <span class="badge ml10"><% comt.video_time|itfDuration|date:'HH:mm:ss' %></span></p>
                                <div class="borderdash-lightgrey"></div>
                            </div>
                           
                        </div>
                    </div>

            </div> 
            <div class="clearfix"></div>
         </div>
    </div> 
</div>

<div class="col-md-4 mb30">
    <div class="borderwhite">
        <div class="bordergrey">
            
            <div class="nav-side-menu2">
                <div class="mb20 blue-bg10 white-text padding10">Recent Video</div>
                <div class="row">
                    <div class="borderdash-lightgrey mt10"></div>
                     @if(isset($data["latestadded"]) and count($data["latestadded"])>0)
                        <div class="list-menu">
                            <ul>
                            @foreach ($data["latestadded"] as $key => $latestvideos)  
                                <?php $latestvideo =$latestvideos->Video()->first(); ?> 
                                <li>
                                    <a href="{{ route('video.detail',$latestvideo->id) }}">
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                <img class="circle50" src="{{ asset("site/videoimg/".$latestvideo->video_image) }}" />
                                            </div>

                                            <div class="col-md-10 col-sm-10 col-xs-10">
                                                <div class="col-md-12 col-sm-12 col-xs-12">                                              
                                                    <div class="small-text">{{$latestvideo->title}}</div>
                                                    <label class="semi_bold">{{$latestvideo->created_at->format('d M Y H:i A')}}</label>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endforeach     
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="clearfix"></div>

            </div>
        </div>
    </div>
</div>

@stop