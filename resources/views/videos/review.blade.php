@extends('layouts.mentor')
@section('title', 'Pending Review Videos')
@section('content')
<div class="row">

<div class="borderwhite">
                    <div class="bordergrey">
                        <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>Pending Review Videos
                        <div class="border-lightgrey mt5"></div>    
                        </div>
                        
                            <div class="list-styled2">
                                <ul> 
                                @foreach ($videosdatas as $datareview)    
                                <?php $videosdata =$datareview->Video()->first(); ?>  
                                <?php $usersinfo =$videosdata->Owner()->first(); 
                                //echo "<pre>USER";print_r($usersinfo);die;
                                ?> 
                                    <li>                                       
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-xs-12">
                                                <div class="circle90">
                                                <img src="{{ asset("site/videoimg/".$videosdata->video_image) }}" />
                                                </div>
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-xs-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-7">
                                                        {{$videosdata->title}}
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-5 text-right">
                                                    {{$videosdata->created_at->format('d M Y H:i A')}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p>{{str_limit($videosdata->description,20,'...')}}</p>
                                                        <p>Uploaded By: {{$usersinfo->name}} {{$usersinfo->last_name}} ({{$usersinfo->company}})</p>
                                                    </div>
                                                    <div class="col-md-6 text-right mt5">
                                                        <a href="{{ route('video.detail',$videosdata->id) }}" class="mb10 btn btn-primary"> <i class="fa fa-eye">&nbsp;</i>View</a>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li> 
                                @endforeach                                   
                                </ul>
                            </div>
                            <div class="mb10">
                                
                                <nav aria-label="...">
                                    <?php echo $videosdatas->render(); ?>
                                </nav>
                             </div>
                    </div>
                </div>
   
</div>

@stop