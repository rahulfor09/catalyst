@extends('layouts.mentor')
@section('title', 'Video Show')

@push('scripts-headers')
<style type="text/css" href="{{URL::asset('css/videoplay.css')}}" rel="stylesheet"></style>
@endpush

@push('scripts-footer')

<script src="{{URL::asset('js/angular.min.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/angular-sanitize.min.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itfvideo.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-controls.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/youtube.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-poster.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-overlay-play.js')}}"  type="text/javascript"></script>
<script src="{{URL::asset('js/itf-buffering.js')}}"  type="text/javascript"></script>

<script type="text/javascript">
'use strict';
        angular.module('itfmirrors',
            ["ngSanitize","com.2fdevs.videogular","com.2fdevs.videogular.plugins.controls","info.vietnamcode.nampnq.videogular.plugins.youtube","com.2fdevs.videogular.plugins.poster","com.2fdevs.videogular.plugins.overlayplay","com.2fdevs.videogular.plugins.buffering"],
            function($interpolateProvider) {
                $interpolateProvider.startSymbol('<%');
                $interpolateProvider.endSymbol('%>');
            })
            .filter('itfDurations',function(){
                return function (s) {
                    var ms = s % 1000;
                    s = (s - ms) / 1000;
                    var secs = s % 60;
                    s = (s - secs) / 60;
                    var mins = s % 60;
                    var hrs = (s - mins) / 60;
                    secs=(secs>9)?secs:"0"+secs;
                    mins=(mins>9)?mins:"0"+mins;

                    if(hrs>0)
                    return hrs + ':' + mins + ':' + secs; 
                        else       
                    return mins + ':' + secs;        
                };
            })
            .filter('itfDuration', function($filter)
            {
               return function(s) {
                    return new Date(1970, 0, 1).setSeconds(s);
                };
            })
            .controller('HomeCtrl',
                ["$sce","$scope","$http", function ($sce,$scope,$http) {

                    $scope.mycomments=[];
                    $scope.comments={};
                    $scope.post_comments=function(){
                        $http({
                            url: "{{route('video.comments',$data['video']->id)}}",
                            method: "POST",
                            data: $scope.comments
                        })
                        .then(function(response) {
                                $scope.loadComments();
                                $scope.comments.category=null;
                                $scope.comments.yourcomment=null;
                                $('.emoji-wysiwyg-editor').empty();
                        }, 
                        function(response) {  });
                        
                    }
                    $scope.post_comments_save=function(currentId){
                        var currentText = $("#comment"+currentId).val();
                        $http({
                            url: "{{route('video.updatecomment',$data['video']->id)}}",
                            method: "POST",
                            data: {'currentId':currentId,'savetext':currentText}
                        })
                        .then(function(response) {
                                $scope.loadComments();
                                $scope.comments.yourcomment='';
                                alert("Data Saved!");
                                $('#'+currentId).modal('toggle');
                        }, 
                        function(response) {});
                    }

                    $scope.delete_comments=function(index){
                       if(confirm("Are you sure to delete this comment?")){

                       $http({
                            url: "{{route('video.comment.delete')}}",
                            method: "POST",
                            data: {id:index}
                        })
                        .then(function(response) {
                                if(response.data.status==1){
                                    $scope.loadComments();
                                }else{

                                }
                        }, 
                        function(response) { });
                    }
                    }
              
                    $scope.confirm_comments=function(){
                        if(confirm("Are you sure to send the review?")){
                            $http({
                                url: "{{route('video.comments.confirm',$data['video']->id)}}",
                                method: "POST"
                            })
                            .then(function(response) {
                                    $scope.loadComments();
                                    if(response.data.status>0){
                                        alert("Now video review has been shared with the respective user.");
                                        window.location.href='/mentor/dashboard';
                                    }
                            }, 
                            function(response) { });
                        }
                    }

                    

                    this.onUpdateTime=function(currentTime,duration){
                        $scope.comments.currentTimes = currentTime;
                    };

                    this.config = {
                        preload: "none",
                        sources: [
                            {src: $sce.trustAsResourceUrl("{{ asset("site/videos/".$data['video']->video_file) }}"), type: "video/mp4"},
                        ],
                        theme: {
                            url: "{{URL::asset('css/itfvideo.css')}}"
                        },
                        plugins: {
                            poster: "{{ asset("site/videoimg/".$data['video']->video_image) }}",
                            controls: {
                                autoHide: false,
                                autoHideTime: 5000
                            }
                        }
                    };
                    this.onPlayerReady = function onPlayerReady(API) { this.API = API; };

                    $scope.loadComments=function(){
                        $http.get("{{route('video.comments',$data['video']->id)}}")
                        .then(function(response) {
                            $scope.mycomments = response.data;
                        });
                    }
                    
                    $scope.chnageSizeofBox=function(){
                        var sizefile =  $('#divcontrollersize').attr('ng-value');
                        if(sizefile=='large'){
                            $('#videodivnew').removeClass('col-md-6');
                            $('#videodivnew2').removeClass('col-md-6');
                            $('#videodivnew').addClass('col-md-9');
                            $('#videodivnew2').addClass('col-md-3');
                            $("#divcontrollersize").attr("ng-value","small");
                            $("#divcontrollersize").html("View video in small screen");
                        }else{
                            $('#videodivnew').removeClass('col-md-9');
                            $('#videodivnew2').removeClass('col-md-3');
                            $('#videodivnew').addClass('col-md-6');
                            $('#videodivnew2').addClass('col-md-6');
                            $("#divcontrollersize").attr("ng-value","large");
                            $("#divcontrollersize").html("View video in large screen");
                        }
                    }
                    $scope.loadComments();
                }]
            );
</script>
@endpush
@section('content')

    <div class="borderwhite" ng-app="itfmirrors" >
        <div class="bordergrey" ng-controller="HomeCtrl as controller">
            <div class="col-md-12">
                <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>{{ $data["video"]->title }}
                    <span class="dateinfo pull-right"><b>Added:</b>  {{ date("d M,Y H:i:s A",strtotime($data["video"]->created_at)) }}</span>
                    <div class="border-lightgrey mt5"></div>    
                </div>
                    <div id="videodivnew" class="col-md-6">
                        <div class="row">
                            <div class="clearfix mb10"></div>
                            <div class="video-area mr10">
                                <div  class="plaeryblock">
                                    <div class="videogular-container">
                                        <videogular vg-theme="controller.config.theme.url" vg-player-ready="controller.onPlayerReady($API)" vg-update-time="controller.onUpdateTime($currentTime, $duration)">
                                            <vg-media vg-src="controller.config.sources"></vg-media>
                                            <vg-controls vg-autohide="controller.config.plugins.controls.autoHide" vg-autohide-time="controller.config.plugins.controls.autoHideTime">
                                                <vg-play-pause-button></vg-play-pause-button>
                                                <vg-time-display><% currentTime | itfDurations %></vg-time-display>
                                                <vg-scrub-bar>
                                                    <vg-scrub-bar-current-time></vg-scrub-bar-current-time>
                                                </vg-scrub-bar>
                                                <vg-time-display><% totalTime|itfDurations %></vg-time-display>
                                                <vg-volume>
                                                    <vg-mute-button></vg-mute-button>
                                                    <vg-volume-bar></vg-volume-bar>
                                                </vg-volume>
                                                <vg-fullscreen-button></vg-fullscreen-button>
                                            </vg-controls>
                                            <vg-overlay-play></vg-overlay-play>
                                            <vg-poster vg-url='controller.config.plugins.poster' class="itfpostals"></vg-poster>
                                            <vg-buffering></vg-buffering>
                                        </videogular>
                                    </div>
                                </div>
                                
                            </div>
                            <div  style="float:right"><a id="divcontrollersize" href="#changesize" ng-value="large" ng-click="chnageSizeofBox()">View video in large screen </a></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <div class="row mt20">
                             <div class="rows_content">
                                <label><strong>Context :</strong></label>
                                <div class="contensts"><p>{{$data['video']->background}}</p></div>
                            </div>
                            <div class="rows_content">
                                <label><strong>Discription :</strong></label>
                                <div class="contensts"><p>{{$data['video']->description}}</p></div>
                            </div>
                            <div class="rows_content mb10">
                                <label><strong>Purpose :</strong></label>
                                <div class="contensts"><p>{{$data['video']->ask_question}}</p></div>
                            </div>
                        </div>
                    </div>
                    <div id="videodivnew2" class="col-md-6">
                    <div class="rows">

                        {!! Form::open(['route' => array('video.detail',$data["videoid"]),'method' => 'post', 'name' => 'frmcomments','id' => 'frmcomments','class'=>'form-horizontal']) !!}
                           
                                <div class="row mt10">
                                    <div class="col-md-12">
                                        <label for="sel1">Current Video Time :</label>
                                        <span ng-model="comments.currentTimes" class="datainfo"><% comments.currentTimes %></span>
                                        <input type="hidden" ng-model="comments.currentTimes" class="form-control" disabled="disabled" >
                                    </div>

                                    <div class="col-md-12">
                                        <label for="sel1">Select Category:</label>
                                            {{ Form::select('category', $data["categories"],null, ['class' => 'form-control','ng-model'=>'comments.category','required'=>'','ng-init'=>"comments.category=1"]) }}                        
                                    </div>

                                    <div class="col-md-12">
                                          <label>Your Comments</label>
                                          <textarea data-emojiable="true" class="form-control" rows="2" id="comment" ng-model="comments.yourcomment" required="required"></textarea>
                                    </div>

                                    <div class="col-md-12 mt10 text-right">
                                        <input type="button" class="btn bg-primary" ng-click="post_comments()" ng-disabled="frmcomments.$invalid" value="Submit">
                                    </div>
                                </div>
                           
                            {!! Form::close() !!}

                        <div class="mt30 "> 
                                <div class="regular-black_head"> 
                                    <i class="fa fa-comment ">&nbsp;</i>Your recent comments
                                    <div class="borderdash-lightgrey mt5"></div>    
                                </div>
                                <div class="mt10 mb10" ng-repeat="comt in mycomments">  
                                    <span ng-if="comt.status<=0" class="blackTex pull-right" ng-click="delete_comments(comt.id)"><i class="fa fa-trash-o" role="button"></i></span>
                                    
                                    <span class="blackText italic"><% comt.category.title %></span>                    
                                    <p><% comt.comments %><span class="badge ml10"><% comt.video_time|itfDuration|date:'HH:mm:ss' %></span>
                                    <div class="small-text">
                                                    <a href="#package"  data-toggle="modal" data-target="#<%comt.id%>"><i class="fa fa-pencil-square-o" role="button"></i></a>
                                                </div>
                                    </p>
                                
                                    
                                    <!-- making Model start-->
                                                
                                                <div class="modal fade" id="<%comt.id%>" role="dialog">
                                                    {!! Form::open(['route' => array('video.updatecomment'),'method' => 'post', 'name' => 'updatecomment','id' => 'updatecomment','class'=>'form-horizontal']) !!}
                                                    <div class="modal-dialog modal-sm">
                                                        <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Edit</h4>
                                                            </div>
                                                            
                                                            <div class="modal-body">
                                                                <span><strong>For permanent save you have to click on save button.</strong></span>
                                                                <textarea cols="35" rows="7"  class="form-control"  id="comment<%comt.id%>" ng-model="comt.comments" required="required"><% comt.comments %></textarea>
                                                                <input type="hidden" ng-model="comt.id" value="<%comt.id%>" id="corrent_commentId" />
                                                            </div>
                                                            <div class="modal-footer">
                                                            <input type="button" class="btn bg-primary" ng-click="post_comments_save(comt.id)" ng-disabled="updatecomment.$invalid" value="Save">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     {!! Form::close() !!}
                                                </div>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <div class="borderdash-lightgrey"></div>
                                </div>

                                <div class="col-md-12 mt10 text-center" ng-show="mycomments.length>0">
                                    <input type="button" class="btn bg-primary" ng-click="confirm_comments()" value="Send review">
                                </div>
                               
                        </div>
                        <div class="clearfix"></div>
                        
                    </div>
                    <div class="clearfix"></div>
                </div> 

            </div> 
            <div class="clearfix"></div>
         </div>
    </div> 
<script>
      jQuery(function() {
        // Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          //assetsPath: 'http://localhost/themirror/public/img',
          assetsPath: '/public/img',
          popupButtonClasses: 'fa fa-smile-o'
        });
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
      });
    </script>
@stop