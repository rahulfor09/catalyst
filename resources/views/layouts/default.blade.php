<!doctype html>
<html>
<head>
    @include('includes.head')
    @stack('styles-head')
</head>
<body>

@include('includes.header')
@yield('content')
@include('includes.footer') 
</body>
</html>