<!doctype html>
<html>
<head>
    @include('includes.head')
    @stack('styles-head')
</head>
<body>

@if (Auth::check())
@include('includes.headeruser')
@else
@include('includes.header')
@endif
@yield('content')
@include('includes.footer') 
</body>
</html>