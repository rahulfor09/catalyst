<!doctype html>
<html>
<head>
    @include('includes.head')
    @stack('styles-head')
</head>
<body>

@include('includes.headeruser')

<section class="inner-bg pb30 pt30">
	<div class="container-fluid">
	    <div class="col-md-12 col-sm-12 col-xs-12 ">
	    	
            @if(session()->has('message'))
                <div class="col-md-12">
	                <div class="alert alert-success">
	                	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    {{ session()->get('message') }}
	                </div>
                </div>
            @endif
	        <div class="res-row">	            
	            <div class="col-md-3 mb30">
	                @include('includes.mentorleft')
	            </div>
              	<div class="col-md-9">
					@yield('content')
				</div>
			</div>      
	    </div>
	</div>
</section>

@include('includes.footer') 
</body>
</html>