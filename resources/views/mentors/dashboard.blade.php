@extends('layouts.mentor')
@section('title', 'Dashboard')

@section('content')
<div class="row">
   
     <div class="col-md-6 mb30 height400">
        <div class="borderwhite">
            <div class="bordergrey">
                <div class="text-center regular-heading"> <i class="fa fa-envelope">&nbsp;</i>Courses and training</div>
                @if(isset($data["latestadded"]) and count($data["latestadded"])>0)
                    <div class="list-menu">
                            <ul> 
                              <?php //echo "<pre>";print_r($data["latestadded"]);die; ?>
                             @foreach( $data["latestadded"] as $key => $latestvideo )  
                             <?php $latestvideoImage = $latestvideo->Video()->first(); ?>
                             <?php $usersinfo =$latestvideo->Owner()->first(); ?>                            
                                <li>                               
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <img src="{{ asset("site/videoimg/".$latestvideoImage->video_image) }}" class="circle50"/>
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-7">
                                                    {{$latestvideo->title}}
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-5 text-right">
                                                <i class="fa fa-arrow-circle-right"></i>{{$latestvideo->created_at->format('d M Y')}}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p>Created by {{$usersinfo->name}} {{$usersinfo->last_name}}</p>
                                                </div>
                                                <div class="col-md-6 text-right mt5">
                                                <a href="{{ route('video.detail',$latestvideo->video_id) }}" class="mr10"> <i class="fa fa-eye">&nbsp;</i>View</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                @endforeach
                            </ul>
                        
                    </div>
                    <div class="mb10 mt30 text-center">
                        <a  href="{{ route('videos.review') }}" class="btn bg-primary">Show More</a>
                    </div>
                @else
                    <div class="mb10 mt30 text-center">
                        <p>You have not added new Courses.</p>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- <div class="col-md-6 mb30 height400">
        <div class="borderwhite">
            <div class="bordergrey">
                <div class="text-center regular-heading"> <i class="fa fa-video-camera">&nbsp;</i>Reflection Videos</div>
                    @if(isset($data["reviewedfile"]) and count($data["reviewedfile"])>0)
                    <div class="list-menu">
                            <ul>
                             <?php //echo "<pre>";print_r($data["reviewedfile"]);die; ?>
                             @foreach( $data["reviewedfile"] as $key => $latestvideos )   

                             <?php $latestvideo =$latestvideos->Video()->first(); 
                                $usersinfo =$latestvideo->Owner()->first();
                             ?>                        
                                <li>                               
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <img src="{{ asset("site/videoimg/".$latestvideo->video_image) }}" class="circle50"/>
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-7">
                                                    {{$latestvideo->title}}
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-5 text-right">
                                                    
                                                    <i class="fa fa-book"></i>{{date('d M Y', strtotime($latestvideos->last_comment))}}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-7">
                                                    
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-5 text-right">
                                                   <i class="fa fa-arrow-circle-right"></i>{{$latestvideos->created_at->format('d M Y')}}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p>Created by {{$usersinfo->name}} {{$usersinfo->last_name}}</p>
                                                </div>
                                                <div class="col-md-6 text-right mt5">
                                                <a href="{{ route('video.detail',$latestvideo->id) }}" class="mr10"> <i class="fa fa-eye">&nbsp;</i>View</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                @endforeach
                            </ul>
                        
                    </div>
                    <div class="mb10 mt30 text-center">
                        <a  href="{{ route('videos.reviewed') }}" class="btn bg-primary">Show More</a>
                    </div>
                @else
                    <div class="mb10 mt30 text-center">
                        <p>You have no file Reviewd.</p>
                    </div>
                @endif

            </div>
        </div>
    </div> -->
</div>
@stop