@extends('layouts.userfull')
@section('title', 'Profile')

@section('content')

<?php $profileinfo = $data["userinfo"]->profile()->first(); //print_r($profileinfo); ?>
<section class="inner-bg pb30 pt30">
    <div class="container-fluid ">
        <div class="bordergrey">
            <div class="col-md-12">
                <div class="col-md-3">
                    <div class="bordergrey">
                        <div class="nav-side-menu">
                            <div class="text-center">
                                <div class="circle130 pimgchange">
                                    <?php if(!empty($data["userinfo"]->user_photo)){ ?>
                                        <img src="{{asset('siteimage/users/'.$data['userinfo']->user_photo)}}"/>
                                    <?php }else{ ?>
                                        <img src="{{asset('images/profile.png')}}"/>
                                    <?php } ?>
                                </div>
                                <div class="mt10">{{$data['userinfo']->name}} {{$data['userinfo']->last_name}}</div>
                                <div class="mb20">({{$profileinfo->post_title}})</div>
                           </div>
                           <ul>
                               <li></li>
                           </ul>

                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="mb10">
                        <h1 class="heading1">{{$data['userinfo']->name}} {{$data['userinfo']->last_name}}</h1>
                        <div class="border-lightgrey "></div>    
                    </div>
                    <div class="sub_heading">{{$profileinfo->post_title}}</div>
                    <div class="extra_detail">
                        <ul>
                            <li>Experience : {{$profileinfo->total_experience}} Yrs</li>
                        </ul>
                    </div>
                    <div class="content_detail">
                        {!! $profileinfo->description !!}
                    </div>

                </div>
            
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
@stop