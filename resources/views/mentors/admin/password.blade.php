@extends('layouts.admin')
@section('title', 'Profile')

@section('content')

<?php $profileinfo = $data["userinfo"]->profile()->first(); ?>
<div class="borderwhite">
    <div class="bordergrey">
            <div class="col-md-12">
            <div class="regular-black_head"> <i class="fa fa-key">&nbsp;</i>Reset Password for {{ $errors->first('name') }} ({{$data["userinfo"]->email}})
                <div class="border-lightgrey mt5"></div>    
            </div>
            <div class="row">

            {!! Form::open(['url' => route('admin.mentors.updatepassword',$data["userinfo"]->id),'method' => 'post', 'name' => 'profile-form','id' => 'profile-form','class'=>'form-horizontal']) !!}
            <div class="col-md-6">
            <label><i class="fa fa-file-text">&nbsp;</i>Password</label>
            <input id="name" name="password" class="form-control" type="password" placeholder="Password" value="">
            @if ($errors->has('password'))<span class="itferror">{{ $errors->first('password') }}</span>@endif
            </div>
            
            <div class="col-md-6">
            <label><i class="fa fa-file-text">&nbsp;</i>Confirm Password</label>
            <input id="last_name" name="confirm-password" class="form-control" type="password" placeholder="Confirm Password" value="">
            @if ($errors->has('confirm-password'))<span class="itferror">{{ $errors->first('confirm-password') }}</span>@endif
            </div>

            <div class="clearfix"></div>                                
            <div class="mb10 mt30 text-right col-md-12">
                <button class="btn bg-primary" type="submit" name="btnupdate">Submit</button>
            </div>

           {!! Form::close()!!}
           </div>
            </div>
            
            <div class="clearfix"></div>
    </div>
</div>
@stop