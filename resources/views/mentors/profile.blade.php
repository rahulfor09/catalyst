@extends('layouts.mentor')
@section('title', 'Profile')

@section('content')

<?php $profileinfo = $data["userinfo"]->profile()->first(); ?>
<div class="borderwhite">
    <div class="bordergrey">
            <div class="col-md-12">
            <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>Edit profile
                <div class="border-lightgrey mt5"></div>    
            </div>
            <div class="row">

            {!! Form::open(['url' => route('mentor.profile'),'method' => 'post', 'name' => 'profile-form','id' => 'profile-form','class'=>'form-horizontal']) !!}
            <div class="col-md-6">
            <label><i class="fa fa-file-text">&nbsp;</i>First Name</label>
            <input id="name" name="name" class="form-control" type="text" placeholder="First Name" value="{{(old('name'))? old('name') :$data["userinfo"]->name}}">
            @if ($errors->has('name'))<span class="itferror">{{ $errors->first('name') }}</span>@endif
            </div>
            
            <div class="col-md-6">
            <label><i class="fa fa-file-text">&nbsp;</i>Last Name</label>
            <input id="last_name" name="last_name" class="form-control" type="text" placeholder="Last Name" value="{{ (old('last_name'))? old('last_name') : $data['userinfo']->last_name}}">
            @if ($errors->has('last_name'))<span class="itferror">{{ $errors->first('last_name') }}</span>@endif
            </div>


            <div class="col-md-6">
                <label><i class="fa fa-file-text">&nbsp;</i>Total Experience</label>
                <input id="total_experience" name="total_experience" class="form-control" type="text" placeholder="Your Experience" value="{{(old('total_experience'))? old('total_experience') : $profileinfo->total_experience}}">
                @if ($errors->has('total_experience'))<span class="itferror">{{ $errors->first('total_experience') }}</span>@endif
            </div>

             <div class="col-md-6">
                <label><i class="fa fa-file-text">&nbsp;</i>Post Title</label>
                <input id="post_title" name="post_title" class="form-control" type="text" placeholder="Your Post title" value="{{(old('post_title'))? old('post_title') : $profileinfo->post_title}}">
                @if ($errors->has('post_title'))<span class="itferror">{{ $errors->first('post_title') }}</span>@endif
            </div>


            <div class="col-md-6">
                <label><i class="fa fa-file-text">&nbsp;</i>Phone No</label>
                <input id="phone_no" name="phone_no" class="form-control" type="text" placeholder="Phone Number" value="{{(old('phone_no'))? old('phone_no') : $data['userinfo']->phone_no}}">
                @if ($errors->has('phone_no'))<span class="itferror">{{ $errors->first('phone_no') }}</span>@endif
            </div>
                
            <div class="col-md-6">
                <label><i class="fa fa-file-text">&nbsp;</i>Email Id</label>
                <input id="emailid" name="emailid" class="form-control" type="email" placeholder="Your Email" value="{{$data["userinfo"]->email}}">
            </div>
            
            <div class="col-md-6">
            <label><i class="fa fa-file-text">&nbsp;</i>Update Password</label>
            <a href="{{ route('mentor.password',$data["userinfo"]->id) }}">Click here for change password</a>
            </div>
            
            
            <div class="col-md-12">
                <label><i class="fa fa-file-text">&nbsp;</i>Detail</label>
                <?php 
                $descriptions = (old('phone_no'))? old('phone_no') : (isset($profileinfo->description)?$profileinfo->description:""); ?>
                {!! Form::textarea('description', $descriptions, ['class'=>'form-control', 'placeholder'=>'Discription','rows'=>'6'])!!}
                @if ($errors->has('description'))<span class="itferror">{{ $errors->first('description') }}</span>@endif
            </div>
            <div class="clearfix"></div>                                
            <div class="mb10 mt30 text-right col-md-12">
                <button class="btn bg-primary" type="submit" name="btnupdate">Submit</button>
            </div>

           {!! Form::close()!!}
           </div>
            </div>
            
            <div class="clearfix"></div>
    </div>
</div>
@stop