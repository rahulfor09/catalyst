@extends('layouts.default')
@section('title', 'Register')
@push('scripts-footer')
@endpush

@section('content')
<div class="modal-dialog">
    <div class="modal-content">
        <!-- Begin # DIV Form -->
        <div id="div-forms" >
        
            <!-- Begin # Login Form -->
           <h2>Thank you for your registration.</h2> <h4>Please check your mail for login in to system</h4>
            
        </div>
        <!-- End # DIV Form -->
    </div>
    
</div>
@stop