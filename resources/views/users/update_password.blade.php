@extends('layouts.user')
@section('title', 'Profile')

@section('content')
<?php $profileinfo = $data["userinfo"]->profile()->first(); ?>
<div class="borderwhite">
    <div class="bordergrey">
            <div class="col-md-12">
            <div class="regular-black_head"> <i class="fa fa-key">&nbsp;</i>Reset Password for {{ $errors->first('name') }} ({{$data["userinfo"]->email}})
                <div class="border-lightgrey mt5"></div>    
            </div>
            <div class="row">
            @if($data["userinfo"]->user_type =='N')     
                {!! Form::open(['url' => route('user.updatepassword',$data["userinfo"]->id),'method' => 'post', 'name' => 'profile-form','id' => $data["userinfo"]->id,'class'=>'form-horizontal']) !!}
            @else
                {!! Form::open(['url' => route('mentor.updatepassword',$data["userinfo"]->id),'method' => 'post', 'name' => 'profile-form','id' => $data["userinfo"]->id,'class'=>'form-horizontal']) !!}
                @endif
            <div class="col-md-6">
            <label><i class="fa fa-file-text">&nbsp;</i>Password</label>
            <input id="password" name="password" class="form-control" type="password" placeholder="Password" value="">
            @if ($errors->has('password'))<span class="itferror">{{ $errors->first('password') }}</span>@endif
            </div>
            
            <div class="col-md-6">
            <label><i class="fa fa-file-text">&nbsp;</i>Confirm Password</label>
            <input id="confirm-password" name="confirm-password" class="form-control" type="password" placeholder="Confirm Password" value="">
            @if ($errors->has('confirm-password'))<span class="itferror">{{ $errors->first('confirm-password') }}</span>@endif
            </div>

            <div class="clearfix"></div>                                
            <div class="mb10 mt30 text-right col-md-12">
                <button class="btn bg-primary" type="submit" id="btnupdate" name="btnupdate">Submit</button>
            </div>

           {!! Form::close()!!}
           </div>
            </div>
            
            <div class="clearfix"></div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $("#btnupdate").click(function () {
            var password = $("#password").val();
            var confirmPassword = $("#confirm-password").val();
            if (password != confirmPassword) {
                alert("Password does not match.");
                return false;
            }
            return true;
        });
    });
</script>
@stop

