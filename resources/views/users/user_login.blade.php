@extends('layouts.default')
@section('title', 'Register')
@push('scripts-footer')
@endpush

@section('content')
<div class="modal-dialog">
    <div class="modal-content">
        <!-- Begin # DIV Form -->
        <div id="div-forms" >
        
            <!-- Begin # Login Form -->
            {!! Form::open(['url' => 'login','method' => 'post', 'name' => 'login-form','id' => 'login-form']) !!}
            
                
                <div class="modal-body">
                    <div id="div-login-msg">
                        <div id="icon-login-msg" class="fa fa-chevron-right"></div>
                        <span id="text-login-msgs">Type your User Id and password.</span>
                        <div class="clearfix"></div>
                        
                    </div>
                    <div id="messagedt">
                        
                    @if($errors->has('approve'))
                    <div class="error alert alert-danger">{{ $errors->first('approve') }}</div>
                    @endif



                    </div>
                    <div class="clearfix"></div>

                    <label>User Id</label>
                    <input id="login_username" name="email" class="form-control" type="text" placeholder="info@example.com" value="{{ old('email') }}">
                    @if($errors->has('email'))
                    <div class="error alert alert-danger">{{ $errors->first('email') }}</div>
                    @endif

                    
                    <label>Password</label>
                    <input id="login_password" name="password" class="form-control" type="password" placeholder="Password" >
                     @if($errors->has('password'))
                    <div class="error alert alert-danger">{{ $errors->first('password') }}</div>
                    @endif
                    <div class="checkbox">
                        <label>
                            <input class="normarcheckbox" type="checkbox" name="remember" id="btn_remembers" > Remember me
                        </label>
                         <a href="{{ url('/forgotpassword') }}">Forgot Your Password?</a>
                    </div> 
                       
                    <div class="mt20">
                        {{ csrf_field() }}
                        <button type="submit" name="login" class="btn btn-primary btn-lg btn-block">Login</button>
                    </div>
                </div>
                 
                <div class="modal-footer modalfooter-bg">
                  
                    <div class="text-center">
                        If you are not register please Click here&nbsp;<a href="{!! url('register') !!}" data-href="{!! url('register') !!}" id="login_register_btn" class="btn-link semi_bold itfs-pop-up">Sign Up</a>
                    </div>
                </div>
           {!! Form::close() !!}
            <!-- End # Login Form -->
            
        </div>
        <!-- End # DIV Form -->
    </div>
    <script type="text/javascript">
    /*    jQuery(document).ready(function(){

            jQuery("#login-form").submit(function(){
                
                jQuery.ajax({
                    url:"{!! url('login') !!}",
                    method:"POST",
                    dataType:"json",
                    data:jQuery("#login-form").serialize(),
                    success:function(res){
                        //console.log(res);
                        if(res.status=="success"){
                            window.location.href=res.redirect;
                        }else{
                            jQuery("#messagedt").html('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+res.message+'</div>');
                        }
                    }
                });
                return false;
            });


        });*/
    </script>
</div>
@stop