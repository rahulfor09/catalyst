@extends('layouts.default')
@section('title', 'Register')
@push('scripts-footer')
@endpush

@section('content')
<div class="modal-dialog">
    <div class="modal-content">
        <!-- Begin # DIV Form -->
        <div id="div-forms" >
        
            <!-- Begin | Lost Password Form -->
             {!! Form::open(['url' => 'forgotpassword','method' => 'post', 'name' => 'lost-form','id' => 'lost-form']) !!}
                <div class="modal-body">
                    <div id="div-lost-msg">
                        <div id="icon-lost-msg" class="glyphicon glyphicon-chevron-right"></div>
                        <span id="text-lost-msg">Type your e-mail.</span>
                    </div>
                    <div id="messagedt"></div>
                    <div class="clearfix"></div>

                    <label>Email Address</label>
                    <input id="lost_email" name="email" class="form-control" type="text" placeholder="E-Mail" required><br>
                    <div class="mt20">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Send</button>
                    </div>
                </div>
                <div class="modal-footer modalfooter-bg">
                    
                    <div  class="text-center">
                        <a class="Button" href="{{ url('/login') }}">Login</a>&nbsp;&nbsp;
                         <a class="Button" href="{{ url('/register') }}">Register</a>
                    </div>
                </div>
            {!! Form::close() !!}
            <!-- End | Lost Password Form -->
            
        </div>
        <!-- End # DIV Form -->
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function(){

            jQuery("#lost-form").submit(function(){
                
                jQuery('label.error').remove();
                jQuery(".form-control").removeClass("errors");

                jQuery.ajax({
                    url:"{!! url('forgotpassword') !!}",
                    method:"POST",
                    dataType:"json",
                    data:jQuery("#lost-form").serialize(),
                    success:function(res){
                        
                        if(res.status=="success"){
                            jQuery("#messagedt").html('<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+res.message+'</div>');

                           // window.location.href="{!! url('/') !!}";
                        }else{
                            jQuery("#messagedt").html('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+res.message+'</div>');
                            jQuery.each(res.errors, function(i, v) {
                                var msg = '<label class="error" for="'+i+'">'+v+'</label>';
                                jQuery('input[name="' + i + '"]').addClass('errors').after(msg);
                            });
                        }
                        
                    }
                });
                jQuery("#lost-form")[0].reset();
                return false;
            });
        });
    </script>
</div>
@stop