@extends('layouts.default')

@section('title', 'Welcome to The Mirror')

@push('scripts-footer')
<script>
    jQuery(document).ready(function($){
        jQuery('.parallax-window').parallax({naturalWidth: 600,naturalHeight: 400});

        jQuery("#frmnewsletter").submit(function(){
                jQuery.ajax({
                    url:"{{url('newsletter')}}",
                    method:"POST",
                    dataType:"json",
                    data:jQuery("#frmnewsletter").serialize(),
                    success:function(res){
                        console.log(res);
                        jQuery('label.error').remove();
                        jQuery(".form-control").removeClass("errors");

                        if(res.status=="success"){
                            jQuery("#newslettermsg").html(res.message);
                            jQuery("#newslettermsg")[0].reset();
                        }else{
                            jQuery.each(res.errors, function(i, v) {
                                jQuery('input[name="' + i + '"]').addClass('errors');
                            });
                        }
                    }
                });
                return false;
            });

            jQuery("#frmcontact").submit(function(){
                jQuery.ajax({
                    url:"{{url('contactus')}}",
                    method:"POST",
                    dataType:"json",
                    data:jQuery("#frmcontact").serialize(),
                    success:function(res){
                        console.log(res);
                        jQuery('label.error').remove();
                        jQuery(".form-control").removeClass("errors");
                        if(res.status=="success"){
                            jQuery("#contactmsg").html(res.message);
                            jQuery("#contactmsg").removeClass("hide");
                            jQuery("#contactmsg").addClass("alert alert-success");
                            jQuery("#frmcontact")[0].reset();

                        }else{
                            jQuery.each(res.errors, function(i, v) {
                                var msg = '<label class="error" for="'+i+'">'+v+'</label>';
                                jQuery('input[name="' + i + '"], select[name="' + i + '"], textarea[name="' + i + '"]').addClass('errors').after(msg);
                            });
                        }
                    }
                });
                return false;
            });
    })
</script>
@endpush

@section('content')
    
<section>
    <div class="banner">
        <div class="leftpanel"></div>
        <div class="right-bg rightside"></div>
        <div class="banner-content">
            <div class="video-img bounce">
                <img src="images/video-logo.png"/>
            </div>
            <h2 class="from-right ">Click </h2>
            <h1 class="h1 from-left">The Mirror</h1>
            <div class="banner-content-text from-bottom">
            <p class="italic">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...""There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container ">
    <div class="text-center marginbottom margintop from-bottom">
            <div class="title-heading mb40">
                <h3 class="heading-color">Lorem Ipsum</h3>
                <div class="title-border">
                    <div class="title-box"></div>
                </div>
            </div>
        <p class="small-text italic mb10">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
        <p class="textp">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
    </div>
    </div>
</section>


<section>
    <div class="banner2 ">
        <div class="leftside"></div>
        <div class="rightpanel"></div>
        <div class="banner-content">
            <div class="container">
                <div class="title-heading mb30">
                    <h3 class="heading">Lorem Ipsum</h3>
                    <div class="title-border2"></div>
                </div>
                <p class="white-text italic">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hide-animation slideUp">
                        <div class="box">
                        <div class="circle"><em>1</em></div>
                        <div class="square">
                            <div class="inner-square">
                                <div class="workicons">
                                    <i class="fa fa-users small-text"></i>
                                </div>
                            </div>
                        </div>
                        <div class="res">
                        <h4>Register</h4>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hide-animation slideUp">
                        <div class="box">
                        <div class="circle"><em>2</em></div>
                        <div class="square">
                            <div class="inner-square">
                                <div class="workicons">
                                    <i class="fa fa-video-camera "></i>
                                </div>
                            </div>
                        </div>
                        <div class="res">
                        <h4>Upload Your Video</h4>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3  hide-animation slideUp">
                        <div class="box">
                        <div class="circle"><em>3</em></div>
                        <div class="square">
                            <div class="inner-square">
                                <div class="workicons">
                                    <i class="fa fa-reply-all"></i>
                                </div>
                            </div>
                        </div>
                        <div class="res">
                        <h4>Get Mentor Review</h4>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hide-animation slideUp">
                        <div class="box">
                        <div class="circle"><em>4</em></div>
                        <div class="square">
                            <div class="inner-square">
                                <div class="workicons">
                                    <i class="fa fa-filter "></i>
                                </div>
                            </div>
                        </div>
                        <div class="res">
                        <h4>Analysis</h4>
                        </div>
                      </div>
                    </div>
                    
                </div>
            
            </div>
        </div>
    </div>
</section>

<section class="mt50 mb50">
    <div class="container from-bottom">
        <div class="text-center">
                <div class="title-heading  mb30">
                    <h3 class="heading-color ">Our Mentor</h3>
                    <div class="title-border">
                        <div class="title-box"></div>
                    </div>
                </div>
                
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
      
                
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                     <img class="circle2" src="images/images1.jpg"/>
                    <h4 class="small-text italic_bold">Vivek Arora</h4>
                    <h5 class="italic_bold">Co-founding Director</h5>
                    <p class="small-text italic">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                
                    <div class="item">
                     <img class="circle2" src="images/images1.jpg"/>
                    <h4 class="small-text italic_bold">Vivek Arora</h4>
                    <h5 class="italic_bold">Co-founding Director</h5>
                    <p class="small-text italic">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                
                    <div class="item">
                      <img class="circle2" src="images/images1.jpg"/>
                    <h4 class="small-text italic_bold">Vivek Arora</h4>
                    <h5 class="italic_bold">Co-founding Director</h5>
                    <p class="small-text italic">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                
                  </div>
                
                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>

                
        </div>
    </div>
</section>


<section>
    <div class="banner">
        
        <div class="leftSecondPanel"></div>
                <div class="rightSecondPanel rightside"></div>
        <div class="parallax-window ">
        
                
             <div class="parallax-slider overlay_image">
                <img src="images/people.jpg">
                <div class="parallax-inner-left"></div>
                <div class="parallax-inner-right"></div>
             </div>
        </div>
        
        <div class="banner-content ">
            <div class="title-heading mt30 from-bottom">
                <h3 class="heading">Sign up to our Newsletter</h3>
                <div class="title-border2">
                    <div class="title-box"></div>
                </div>
            </div>
            
            <div class="banner-content-text from-bottom ">
            <p class="italic mt40">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
            <p class="italic mt20">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the </p>
      
            <div class="form-group form-group-lg form-group-icon-left mt40">
                <form name="frmnewsletter" id="frmnewsletter" method="post"  accept-charset="UTF-8">
                    <span class="newslettermsg" id="newslettermsg"></span>
                    <div class="input-group mt28 newslatter">
                        <input class="form-control" name="txtemail" id="txtemail" placeholder="email address" type="email">
                        <span class="input-group-btn">
                            <button class="btn search_button " type="submit" id="search_trail"> 
                            Subcribe
                            </button>
                        </span> 
                    </div>  
                </form>
            
            </div>

            </div>
        </div>
        
    </div>
</section>

<section>
    <div class="container from-bottom">
       <div class="mt80 mb80">
            <div class="title-heading mb30">
                <h3 class="heading-color">Experience Mirror</h3>
                <div class="title-border">
                    <div class="title-box"></div>
                </div>
            </div>
            <p class="small-text italic text-center">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
            <p class="small-text italic text-center hide" id="contactmsg"></p>
            <div class="width600 ">
                <form class="form_layout1  mt30 " name="frmcontact" id="frmcontact" method="post" accept-charset="UTF-8">
                    <div class="col-md-6">
                        <div class="form-group">
                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <input type="text" class="form-control" id="phone_no" name="phone_no" placeholder="Phone No">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <input type="text" class="form-control" id="contact_email" name="contact_email" placeholder="Email Address">
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                          <textarea class="form-control" rows="5" id="comment" name="comment" placeholder="Message"></textarea>
                        </div>
                    </div>
                     <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary btn-lg ">Send Message</button>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>


@stop