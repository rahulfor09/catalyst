@extends('layouts.admin')
@section('title', 'User')
@section('content')
<div class="borderwhite">
    <div class="bordergrey">
        <div class="col-md-12">
            <div class="regular-black_head"> <i class="fa fa-user">&nbsp;</i>Edit User
                <div class="border-lightgrey mt5"></div>    
            </div>

                <div class="row">
                    {!! Form::open(['route' => ['admin.users.update',$data["user"]->id],'method' => 'post', 'name' => 'user-form','id' => 'user-form','class'=>'form-horizontal']) !!}

                        <div class="col-md-6">
                        <label><i class="fa fa-file-text">&nbsp;</i>First Name</label>
                        <input id="name" name="name" class="form-control" type="text" placeholder="First Name" value="{{ (old('name'))? old('name') : $data['user']->name}}">
                        @if ($errors->has('name'))<span class="itferror">{{ $errors->first('name') }}</span>@endif
                        </div>
                        
                        <div class="col-md-6">
                        <label><i class="fa fa-file-text">&nbsp;</i>Last Name</label>
                        <input id="last_name" name="last_name" class="form-control" type="text" placeholder="Last Name" value="{{ (old('last_name'))? old('last_name') : $data['user']->last_name}}">
                        @if ($errors->has('last_name'))<span class="itferror">{{ $errors->first('last_name') }}</span>@endif
                        </div>

                        <div class="col-md-6">
                        <label><i class="fa fa-file-text">&nbsp;</i>Phone No</label>
                        <input id="phone_no" name="phone_no" class="form-control" type="text" placeholder="Phone Number" value="{{ (old('phone_no'))? old('phone_no') : $data['user']->phone_no}}">
                        @if ($errors->has('phone_no'))<span class="itferror">{{ $errors->first('phone_no') }}</span>@endif
                        </div>
                            
                        <div class="col-md-6">
                        <label><i class="fa fa-file-text">&nbsp;</i>Email Id</label>
                        <input id="email" name="email" class="form-control" type="email" placeholder="Your Email" value="{{ (old('email'))? old('email') : $data['user']->email}}">
                        @if ($errors->has('email'))<span class="itferror">{{ $errors->first('email') }}</span>@endif
                        </div>
                              
                      
                        <div class="clearfix"></div>                                
                        <div class="mb10 mt30 text-right col-md-12">
                            <button class="btn bg-primary" type="submit" name="btnupdate">Submit</button>
                        </div>
                    {!! Form::close() !!}
                </div>
                
        </div> 
        <div class="clearfix"></div>
     </div>
</div> 
@stop