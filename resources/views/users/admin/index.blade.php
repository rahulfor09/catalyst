@extends('layouts.admin')
@section('title', 'Users')
@section('content')
<div class="row">

<div class="borderwhite">
                    <div class="bordergrey">
                        <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>All Users
                        <div class="border-lightgrey mt5"></div>    
                        </div>
                        
                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Created</th>
                                    <th class="actionblock">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach ($userinfo as $userdata)  
                                  <tr>
                                    <td>
                                       <?php if(!empty($userdata->user_photo)){ ?>
                                            <img src="{{asset('siteimage/users/'.$userdata->user_photo)}}" width="50" height="50"  class="img-circle" />
                                        <?php }else{ ?>
                                            <img src="{{asset('images/profile.png')}}" width="50" height="50" class="img-circle" />
                                        <?php } ?>
                                    </td>
                                    <td>{{$userdata->name}} {{$userdata->last_name}}</td>
                                    <td>{{$userdata->email}}</td>
                                    <td>{{$userdata->created_at->format('d M Y')}}</td>
                                    <td >
                                        <a href="{{ route('admin.users.edit',$userdata->id) }}" class="mb10 btn btn-primary"> <i class="fa fa-edit">&nbsp;</i></a>
                                                    {{ Form::open(['method' => 'DELETE','route' => ['admin.users.delete', $userdata->id],'class'=>'form-btns itfdeletefrm']) }}
                                                        {{ Form::hidden('id', $userdata->id) }}
                                                        {!! Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger','type'=>'submit']) !!}
                                                    {{ Form::close() }}
                                    </td>
                                  </tr>
                                   @endforeach 
                                </tbody>
                              </table>
                              <div class="clearfix"></div>
                        </div>
                        <div class="mb10">
                            
                            <nav aria-label="...">
                                <?php echo $userinfo->render(); ?>
                            </nav>
                         </div>
                         <div class="clearfix"></div>
                    </div>
                </div>
   
</div>

@stop