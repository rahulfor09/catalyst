@extends('layouts.admin')
@section('title', 'Package')
@section('content')
<div class="borderwhite">
    <div class="bordergrey">
        <div class="col-md-12">
            <div class="regular-black_head"> <i class="fa fa-video-camera">&nbsp;</i>Add Package
                <div class="border-lightgrey mt5"></div>    
            </div>

                <div class="col-md-12">
                   
                    <div class="col-md-12"> 
                        {!! Form::open(['route' => 'admin.package.store','method' => 'post', 'name' => 'package-form','id' => 'package-form','class'=>'form-horizontal']) !!}
                            
                        <div class="form-group">
                            <div class="col-md-12">
                            <label>Title</label>
                            <input type="text" id="title" name="title" class="form-control"  placeholder="Title" value="{{old('title')}}" />
                            @if ($errors->has('title'))<span class="itferror">{{ $errors->first('title') }}</span>@endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                            <label>Description</label>
                            <textarea id="description" name="description" class="form-control" placeholder="Title" rows="3" >{{old('description')}}</textarea>
                            @if ($errors->has('description'))<span class="itferror">{{ $errors->first('description') }}</span>@endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                            <label>Price</label>
                            <input type="text" id="price" name="price" class="form-control"  placeholder="Price" value="{{old('price')}}" />
                            @if ($errors->has('price'))<span class="itferror">{{ $errors->first('price') }}</span>@endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                            <label>Total Character</label>
                            <input type="text" id="total_character" name="total_character" class="form-control"  placeholder="Total Character" value="{{old('total_character')}}" />
                            @if ($errors->has('total_character'))<span class="itferror">{{ $errors->first('total_character') }}</span>@endif
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-6 col-xs-6">
                                <input type="submit" class="btn btn-primary" value="Save" name="uploadbtn" id="uploadbtn">
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="clearfix"></div>
            </div>
        </div> 
        <div class="clearfix"></div>
     </div>
</div> 
@stop